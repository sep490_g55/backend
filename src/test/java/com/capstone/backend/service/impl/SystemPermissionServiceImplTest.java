package com.capstone.backend.service.impl;

import com.capstone.backend.entity.SystemPermission;
import com.capstone.backend.entity.User;
import com.capstone.backend.entity.type.MethodType;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.systempermission.PermissionDTODisplay;
import com.capstone.backend.model.dto.systempermission.SystemPermissionDTORequest;
import com.capstone.backend.model.dto.systempermission.SystemPermissionDTOResponse;
import com.capstone.backend.model.dto.systempermission.SystemPermissionDTOUpdate;
import com.capstone.backend.repository.SystemPermissionRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.SystemPermissionCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
@MockitoSettings(strictness = Strictness.LENIENT)
class SystemPermissionServiceImplTest {
    @InjectMocks
    SystemPermissionServiceImpl systemPermissionService;
    @Mock
    SystemPermissionRepository systemPermissionRepository;
    @Mock
    SystemPermissionCriteria systemPermissionCriteria;
    @Mock
    UserRepository userRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserHelper userHelper;

    @Test
    void viewSearchPermission() {
    }

    @Test
    void getSystemPermissionById_id_not_found() {
        Long id = 10L;
        when(systemPermissionRepository.findById(id)).thenReturn(Optional.empty());
        ApiException actualThrow = assertThrows(ApiException.class, () -> systemPermissionService.getSystemPermissionById(id));
        // Verify that the findByUsernameOrEmailActive method was called with the correct parameter
        verify(systemPermissionRepository, times(1)).findById(id);

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("System permission is not found", actualThrow.getError().getMessage());
    }

    @Test
    void getSystemPermissionById_success() {
        Long id = 10L;
        when(systemPermissionRepository.findById(id)).thenReturn(Optional.of(SystemPermission.builder().build()));
        when(userRepository.findUsernameByUserId(1L)).thenReturn(any());
        SystemPermissionDTOResponse response = systemPermissionService.getSystemPermissionById(id);
        verify(systemPermissionRepository, times(1)).findById(id);
    }

    @Test
    void createSystemPermission_dependency_permission_id_not_found() {
        Long id = 1L;
        SystemPermissionDTORequest request = SystemPermissionDTORequest.builder()
                .permissionName("")
                .build();
        when(systemPermissionRepository.findById(1L)).thenReturn(Optional.empty());
        ApiException actualThrow = assertThrows(ApiException.class, () -> systemPermissionService.getSystemPermissionById(id));
        // Verify that the findByUsernameOrEmailActive method was called with the correct parameter
        verify(systemPermissionRepository, times(1)).findById(id);

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("System permission is not found", actualThrow.getError().getMessage());
    }

    @Test
    public void createSystemPermission_shouldThrowExceptionWhenDuplicateSystemPermissionName() {
        // Arrange
        SystemPermissionDTORequest request = new SystemPermissionDTORequest();
        request.setPermissionName("Manage Users");
        request.setPath("/api/users");
        request.setMethodType(MethodType.GET);

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);

        SystemPermission existingSystemPermission = new SystemPermission();
        existingSystemPermission.setName("Manage Users");
        Mockito.when(systemPermissionRepository.findByPathAndMethod(request.getPath(), request.getMethodType(), 0L))
                .thenReturn(Optional.of(existingSystemPermission));

        ApiException actualThrow = assertThrows(ApiException.class, () -> systemPermissionService.createSystemPermission(request));
        // Verify that the findByUsernameOrEmailActive method was called with the correct parameter

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Duplicate system permission name", actualThrow.getError().getMessage());

        Mockito.verify(systemPermissionRepository, times(0)).save(Mockito.any(SystemPermission.class));
    }


    @Test
    void updateSystemPermission_dependency_permission_id_not_found() {
        Long id = 1L;
        SystemPermissionDTOUpdate request = SystemPermissionDTOUpdate.builder()
                .permissionId(1L)
                .permissionName("")
                .build();
        when(systemPermissionRepository.findById(1L)).thenReturn(Optional.empty());
        ApiException actualThrow = assertThrows(ApiException.class, () -> systemPermissionService.updateSystemPermission(request));
        // Verify that the findByUsernameOrEmailActive method was called with the correct parameter
        verify(systemPermissionRepository, times(1)).findById(id);

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("System permission is not found", actualThrow.getError().getMessage());
    }

    @Test
    void updateSystemPermission_permission_id_not_found() {
        Long id = 1L;
        SystemPermissionDTOUpdate request = SystemPermissionDTOUpdate.builder()
                .permissionId(1L)
                .permissionName("")
                .build();
        when(systemPermissionRepository.findById(1L)).thenReturn(Optional.empty());
        ApiException actualThrow = assertThrows(ApiException.class, () -> systemPermissionService.updateSystemPermission(request));
        // Verify that the findByUsernameOrEmailActive method was called with the correct parameter
        verify(systemPermissionRepository, times(1)).findById(id);

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("System permission is not found", actualThrow.getError().getMessage());
    }

    @Test
    public void updateSystemPermission_shouldThrowExceptionWhenDuplicateSystemPermissionName() {
        // Arrange
        SystemPermissionDTOUpdate request = new SystemPermissionDTOUpdate();
        request.setPermissionId(1L);
        request.setPermissionName("Manage Users");
        request.setPath("/api/users");
        request.setMethodType(MethodType.DELETE);

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);

        SystemPermission systemPermissionRoot = new SystemPermission();
        systemPermissionRoot.setId(1L);

        SystemPermission existingSystemPermission = new SystemPermission();
        existingSystemPermission.setName("Manage Users");
        Mockito.when(systemPermissionRepository.findByPathAndMethod(request.getPath(), request.getMethodType(), request.getPermissionId())).thenReturn(Optional.of(existingSystemPermission));

        // Assertions for ApiException properties
        ApiException actualThrow = assertThrows(ApiException.class, () -> systemPermissionService.updateSystemPermission(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Duplicate system permission name", actualThrow.getError().getMessage());

        Mockito.verify(systemPermissionRepository, times(0)).save(Mockito.any(SystemPermission.class));
    }


    @Test
    void changeStatus_permission_id() {
        Long id = 1L;
        when(systemPermissionRepository.findById(1L)).thenReturn(Optional.empty());
        ApiException actualThrow = assertThrows(ApiException.class, () -> systemPermissionService.changeStatus(id));
        // Verify that the findByUsernameOrEmailActive method was called with the correct parameter
        verify(systemPermissionRepository, times(1)).findById(id);

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("System permission is not found", actualThrow.getError().getMessage());
    }

    @Test
    void changeStatus_success() {
        Long id = 1L;

        SystemPermission systemPermission = SystemPermission.builder()
                .id(1L)
                .name("system permission")
                .active(true)
                .build();

        when(systemPermissionRepository.findById(1L)).thenReturn(Optional.of(systemPermission));
        when(systemPermissionRepository.save(systemPermission)).thenReturn(SystemPermission.builder().build());

        Boolean isChange = systemPermissionService.changeStatus(id);

        // Verify that the findByUsernameOrEmailActive method was called with the correct parameter
        verify(systemPermissionRepository, times(1)).findById(id);

        verify(systemPermissionRepository, times(1)).save(systemPermission);
    }

    @Test
    public void getSystemPermissionsByRole_shouldGetSystemPermissionsByRoleWithExistingPermissions() {
        // Arrange
        Long roleId = 1L;

        // Mock dependencies
        List<SystemPermission> systemPermissions = new ArrayList<>();
        SystemPermission systemPermission1 = new SystemPermission();
        systemPermission1.setId(1L);
        systemPermission1.setName("Manage Users");
        systemPermission1.setPath("/api/users");
        systemPermission1.setMethodType(MethodType.GET);
        systemPermissions.add(systemPermission1);

        SystemPermission systemPermission2 = new SystemPermission();
        systemPermission2.setId(2L);
        systemPermission2.setName("Manage Roles");
        systemPermission2.setPath("/api/roles");
        systemPermission2.setMethodType(MethodType.GET);
        systemPermissions.add(systemPermission2);

        Mockito.when(systemPermissionRepository.findSystemPermissionByRoleId(roleId)).thenReturn(systemPermissions);

        // Act
        List<PermissionDTODisplay> permissions = systemPermissionService.getSystemPermissionsByRole(roleId);
    }

    @Test
    public void getSystemPermissionsByRole_shouldReturnEmptyListWhenNoPermissionsExist() {
        // Arrange
        Long roleId = 1L;

        // Mock dependencies
        Mockito.when(systemPermissionRepository.findSystemPermissionByRoleId(roleId)).thenReturn(Collections.emptyList());

        // Act
        List<PermissionDTODisplay> permissions = systemPermissionService.getSystemPermissionsByRole(roleId);

        // Assert
        Assertions.assertNotNull(permissions);
        Assertions.assertEquals(0, permissions.size());
    }
}