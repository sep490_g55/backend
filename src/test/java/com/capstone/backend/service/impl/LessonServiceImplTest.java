package com.capstone.backend.service.impl;

import com.capstone.backend.entity.BookVolume;
import com.capstone.backend.entity.Chapter;
import com.capstone.backend.entity.Lesson;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.chapter.ChapterDTOResponse;
import com.capstone.backend.model.dto.lesson.LessonDTOFilter;
import com.capstone.backend.model.dto.lesson.LessonDTORequest;
import com.capstone.backend.model.dto.lesson.LessonDTOResponse;
import com.capstone.backend.repository.ChapterRepository;
import com.capstone.backend.repository.LessonRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.LessonCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class LessonServiceImplTest {
    @Mock
    LessonRepository lessonRepository;
    @Mock
    ChapterRepository chapterRepository;
    @Mock
    LessonCriteria lessonCriteria;
    @Mock
    UserHelper userHelper;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;

    @InjectMocks
    LessonServiceImpl lessonService;

    User userLogged;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung");
        userLogged.setId(1L);
    }

    @Test
    void createLesson_When_DuplicateName() {
        // input
        LessonDTORequest request = new LessonDTORequest("Bai 1");
        Chapter chapter = new Chapter();
        chapter.setId(3L);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(lessonRepository.findByName(Mockito.eq("Bai 1"), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(new Lesson()));
        // test accept
        assertThrows(ApiException.class, () -> {
            lessonService.createLesson(request, chapter.getId());
        }, "Duplicate bookVolume name");
    }

    @Test
    void createLesson_When_requestNull() {
        // input
        LessonDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            lessonService.createLesson(request, 1L);
        }, "NullPointerException");
    }

    @Test
    void createLesson_When_Success() {
        LessonDTOResponse lessonDTOResponse = new LessonDTOResponse();
        lessonDTOResponse.setName("Bai 2");

        Chapter chapter = new Chapter();
        chapter.setId(1L);
        chapter.setName("Chuong 1");
        chapter.setBookVolume(BookVolume.builder().name("Tap 1").userId(1L).createdAt(LocalDateTime.now()).build());
        chapter.setActive(true);
        chapter.setCreatedAt(LocalDateTime.now());

        LessonDTORequest request = new LessonDTORequest("Bai 2");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Lesson lesson = Lesson.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .chapter(chapter)
                .build();

        Mockito.when(lessonRepository.findByName(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.empty());
        Mockito.when(chapterRepository.findById(Mockito.any())).thenReturn(Optional.of(new Chapter()));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        Mockito.when(lessonRepository.save(Mockito.any())).thenReturn(lesson);
        lessonDTOResponse = LessonDTOResponse.builder()
                .active(true)
                .name(lesson.getName())
                .id(lesson.getId())
                .active(lesson.getActive())
                .createdAt(lesson.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        LessonDTOResponse lessonDTOResponse1 = lessonService.createLesson(request, 1L);
        assertEquals(lessonDTOResponse1.getCreator(), lessonDTOResponse.getCreator());
        assertEquals(lessonDTOResponse1.getCreatedAt(), lessonDTOResponse.getCreatedAt());
        assertEquals(lessonDTOResponse1.getId(), lessonDTOResponse.getId());
        assertEquals(lessonDTOResponse1.getName(), lessonDTOResponse.getName());

    }

    @Test
    void update_with_duplicate_Chapter() {
        LessonDTORequest request = new LessonDTORequest("Bai 1");
        Chapter chapter = new Chapter();
        chapter.setId(1L);

        Long id = 1L;
        Lesson lesson = Lesson.builder()
                .chapter(chapter)
                .build();
        Lesson lesson1 = Lesson.builder().build();
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(lessonRepository.findById(Mockito.eq(id)))
                .thenReturn(Optional.of(lesson));
        Mockito.when(lessonRepository.findByName(
                        Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(lesson1));
        // test accept
        assertThrows(ApiException.class, () -> {
            lessonService.updateLesson(id, request);
        }, "Duplicate lessin name");

    }

    @Test
    void updateLesson_When_requestNull() {
        // input
        LessonDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            lessonService.updateLesson(1L, request);
        }, "NullPointerException");
    }

    @Test
    void changeStatus_When_ThrowEx() {
        Long id = 1L;
        Chapter chapter = new Chapter();
        chapter.setId(1L);
        chapter.setName("Tap 1");
        chapter.setActive(true);
        chapter.setCreatedAt(LocalDateTime.now());

//        bookSeriesList.add(new BookSeries());
        Lesson lesson = Lesson.builder()
                .userId(userLogged.getId())
                .chapter(chapter)
                .name("chuong 2")
                .active(true)
                .build();
        Mockito.when(lessonRepository.findById(id)).thenReturn(Optional.of(lesson));
        // test accept
        assertThrows(NullPointerException.class, () -> {
            lessonService.changeStatus(id);
        }, "Can not change status Chapter because Lesson already exists");
    }

    @Test
    void searchLesson_When_Ex() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        Chapter chapter = new Chapter();
        chapter.setId(1L);
        LessonDTOFilter lessonDTOFilter = new LessonDTOFilter();
        lessonDTOFilter.setName("Bai 1");
        Mockito.when(chapterRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            lessonService.searchLesson(lessonDTOFilter, chapter.getId());
        }, "messageException.MSG_USER_NOT_FOUND");
    }

    @Test
    void searchLesson() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        Chapter chapter = new Chapter();
        chapter.setId(1L);
        Mockito.when(lessonCriteria.searchLesson(Mockito.any(), Mockito.any())).thenReturn(response);
        Mockito.when(chapterRepository.findById(Mockito.any())).thenReturn(Optional.of(new Chapter()));
        LessonDTOFilter lessonDTOFilter = new LessonDTOFilter();
        lessonDTOFilter.setName("Bai 2");
        PagingDTOResponse pagingDTOResponse = lessonService.searchLesson(lessonDTOFilter, chapter.getId());
        assertEquals(pagingDTOResponse.getTotalElement(), pagingDTOResponse.getTotalElement());
        assertEquals(pagingDTOResponse.getTotalPage(), pagingDTOResponse.getTotalPage());
    }

    @Test
    void viewChapterById() {
        ChapterDTOResponse chapterDTOResponse = ChapterDTOResponse.builder().id(10L).build();
        Long id = 1L;
        Chapter chapter = new Chapter();
        chapter.setId(1L);
        chapter.setName("Tap 1");
        chapter.setActive(true);
        chapter.setCreatedAt(LocalDateTime.now());

//        bookSeriesList.add(new BookSeries());
        Lesson lesson = Lesson.builder()
                .id(10L)
                .userId(userLogged.getId())
                .chapter(chapter)
                .name("bai 2")
                .active(true)
                .build();
        Mockito.when(lessonRepository.findById(id)).thenReturn(Optional.of(lesson));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        LessonDTOResponse lessonDTOResponse1 = lessonService.viewLessonById(id);
        assertEquals(lessonDTOResponse1.getId(), 10L);
        assertEquals(lessonDTOResponse1.getName(), "bai 2");
    }

    @Test
    void viewLessonById_When_ResouceNotFound() {
        Long id = 1L;
        Chapter chapter = new Chapter();
        chapter.setId(1L);
        chapter.setName("Tap 1");
        chapter.setActive(true);
        chapter.setCreatedAt(LocalDateTime.now());

//        bookSeriesList.add(new BookSeries());
        Lesson lesson = Lesson.builder()
                .id(10L)
                .userId(userLogged.getId())
                .chapter(chapter)
                .name("bai 2")
                .active(true)
                .build();
        Mockito.when(lessonRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            lessonService.viewLessonById(id);
        }, "Not Found Lesson");
    }
}