package com.capstone.backend.service.impl;

import com.capstone.backend.entity.BookSeries;
import com.capstone.backend.entity.BookSeriesSubject;
import com.capstone.backend.entity.Class;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.bookseries.BookSeriesDTOFilter;
import com.capstone.backend.model.dto.bookseries.BookSeriesDTORequest;
import com.capstone.backend.model.dto.bookseries.BookSeriesDTOResponse;
import com.capstone.backend.repository.BookSeriesRepository;
import com.capstone.backend.repository.ClassRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.BookSeriesCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class BookSeriesServiceImplTest {

    @InjectMocks
    BookSeriesServiceImpl bookSeriesService;

    @Mock
    BookSeriesRepository bookSeriesRepository;
    @Mock
    ClassRepository classRepository;
    @Mock
    MessageException messageException;
    @Mock
    BookSeriesCriteria bookSeriesCriteria;
    @Mock
    UserHelper userHelper;
    @Mock
    UserRepository userRepository;


    User userLogged;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung");
        userLogged.setId(1L);
    }

    @Test
    void createBookSeries() {
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Class classObject = new Class();
        classObject.setId(1L);
        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Rap1")
                .userId(1L)
                .classObject(classObject)
                .build();
        Mockito.when(bookSeriesRepository.save(bookSeries)).thenReturn(bookSeries);

        assertEquals(bookSeries, bookSeriesRepository.save(bookSeries));
    }

    @Test
    void createBookSeries_When_DuplicateName() {
        // input
        BookSeriesDTORequest request = new BookSeriesDTORequest("History");
        Class classEntity = new Class();
        classEntity.setId(1L);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(bookSeriesRepository.findByName(Mockito.eq("History"), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(new BookSeries()));
        // test accept
        assertThrows(ApiException.class, () -> {
            bookSeriesService.createBookSeries(request, classEntity.getId());
        }, "Duplicate bookSeires name");
    }

    @Test
    void createBookSeries_When_ClassNotFound() {
        // input
        BookSeriesDTORequest request = new BookSeriesDTORequest("History");
        Class classEntity = new Class();
        classEntity.setId(1L);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(bookSeriesRepository.findByName(Mockito.eq("History"), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(Mockito.any()));
        // test accept
        Mockito.when(classRepository.findById(Mockito.any()))
                .thenReturn(Optional.empty());
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        assertThrows(ApiException.class, () -> {
            bookSeriesService.createBookSeries(request, classEntity.getId());
        }, "Class is not found");
    }

    @Test
    void createBookSeries_When_requestNull() {
        // input
        BookSeriesDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            bookSeriesService.createBookSeries(request, 1L);
        }, "NullPointerException");
    }

    @Test
    void createBookSeries_When_Success() {
        BookSeriesDTOResponse bookSeriesDTOResponse = new BookSeriesDTOResponse();
        bookSeriesDTOResponse.setName("History Book");
        BookSeriesDTORequest request = new BookSeriesDTORequest("History Book");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .classObject(Class.builder().name("12345").active(true).id(1L).createdAt(LocalDateTime.now()).build())
                .userId(userLogged.getId())
                .build();

        Mockito.when(bookSeriesRepository.findByName(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.empty());
        Mockito.when(classRepository.findById(Mockito.any())).thenReturn(Optional.of(new Class()));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        Mockito.when(bookSeriesRepository.save(Mockito.any())).thenReturn(bookSeries);
        bookSeriesDTOResponse = BookSeriesDTOResponse.builder()
                .name(bookSeries.getName())
                .id(bookSeries.getId())
                .active(bookSeries.getActive())
                .createdAt(bookSeries.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        BookSeriesDTOResponse bookSeriesDTOResponse1 = bookSeriesService.createBookSeries(request, 1L);
        assertEquals(bookSeriesDTOResponse1.getCreator(), bookSeriesDTOResponse.getCreator());
        assertEquals(bookSeriesDTOResponse1.getCreatedAt(), bookSeriesDTOResponse.getCreatedAt());
        assertEquals(bookSeriesDTOResponse1.getId(), bookSeriesDTOResponse.getId());
        assertEquals(bookSeriesDTOResponse1.getName(), bookSeriesDTOResponse.getName());

    }

    @Test
    void updateBookSeries() {
        Long id = 1L;
        BookSeries bookSeriesMock = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("abc")
                .userId(1L)
                .build();
        Optional<BookSeries> bookSeries = Optional.of(bookSeriesMock);
//        Mockito.when(bookSeriesRepository.findById(id)).thenReturn(bookSeries);
        BookSeries entity = bookSeries.get();
        entity.setName("abc");
        entity.setUserId(1L);
        Mockito.when(bookSeriesRepository.save(entity)).thenReturn(entity);
        assertEquals(entity, bookSeriesRepository.save(entity));
    }

    @Test
    void update_with_duplicate_BookSeries() {
        BookSeriesDTORequest request = new BookSeriesDTORequest("History Book");
        Class classEntity = new Class();
        classEntity.setId(1L);
        Long id = 1L;
        BookSeries bookSeries = BookSeries.builder().classObject(Class.builder().id(1L).name("abcs").build()).build();
        BookSeries bookSeries1 = BookSeries.builder().build();
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(bookSeriesRepository.findById(Mockito.eq(id)))
                .thenReturn(Optional.of(bookSeries));
        Mockito.when(bookSeriesRepository.findByName(
                        Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(bookSeries1));
        // test accept
        assertThrows(ApiException.class, () -> {
            bookSeriesService.updateBookSeries(id, request);
        }, "Duplicate book series name");

    }

    @Test
    void updateBookSeries_When_requestNull() {
        // input
        BookSeriesDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            bookSeriesService.createBookSeries(request, 1L);
        }, "NullPointerException");
    }

    //dang fail
    @Test
    void updateBookSeries_When_Success() {
        BookSeriesDTOResponse bookSeriesDTOResponse = new BookSeriesDTOResponse();
        bookSeriesDTOResponse.setName("History Book");
        BookSeriesDTORequest request = new BookSeriesDTORequest("History Book");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .classObject(Class.builder().name("12345").active(true).id(1L).createdAt(LocalDateTime.now()).build())
                .userId(userLogged.getId())
                .build();

        Mockito.when(bookSeriesRepository.findByName(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.empty());
        Mockito.when(classRepository.findById(Mockito.any())).thenReturn(Optional.of(new Class()));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        Mockito.when(bookSeriesRepository.save(Mockito.any())).thenReturn(bookSeries);
        bookSeriesDTOResponse = BookSeriesDTOResponse.builder()
                .name(bookSeries.getName())
                .id(bookSeries.getId())
                .active(bookSeries.getActive())
                .createdAt(bookSeries.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        BookSeriesDTOResponse bookSeriesDTOResponse1 = bookSeriesService.createBookSeries(request, 1L);
        assertEquals(bookSeriesDTOResponse1.getCreator(), bookSeriesDTOResponse.getCreator());
        assertEquals(bookSeriesDTOResponse1.getCreatedAt(), bookSeriesDTOResponse.getCreatedAt());
        assertEquals(bookSeriesDTOResponse1.getId(), bookSeriesDTOResponse.getId());
        assertEquals(bookSeriesDTOResponse1.getName(), bookSeriesDTOResponse.getName());
    }

    @Test
    void changeStatus_When_ThrowEx() {
        Long id = 1L;
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        bookSeriesSubjectList.add(new BookSeriesSubject());
//        bookSeriesList.add(new BookSeries());
        BookSeries bookSeries = BookSeries.builder()
                .userId(userLogged.getId())
                .bookSeriesSubjects(bookSeriesSubjectList)
                .build();
        Mockito.when(bookSeriesRepository.findById(id)).thenReturn(Optional.of(bookSeries));
        // test accept
        assertThrows(ApiException.class, () -> {
            bookSeriesService.changeStatus(id);
        }, "Can not change status Class because Book Series already exists");
    }

    @Test
    void changeStatus_When_Success_False_To_True() {
        Long id = 1L;
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        BookSeries bookSeries = BookSeries.builder()
                .userId(userLogged.getId())
                .bookSeriesSubjects(bookSeriesSubjectList)
                .bookSeriesSubjects(bookSeriesSubjectList)
                .active(false)
                .build();
        Mockito.when(bookSeriesRepository.findById(id)).thenReturn(Optional.of(bookSeries));
        // test accept
        Mockito.when(bookSeriesRepository.save(Mockito.any())).thenReturn(bookSeries);
        bookSeriesService.changeStatus(id);
//        assertTrue(result);
        assertEquals(bookSeries.getActive(), true);
    }

    @Test
    void changeStatus_When_Success_True_To_False() {
        Long id = 1L;
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        BookSeries bookSeries = BookSeries.builder()
                .userId(userLogged.getId())
                .bookSeriesSubjects(bookSeriesSubjectList)
                .bookSeriesSubjects(bookSeriesSubjectList)
                .active(true)
                .build();
        Mockito.when(bookSeriesRepository.findById(id)).thenReturn(Optional.of(bookSeries));
        // test accept
        Mockito.when(bookSeriesRepository.save(Mockito.any())).thenReturn(bookSeries);
        bookSeriesService.changeStatus(id);
//        assertTrue(result);
        assertEquals(bookSeries.getActive(), false);
    }

    @Test
    void searchBookSeries() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        Class classEntity = new Class();
        classEntity.setId(1L);
        Mockito.when(bookSeriesCriteria.searchBookSeries(Mockito.any(), Mockito.any())).thenReturn(response);
        Mockito.when(classRepository.findById(Mockito.any())).thenReturn(Optional.of(new Class()));
        BookSeriesDTOFilter bookSeriesDTOFilter = new BookSeriesDTOFilter();
        bookSeriesDTOFilter.setName("Cánh Diều");
        PagingDTOResponse pagingDTOResponse = bookSeriesService.searchBookSeries(bookSeriesDTOFilter, classEntity.getId());
        assertEquals(pagingDTOResponse.getTotalElement(), pagingDTOResponse.getTotalElement());
        assertEquals(pagingDTOResponse.getTotalPage(), pagingDTOResponse.getTotalPage());
    }

    @Test
    void searchBookSeries_When_Ex() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        Class classEntity = new Class();
        classEntity.setId(1L);
        BookSeriesDTOFilter bookSeriesDTOFilter = new BookSeriesDTOFilter();
        bookSeriesDTOFilter.setName("abc");
        Mockito.when(classRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            bookSeriesService.searchBookSeries(bookSeriesDTOFilter, classEntity.getId());
        }, "messageException.MSG_USER_NOT_FOUND");
    }

    @Test
    void viewBookSeriesById() {
        BookSeriesDTOResponse bookSeriesDTOResponse = BookSeriesDTOResponse.builder().id(10L).build();
        Long id = 1L;
        BookSeries bookSeries = BookSeries.builder()
                .name("abcd")
                .id(10L)
                .classObject(Class.builder().name("123").id(1L).active(true).createdAt(LocalDateTime.now()).build())
                .active(true)
                .build();
        Mockito.when(bookSeriesRepository.findById(id)).thenReturn(Optional.of(bookSeries));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        BookSeriesDTOResponse bookSeriesDTOResponse1 = bookSeriesService.viewBookSeriesById(id);
        assertEquals(bookSeriesDTOResponse1.getId(), 10L);
        assertEquals(bookSeriesDTOResponse1.getName(), "abcd");

    }

    @Test
    void viewBookSeriesById_When_BookNotFound() {
        BookSeriesDTOResponse bookSeriesDTOResponse = BookSeriesDTOResponse.builder().id(10L).build();
        Long id = 1L;
        BookSeries bookSeries = BookSeries.builder()
                .name("abcd")
                .id(10L)
                .classObject(Class.builder().name("123").id(1L).active(true).createdAt(LocalDateTime.now()).build())
                .active(true)
                .build();
        Mockito.when(bookSeriesRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            bookSeriesService.viewBookSeriesById(id);
        }, "Can not change status Class because Book Series already exists");
    }
}

