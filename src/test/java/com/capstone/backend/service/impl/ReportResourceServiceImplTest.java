package com.capstone.backend.service.impl;

import com.capstone.backend.entity.Resource;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.reportresource.ReportResourceDTORequest;
import com.capstone.backend.repository.ReportResourceRepository;
import com.capstone.backend.repository.ResourceRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.UserResourceCriteria;
import com.capstone.backend.utils.FileHelper;
import com.capstone.backend.utils.MessageException;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
class ReportResourceServiceImplTest {
    @Mock
    UserRepository userRepository;
    @Mock
    ResourceRepository resourceRepository;
    @Mock
    ReportResourceRepository reportResourceRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserResourceCriteria userResourceCriteria;
    @InjectMocks
    ReportResourceServiceImpl reportResourceService;

    @Test
    public void createReportResource_shouldThrowExceptionWhenUserNotFound() {
        // Arrange
        ReportResourceDTORequest request = new ReportResourceDTORequest();
        request.setResourceId(2L);
        request.setMessage("This resource is offensive.");

        // Mock dependencies

        ApiException actualThrow = assertThrows(ApiException.class, () -> reportResourceService.createReportResource(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_USER_NOT_FOUND, actualThrow.getError().getMessage());
    }

    @Test
    public void createReportResource_shouldThrowExceptionWhenResourceNotFound() {
        // Arrange
        ReportResourceDTORequest request = new ReportResourceDTORequest();
        request.setResourceId(2L);
        request.setMessage("This resource is offensive.");

        // Mock dependencies
        User reporter = new User();

        Mockito.when(resourceRepository.findById(request.getResourceId())).thenReturn(Optional.empty());

        ApiException actualThrow = assertThrows(ApiException.class, () -> reportResourceService.createReportResource(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_RESOURCE_NOT_FOUND, actualThrow.getError().getMessage());
    }

    @Test
    public void createReportResource_shouldThrowExceptionWhenMessageContainsInvalidWords() {
        // Arrange
        ReportResourceDTORequest request = new ReportResourceDTORequest();
        request.setResourceId(2L);
        request.setMessage("This resource contains an offensive word.");

        // Mock dependencies
        User reporter = new User();

        Resource resource = new Resource();
        resource.setId(request.getResourceId());

        Mockito.when(resourceRepository.findById(request.getResourceId())).thenReturn(Optional.of(resource));
        Mockito.mockStatic(FileHelper.class);
        Mockito.when(FileHelper.checkContentInputValid(request.getMessage())).thenReturn(true);

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> reportResourceService.createReportResource(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_TEXT_NO_STANDARD_WORD, actualThrow.getError().getMessage());
    }


}