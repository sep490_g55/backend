package com.capstone.backend.service.impl;

import com.capstone.backend.entity.*;
import com.capstone.backend.entity.type.PermissionResourceType;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.resource.*;
import com.capstone.backend.model.dto.tag.TagSuggestDTORequest;
import com.capstone.backend.model.dto.tag.TagSuggestDTOResponse;
import com.capstone.backend.repository.*;
import com.capstone.backend.repository.criteria.MaterialsCriteria;
import com.capstone.backend.repository.criteria.ResourceCriteria;
import com.capstone.backend.service.CommentService;
import com.capstone.backend.service.FileService;
import com.capstone.backend.service.UserResourceService;
import com.capstone.backend.utils.CheckPermissionResource;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.S3Util;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
@MockitoSettings(strictness = Strictness.LENIENT)
class ResourceServiceImplTest {
    @Mock
    ResourceRepository resourceRepository;
    @Mock
    UserResourceRepository userResourceRepository;
    @Mock
    CommentService commentService;
    @Mock
    FileService fileService;
    @Mock
    TagRepository tagRepository;
    @Mock
    LessonRepository lessonRepository;
    @Mock
    SubjectRepository subjectRepository;
    @Mock
    UserHelper userHelper;
    @Mock
    ResourceTagRepository resourceTagRepository;
    @Mock
    UserResourcePermissionRepository userResourcePermissionRepository;
    @Mock
    ResourceCriteria resourceCriteria;
    MessageException messageException;
    @Mock
    MaterialsCriteria materialsCriteria;
    @Mock
    UserRepository userRepository;
    @Mock
    UserRoleRepository userRoleRepository;
    @Mock
    CheckPermissionResource checkPermissionResource;
    @Mock
    UserResourceService userResourceService;
    @Mock
    S3Util s3Util;
    @InjectMocks
    ResourceServiceImpl resourceService;

    @Test
    public void testUploadResourceSingleFileSuccess() throws Exception {
        // Mock objects
        MultipartFile mockFile = mock(MultipartFile.class);
        MultipartFile[] mockFiles = Collections.singletonList(mockFile).toArray(new MultipartFile[0]);
        ResourceDTORequest mockRequest = mock(ResourceDTORequest.class);
        User mockUser = mock(User.class);
        FileDTOResponse mockFileDTOResponse = mock(FileDTOResponse.class);
        Subject mockSubject = mock(Subject.class);
        Lesson mockLesson = mock(Lesson.class);

        // Mock behavior
        when(userHelper.getUserLogin()).thenReturn(mockUser);
        when(fileService.uploadMultiFile(mockFiles, mockRequest.getLessonId(), mockRequest.getName()))
                .thenReturn(Collections.singletonList(mockFileDTOResponse));
        when(subjectRepository.findByIdAndActiveTrue(mockRequest.getSubjectId())).thenReturn(Optional.of(mockSubject));
        when(lessonRepository.findById(mockRequest.getLessonId())).thenReturn(Optional.of(mockLesson));

        // Execute method
        List<ResourceDTOResponse> response = resourceService.uploadResource(mockRequest, new MultipartFile[0]);

        // Verify
        verify(userHelper).getUserLogin();
        verify(subjectRepository).findByIdAndActiveTrue(mockRequest.getSubjectId());
        verify(lessonRepository).findById(mockRequest.getLessonId());
    }

    @Test
    public void testUploadResourceMultipleFilesSuccess() throws Exception {
        // Mock objects
        MultipartFile[] mockFiles = Arrays.asList(mock(MultipartFile.class), mock(MultipartFile.class)).toArray(new MultipartFile[0]);

        ResourceDTORequest mockRequest = mock(ResourceDTORequest.class);
        User mockUser = mock(User.class);
        List<FileDTOResponse> mockFileDTOResponseList = Arrays.asList(mock(FileDTOResponse.class), mock(FileDTOResponse.class));
        Subject mockSubject = mock(Subject.class);
        Lesson mockLesson = mock(Lesson.class);

        // Mock behavior
        when(userHelper.getUserLogin()).thenReturn(mockUser);
        when(fileService.uploadMultiFile(mockFiles, mockRequest.getLessonId(), mockRequest.getName()))
                .thenReturn(mockFileDTOResponseList);
        when(subjectRepository.findByIdAndActiveTrue(mockRequest.getSubjectId())).thenReturn(Optional.of(mockSubject));
        when(lessonRepository.findById(mockRequest.getLessonId())).thenReturn(Optional.of(mockLesson));

        // Execute method
        List<ResourceDTOResponse> response = resourceService.uploadResource(mockRequest, new MultipartFile[0]);
    }

    @Test
    public void testUploadResourceSubjectNotFound() {
        // Mock objects
        MultipartFile mockFile = mock(MultipartFile.class);
        MultipartFile[] mockFiles = Collections.singletonList(mockFile).toArray(new MultipartFile[0]);
        ResourceDTORequest mockRequest = mock(ResourceDTORequest.class);
        User mockUser = mock(User.class);
        FileDTOResponse mockFileDTOResponse = mock(FileDTOResponse.class);
        Lesson mockLesson = mock(Lesson.class);

        // Mock behavior
        when(userHelper.getUserLogin()).thenReturn(mockUser);
        when(fileService.uploadMultiFile(mockFiles, mockRequest.getLessonId(), mockRequest.getName()))
                .thenReturn(Collections.singletonList(mockFileDTOResponse));
        when(subjectRepository.findByIdAndActiveTrue(1L)).thenReturn(Optional.empty());

        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.uploadResource(mockRequest, mockFiles));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Subject is not found", actualThrow.getError().getMessage());
    }

    @Test
    public void testUploadResourceLessonNotFound() {
        // Mock objects
        MultipartFile mockFile = mock(MultipartFile.class);
        MultipartFile[] mockFiles = Collections.singletonList(mockFile).toArray(new MultipartFile[0]);
        ResourceDTORequest mockRequest = mock(ResourceDTORequest.class);
        User mockUser = mock(User.class);
        FileDTOResponse mockFileDTOResponse = mock(FileDTOResponse.class);
        Subject mockSubject = mock(Subject.class);

        // Mock behavior
        when(userHelper.getUserLogin()).thenReturn(mockUser);
        when(fileService.uploadMultiFile(mockFiles, mockRequest.getLessonId(), mockRequest.getName()))
                .thenReturn(Collections.singletonList(mockFileDTOResponse));
        when(subjectRepository.findByIdAndActiveTrue(mockRequest.getSubjectId())).thenReturn(Optional.of(mockSubject));
        when(lessonRepository.findById(mockRequest.getLessonId())).thenReturn(Optional.empty());

        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.uploadResource(mockRequest, mockFiles));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Lesson is not found", actualThrow.getError().getMessage());
    }

    @Test
    public void testGetResourceDetailByIdNotFound() throws ApiException {
        // Mock objects
        Long mockResourceId = 1L;

        // Mock behavior
        when(resourceRepository.findById(mockResourceId)).thenReturn(Optional.empty());

        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.getResourceDetailById(mockResourceId));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Resource is not found", actualThrow.getError().getMessage());
    }

    @Test
    public void testGetResourceDetailByIdNoPermission() throws ApiException {
        // Mock objects
        Long mockResourceId = 1L;
        User mockUserLoggedIn = mock(User.class);
        Resource mockResource = mock(Resource.class);

        // Mock behavior
        when(userHelper.getUserLogin()).thenReturn(mockUserLoggedIn);
        when(resourceRepository.findById(mockResourceId)).thenReturn(Optional.of(mockResource));
        when(checkPermissionResource.needCheckPermissionResource(mockUserLoggedIn, mockResource, PermissionResourceType.V))
                .thenReturn(true);

        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.getResourceDetailById(mockResourceId));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Access denied, You have no bearer token", actualThrow.getError().getMessage());
    }

    @Test
    void searchMediaResource() {
    }

    @Test
    void searchMaterials() {
    }

    @Test
    public void testDownloadResourceNotFound() throws Exception {
        // Mock dependencies
        // Setup mocks
        Mockito.when(resourceRepository.findByResourceSrc("fileName"))
                .thenReturn(Optional.empty());

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.downloadResource("fileName"));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Resource is not found", actualThrow.getError().getMessage());
    }

    @Test
    public void testDownloadResourceWithInvalidPermission() throws Exception {
        // Mock dependencies
        ResourceRepository mockResourceRepository = Mockito.mock(ResourceRepository.class);
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        CheckPermissionResource mockCheckPermissionResource = Mockito.mock(CheckPermissionResource.class);

        // Setup mocks
        Resource document = Resource.builder().id(1L).build(); // create a dummy resource
        User userLoggedIn = new User(); // create a dummy user
        Mockito.when(resourceRepository.findByResourceSrc("fileName")).thenReturn(Optional.of(document));
        Mockito.when(mockUserHelper.getUserLogin()).thenReturn(null);
        Mockito.when(mockCheckPermissionResource.needCheckPermissionResource(userLoggedIn, document, PermissionResourceType.D))
                .thenReturn(false);

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.downloadResource("fileName"));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Access denied, You have no permission", actualThrow.getError().getMessage());
    }

    @Test
    public void testDownloadResourceWithFileDownloadError() throws Exception {
        // Mock dependencies
        ResourceRepository mockResourceRepository = Mockito.mock(ResourceRepository.class);
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        CheckPermissionResource mockCheckPermissionResource = Mockito.mock(CheckPermissionResource.class);
        FileService mockFileService = Mockito.mock(FileService.class);

        // Setup mocks
        Resource document = Resource.builder().id(1L).build(); // create a dummy resource
        User userLoggedIn = new User(); // create a dummy user
        Mockito.when(mockResourceRepository.findByResourceSrc("fileName")).thenReturn(Optional.of(document));
        Mockito.when(mockUserHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(mockCheckPermissionResource.needCheckPermissionResource(userLoggedIn, document, PermissionResourceType.D))
                .thenReturn(true);
        Mockito.when(mockFileService.downloadFile("fileName")).thenReturn(any());

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.downloadResource("fileName"));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
//        assertEquals("File download error", actualThrow.getError().getMessage());
    }

    @Test
    public void testDownloadResourceWithInternalServerError() throws Exception {
        // Mock dependencies
        ResourceRepository mockResourceRepository = Mockito.mock(ResourceRepository.class);
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        CheckPermissionResource mockCheckPermissionResource = Mockito.mock(CheckPermissionResource.class);
        FileService mockFileService = Mockito.mock(FileService.class);

        // Setup mocks
        Resource document = new Resource(); // create a dummy resource
        User userLoggedIn = new User(); // create a dummy user
        Mockito.when(mockResourceRepository.findByResourceSrc("fileName")).thenReturn(Optional.of(document));
        Mockito.when(mockUserHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(mockCheckPermissionResource.needCheckPermissionResource(userLoggedIn, document, PermissionResourceType.D))
                .thenReturn(true);
        Mockito.when(mockFileService.downloadFile("fileName")).thenThrow(new RuntimeException());

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.downloadResource("fileName"));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
    }

    @Test
    public void testShareResourceWithResourceNotFound() throws Exception {
        // Mock dependencies
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        ResourceRepository mockResourceRepository = Mockito.mock(ResourceRepository.class);

        // Setup mocks
        ResourceSharedDTORequest request = new ResourceSharedDTORequest();
        User userLoggedIn = new User(); // create a dummy user

        Mockito.when(mockUserHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(mockResourceRepository.findById(request.getResourceId())).thenReturn(Optional.empty());

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.shareResource(request));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Resource is not found", actualThrow.getError().getMessage());
    }


    @Test
    public void testViewResourceShareWithResourceNotFound() throws Exception {
        // Mock dependencies
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        ResourceRepository mockResourceRepository = Mockito.mock(ResourceRepository.class);

        // Setup mocks
        Long resourceId = 1L; // dummy resource id
        User userLoggedIn = new User(); // create a dummy logged-in user

        Mockito.when(mockUserHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(mockResourceRepository.findById(resourceId)).thenReturn(Optional.empty());

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.viewResourceShareById(resourceId));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Resource is not found", actualThrow.getError().getMessage());
    }

    @Test
    public void testViewResourceShareWithNoPermission() throws Exception {
        // Mock dependencies
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        ResourceRepository mockResourceRepository = Mockito.mock(ResourceRepository.class);
        UserResourcePermissionRepository mockUserResourcePermissionRepository = Mockito.mock(UserResourcePermissionRepository.class);
        CheckPermissionResource mockCheckPermissionResource = Mockito.mock(CheckPermissionResource.class);

        // Setup mocks
        Long resourceId = 1L; // dummy resource id
        User userLoggedIn = new User(); // create a dummy logged-in user
        User ownerResource = new User(); // create a dummy owner user
        Resource resource = new Resource(); // create a dummy resource

        Mockito.when(mockUserHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(mockUserResourcePermissionRepository.getUserOwnerResource(resourceId)).thenReturn(ownerResource);
        Mockito.when(mockResourceRepository.findById(resourceId)).thenReturn(Optional.of(resource));
        Mockito.when(checkPermissionResource.needCheckPermissionResource(userLoggedIn, resource, PermissionResourceType.C))
                .thenReturn(false);

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.viewResourceShareById(resourceId));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
//        assertEquals("Access denied, You have no permission", actualThrow.getError().getMessage());
    }

    @Test
    public void testSuggestionUserShareWithValidText() throws Exception {
        // Mock dependencies
        // Setup mocks
        String text = "John"; // dummy search text
        User userLoggedIn = new User(); // create a dummy logged-in user
        List<User> users = new ArrayList<>(); // create a dummy user list

        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(userRoleRepository.findTeacherByUsernameOrEmailContaining(text, userLoggedIn.getId())).thenReturn(users);

        // Call the method
        List<UserSharedDTOResponse> suggestions = resourceService.suggestionUserShare(text);

        // Assert results
        Assertions.assertNotNull(suggestions);
    }


    @Test
    public void testGetListTagsSuggestWithExistingTags() throws Exception {
        // Mock dependencies
        TagRepository mockTagRepository = Mockito.mock(TagRepository.class);

        // Setup mocks
        String tagSuggest = "math"; // search parameter
        List<Tag> tags = new ArrayList<>(); // create dummy tags with the search text
        tags.add(Tag.builder().id(1L).name(tagSuggest).active(true).build());

        Mockito.when(mockTagRepository.findAllByNameContainsAndActive(tagSuggest)).thenReturn(tags);

        // Call the method
        List<TagSuggestDTOResponse> responses = resourceService.getListTagsSuggest(TagSuggestDTORequest.builder().tagSuggest(tagSuggest).build());
    }


    @Test
    public void testGetListTagsGlobalSuggest_Success() throws Exception {
        // Mock dependencies
        TagRepository mockTagRepository = Mockito.mock(TagRepository.class);

        // Setup mocks
        String tagSuggest = "math"; // search parameter
        List<Tag> tags = new ArrayList<>(); // create dummy tags with the search text
        tags.add(Tag.builder().id(1L).name(tagSuggest).active(true).build());

        Mockito.when(mockTagRepository.findGlobalTagByNameContainsAndActive(tagSuggest))
                .thenReturn(tags);

        // Call the method
        TagSuggestDTORequest request = new TagSuggestDTORequest(tagSuggest);
        List<TagSuggestDTOResponse> actualResponses = resourceService.getListTagsGlobalSuggest(request);
    }

    @Test
    public void testViewResourceWithResourceNotFound_Fail() throws Exception {
        // Mock dependencies
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        ResourceRepository mockResourceRepository = Mockito.mock(ResourceRepository.class);

        // Setup mocks
        Long resourceId = 1L;

        Mockito.when(mockUserHelper.getUserLogin()).thenReturn(new User());
        Mockito.when(mockResourceRepository.findByIdAndActiveTrue(resourceId)).thenReturn(Optional.empty());

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.viewResource(resourceId));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Resource is not found", actualThrow.getError().getMessage());
    }

    @Test
    public void testViewResourceWithNoPermission_Fail() throws Exception {
        // Mock dependencies
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        ResourceRepository mockResourceRepository = Mockito.mock(ResourceRepository.class);
        CheckPermissionResource mockCheckPermissionResource = Mockito.mock(CheckPermissionResource.class);

        // Setup mocks
        Long resourceId = 1L;
        User user = new User(); // logged-in user
        User author = new User(); // author of the resource (different from user)
        Resource resource = new Resource(); // dummy resource

        Mockito.when(mockUserHelper.getUserLogin()).thenReturn(user);
        Mockito.when(mockResourceRepository.findByIdAndActiveTrue(resourceId)).thenReturn(Optional.of(resource));
        Mockito.when(resource.getAuthor()).thenReturn(author);
        Mockito.when(mockCheckPermissionResource.needCheckPermissionResource(user, resource, PermissionResourceType.V))
                .thenReturn(true); // user needs permission check
        Mockito.when(checkPermissionResource.checkPermissionResourceType(user, resource, PermissionResourceType.V))
                .thenReturn(false); // user doesn't have permission

        // Call the method
        ApiException actualThrow = assertThrows(ApiException.class, () -> resourceService.viewResource(resourceId));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Access denied, You have no permission", actualThrow.getError().getMessage());
    }
}