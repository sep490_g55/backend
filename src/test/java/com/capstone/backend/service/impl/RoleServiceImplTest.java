package com.capstone.backend.service.impl;

import com.capstone.backend.entity.Role;
import com.capstone.backend.entity.SystemPermission;
import com.capstone.backend.entity.User;
import com.capstone.backend.entity.UserRolePermission;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.role.RoleDTODetailResponse;
import com.capstone.backend.model.dto.role.RoleDTORequest;
import com.capstone.backend.model.dto.role.RoleDTOUpdate;
import com.capstone.backend.model.dto.role.UserRoleDTOResponse;
import com.capstone.backend.repository.RoleRepository;
import com.capstone.backend.repository.SystemPermissionRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.UserRolePermissionRepository;
import com.capstone.backend.repository.criteria.RoleCriteria;
import com.capstone.backend.utils.DataHelper;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.S3Util;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
class RoleServiceImplTest {
    @InjectMocks
    RoleServiceImpl roleService;
    @Mock
    RoleRepository roleRepository;
    @Mock
    RoleCriteria roleCriteria;
    @Mock
    UserRolePermissionRepository userRolePermissionRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @Mock
    UserHelper userHelper;
    @Mock
    SystemPermissionRepository systemPermissionRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void viewSearchRole() {
    }

    @Test
    void getRoleById_id_not_found() {
        Long id = 1L;
        Role role = Role.builder()
                .userId(1L)
                .id(1L)
                .build();

        when(roleRepository.findById(id)).thenReturn(Optional.empty());
        ApiException actualThrow = assertThrows(ApiException.class, () -> roleService.getRoleById(1L));
        verify(roleRepository, times(1)).findById(id);

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Role is not found", actualThrow.getError().getMessage());
    }

    @Test
    void getRoleById_success() {
        Long id = 1L;

        Role role = Role.builder()
                .userId(1L)
                .id(1L)
                .build();

        User user = User.builder()
                .id(1L)
                .build();

        SystemPermission systemPermission = SystemPermission.builder()
                .id(1L)
                .build();

        UserRolePermission urp = UserRolePermission.builder()
                .id(1L)
                .build();

        when(roleRepository.findById(id)).thenReturn(Optional.of(role));
        when(systemPermissionRepository.findById(id)).thenReturn(Optional.of(systemPermission));
        when(userRepository.findUsernameByUserId(id)).thenReturn("username");
        when(userRolePermissionRepository.save(any(UserRolePermission.class))).thenReturn(any());
        RoleDTODetailResponse response = roleService.getRoleById(id);
    }

    @Test
    void createRole_success() {
        Long id = 1L;

        RoleDTORequest request = RoleDTORequest.builder()
                .roleName("role")
                .build();

        User user = User.builder()
                .id(1L)
                .build();

        Role role = Role.builder()
                .userId(2L)
                .id(1L)
                .build();

        SystemPermission systemPermission = SystemPermission.builder()
                .id(1L)
                .build();

        UserRolePermission urp = UserRolePermission.builder()
                .id(1L)
                .build();

        when(userHelper.getUserLogin()).thenReturn(user);
        when(roleRepository.save(any(Role.class))).thenReturn(role);
        when(systemPermissionRepository.findById(1L)).thenReturn(Optional.of(systemPermission));
        when(userRolePermissionRepository.save(urp)).thenReturn(urp);
        when(userRepository.findUsernameByUserId(1L)).thenReturn("username");
        RoleDTODetailResponse response = roleService.createRole(request);
    }

    @Test
    public void createRole_duplicate_name() {
        // Arrange
        RoleDTORequest request = new RoleDTORequest();
        request.setRoleName("Test Role");

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);

        Optional<Role> existingRole = Optional.of(new Role());
        Mockito.when(roleRepository.findByName(request.getRoleName(), 0L)).thenReturn(existingRole);

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> roleService.createRole(request));

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Duplicate role name", actualThrow.getError().getMessage());

        // Verify interactions
        Mockito.verify(roleRepository, times(0)).save(Mockito.any(Role.class));
    }

    @Test
    public void updateRole_shouldThrowExceptionForDuplicateRoleName() {
        // Arrange
        RoleDTOUpdate request = new RoleDTOUpdate();
        request.setRoleId(1L);
        request.setRoleName("Test Role");

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);

        Mockito.when(roleRepository.findByName(request.getRoleName(), request.getRoleId()))
                .thenReturn(Optional.of(Role.builder().id(1L).build()));

        // Assertions for ApiException properties
        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> roleService.updateRole(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Duplicate role name", actualThrow.getError().getMessage());

        // Verify interactions
        Mockito.verify(roleRepository, times(0)).save(Mockito.any(Role.class));
    }

    @Test
    public void changeStatus_shouldThrowExceptionForRoleIdNotFound() {
        // Arrange
        Long roleId = 1L;

        // Mock dependencies
        Mockito.when(roleRepository.findById(roleId)).thenReturn(Optional.empty());

        // Verify interactions
        ApiException actualThrow = assertThrows(ApiException.class, () -> roleService.changeStatus(roleId));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_ROLE_NOT_FOUND, actualThrow.getError().getMessage());

        // Verify interactions
        Mockito.verify(roleRepository, times(0)).save(Mockito.any(Role.class));
    }

    @Test
    public void changeStatus_shouldChangeStatusSuccessfully() {
        // Arrange
        Long roleId = 1L;

        // Mock dependencies
        Role existingRole = new Role();
        existingRole.setId(roleId);
        existingRole.setActive(false);
        Mockito.when(roleRepository.findById(roleId)).thenReturn(Optional.of(existingRole));

        // Act
        boolean statusChanged = roleService.changeStatus(roleId);

        // Assert
        assertTrue(statusChanged);
        assertTrue(existingRole.getActive());

        // Verify interactions
        Mockito.verify(roleRepository, times(1)).save(existingRole);
    }

    @Test
    public void getListRoleUser_shouldReturnListRoleUser() {
        // Arrange
        S3Util s3Util = Mockito.mock(S3Util.class);

        // Mock dependencies
        User user = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(user);

        List<Role> roles = new ArrayList<>();
        Role role1 = new Role();
        role1.setId(1L);
        role1.setName("Test Role 1");
        Role role2 = new Role();
        role2.setId(2L);
        role2.setName("Test Role 2");
        roles.add(role1);
        roles.add(role2);

        Mockito.when(roleRepository.findAllByUserAndActive(user)).thenReturn(roles);
        Mockito.mockStatic(DataHelper.class);
        Mockito.when(DataHelper.getLinkAvatar(user, s3Util)).thenReturn("https://example.com/avatar.jpg");
        // Act
        UserRoleDTOResponse response = roleService.getListRoleUser(s3Util);

        // Assert
        assertEquals(user.getId(), response.getUserId());
        assertEquals("https://example.com/avatar.jpg", response.getAvatar());
        assertEquals(user.getEmail(), response.getEmail());
        assertEquals(user.getUsername(), response.getUsername());
        assertEquals(2, response.getRoleDTODisplays().size());
        assertEquals(role1.getId(), response.getRoleDTODisplays().get(0).getRoleId());
        assertEquals(role1.getName(), response.getRoleDTODisplays().get(0).getRoleName());
        assertEquals(role2.getId(), response.getRoleDTODisplays().get(1).getRoleId());
        assertEquals(role2.getName(), response.getRoleDTODisplays().get(1).getRoleName());

        // Verify interactions
        Mockito.verify(roleRepository, times(1)).findAllByUserAndActive(user);
    }
}