package com.capstone.backend.service.impl;

import com.capstone.backend.entity.Comment;
import com.capstone.backend.entity.Resource;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.comment.CommentDTORequest;
import com.capstone.backend.model.dto.comment.CommentDTOResponse;
import com.capstone.backend.model.dto.comment.CommentDetailDTOResponse;
import com.capstone.backend.repository.CommentRepository;
import com.capstone.backend.repository.ResourceRepository;
import com.capstone.backend.repository.criteria.CommentCriteria;
import com.capstone.backend.utils.FileHelper;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.S3Util;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
@MockitoSettings(strictness = Strictness.LENIENT)
class CommentServiceImplTest {
    @Mock
    CommentRepository commentRepository;
    @Mock
    ResourceRepository resourceRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserHelper userHelper;
    @Mock
    S3Util s3Util;
    @Mock
    CommentCriteria commentCriteria;
    @InjectMocks
    CommentServiceImpl commentService;

    User userLogged;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung");
        userLogged.setId(1L);
    }

    @Test
    void getListCommentDetailDTOResponse() {
    }

    @Test
    void changeStatus_When_True_to_False() {
        Long id = 1L;
        User user = User.builder().id(1L).username("dung").build();

        Comment comment = Comment.builder()
                .commentId(1L)
                .commenter(user)
                .createdAt(LocalDateTime.now())
                .content("Hay day")
                .active(true)
                .build();
        Mockito.when(commentRepository.findById(id)).thenReturn(Optional.of(comment));
        // test accept
        Mockito.when(commentRepository.save(Mockito.any())).thenReturn(comment);
        commentService.changeStatus(id);
//        assertTrue(result);
        assertEquals(comment.getActive(), false);
    }

    @Test
    void changeStatus_When_False_to_True() {
        Long id = 1L;
        User user = User.builder().id(1L).username("dung").build();

        Comment comment = Comment.builder()
                .commentId(1L)
                .commenter(user)
                .createdAt(LocalDateTime.now())
                .content("Hay day")
                .active(false)
                .build();
        Mockito.when(commentRepository.findById(id)).thenReturn(Optional.of(comment));
        // test accept
        Mockito.when(commentRepository.save(Mockito.any())).thenReturn(comment);
        commentService.changeStatus(id);
//        assertTrue(result);
        assertEquals(comment.getActive(), true);
    }

    @Test
    public void testCreateCommentResourceNotFound() {
        // Arrange
        User mockUser = Mockito.mock(User.class);
        CommentDTORequest mockRequest = Mockito.mock(CommentDTORequest.class);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUser);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> commentService.createComment(mockRequest));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_USER_RESOURCE_NOT_FOUND, actualThrow.getError().getMessage());
    }

    @Test
    public void testCreateCommentInvalidContent() {
        // Arrange
        User mockUser = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);
        CommentDTORequest mockRequest = Mockito.mock(CommentDTORequest.class);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUser);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));
        Mockito.mockStatic(FileHelper.class);
        Mockito.when(FileHelper.checkContentInputValid(mockRequest.getContent())).thenReturn(false);

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> commentService.createComment(mockRequest));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_TEXT_NO_STANDARD_WORD, actualThrow.getError().getMessage());
    }

    @Test
    public void testCreateCommentCommentRootNotFound() {
        // Arrange
        User mockUser = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);
        CommentDTORequest mockRequest = Mockito.mock(CommentDTORequest.class);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUser);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));
        Mockito.when(commentRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> commentService.createComment(mockRequest));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_COMMENT_ROOT_ID_NOT_FOUND, actualThrow.getError().getMessage());
    }

    @Test
    public void testGetListCommentDetailDTOResponseEmptyCommentList() {
        // Arrange
        List<Comment> mockComments = new ArrayList<>();

        Mockito.when(commentRepository.findByResourceIdAndCommentRootIdIsNull(1L)).thenReturn(mockComments);

        // Act
        List<CommentDetailDTOResponse> result = commentService.getListCommentDetailDTOResponse(1L);

        // Assert
        Mockito.verify(commentRepository, times(1)).findByResourceIdAndCommentRootIdIsNull(1L);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    public void testSeeMoreReplyCommentEmptyReplyCommentList() {
        // Arrange
        List<Comment> mockComments = new ArrayList<>();

        Mockito.when(commentRepository.findAllCommentReply(1L)).thenReturn(mockComments);

        // Act
        List<CommentDTOResponse> result = commentService.seeMoreReplyComment(1L);

        // Assert
        Mockito.verify(commentRepository, times(1)).findAllCommentReply(1L);
        Assertions.assertEquals(0, result.size());
    }

    @Test
    public void testGetReportDetailCommentCommentNotFound() {
        // Arrange
        Mockito.when(commentRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> commentService.getReportDetailComment(1L));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_COMMENT_NOT_FOUND, actualThrow.getError().getMessage());
    }


    @Test
    public void testChangeStatusSuccess() {
        // Arrange
        Comment mockComment = Mockito.mock(Comment.class);
        Mockito.when(commentRepository.findById(1L)).thenReturn(Optional.of(mockComment));

        // Act
        boolean result = commentService.changeStatus(1L);

        // Assert
        Mockito.verify(commentRepository, times(1)).findById(1L);
        Mockito.verify(commentRepository, times(1)).save(mockComment);
        Assertions.assertTrue(result);
    }

    @Test
    public void testChangeStatusCommentNotFound() {
        // Arrange
        Mockito.when(commentRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> commentService.changeStatus(1L));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_COMMENT_NOT_FOUND, actualThrow.getError().getMessage());
    }
}