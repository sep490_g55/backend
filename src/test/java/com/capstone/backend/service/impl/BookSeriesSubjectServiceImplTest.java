package com.capstone.backend.service.impl;

import com.capstone.backend.entity.BookSeries;
import com.capstone.backend.entity.BookSeriesSubject;
import com.capstone.backend.entity.Subject;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.bookseriesSubject.BookSeriesSubjectDTOFilter;
import com.capstone.backend.model.dto.bookseriesSubject.BookSeriesSubjectDTOResponse;
import com.capstone.backend.model.dto.subject.SubjectShowDTOResponse;
import com.capstone.backend.repository.BookSeriesRepository;
import com.capstone.backend.repository.BookSeriesSubjectRepository;
import com.capstone.backend.repository.SubjectRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.BookSeriesSubjectCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BookSeriesSubjectServiceImplTest {

    @InjectMocks
    BookSeriesSubjectServiceImpl bookSeriesSubjectService;
    @Mock
    BookSeriesRepository bookSeriesRepository;
    @Mock
    SubjectRepository subjectRepository;
    @Mock
    BookSeriesSubjectRepository bookSeriesSubjectRepository;
    @Mock
    BookSeriesSubjectCriteria bookSeriesSubjectCriteria;
    @Mock
    MessageException messageException;
    @Mock
    UserHelper userHelper;
    @Mock
    UserRepository userRepository;

    User userLogged;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung");
        userLogged.setId(1L);
    }

    @Test
    void changeSubjectInBookSeries_When_Success() {
        List<Long> subjects = new ArrayList<>();
        subjects.add(2L);
        subjects.add(3L);
        Long bookSeriesId = 1L;
        List<BookSeriesSubject> bookSeriesSubjects = new ArrayList<>();
        Subject subject = Subject.builder()
                .id(1L)
                .build();
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .id(1L)
                .subject(subject)
                .build();
        bookSeriesSubjects.add(bookSeriesSubject);
        BookSeries bookSeries = BookSeries.builder()
                .bookSeriesSubjects(bookSeriesSubjects)
                .build();
        Mockito.when(bookSeriesRepository.findById(bookSeriesId)).thenReturn(Optional.of(bookSeries));
        Mockito.when(bookSeriesSubjectRepository.saveAll(Mockito.any())).thenReturn(new ArrayList<>());
        Mockito.when(bookSeriesSubjectRepository.findBySubjectAndBookSeries(Mockito.any(), Mockito.any(), true))
                .thenReturn(new BookSeriesSubject());
        Mockito.verify(Mockito
                                .mock(BookSeriesSubjectRepository.class),
                        Mockito.times(0))
                .deleteAll(Mockito.any());
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(subjectRepository.findById(Mockito.any())).thenReturn(Optional.of(new Subject()));
        Boolean rs = bookSeriesSubjectService.changeSubjectInBookSeries(subjects, bookSeriesId);
        assertTrue(rs);
    }

    @Test
    void changeSubjectInBookSeries_When_Exception() {
        List<Long> subjects = new ArrayList<>();
        subjects.add(2L);
        subjects.add(3L);
        Long bookSeriesId = 1L;
        List<BookSeriesSubject> bookSeriesSubjects = new ArrayList<>();
        Subject subject = Subject.builder()
                .id(1L)
                .build();
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .id(1L)
                .subject(subject)
                .build();
        bookSeriesSubjects.add(bookSeriesSubject);
        Mockito.when(bookSeriesRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            bookSeriesSubjectService.changeSubjectInBookSeries(subjects, bookSeriesId);
        }, messageException.MSG_BOOK_SERIES_NOT_FOUND);
    }

    @Test
    void toBookSeriesSubject() {
        BookSeries bs = BookSeries.builder()
                .name("BookSeries")
                .build();
        Long subjectId = 1L;
        Subject subject = Subject.builder()
                .id(1L)
                .name("Abc")
                .build();
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(subjectRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(subject));
        BookSeriesSubject bookSeriesSubject = bookSeriesSubjectService.toBookSeriesSubject(bs, subjectId);
        assertEquals(bookSeriesSubject.getSubject().getId(), 1L);
        assertEquals(bookSeriesSubject.getSubject().getName(), "Abc");
        assertEquals(bookSeriesSubject.getActive(), true);
        assertEquals(bookSeriesSubject.getBookSeries().getName(), "BookSeries");
        assertEquals(bookSeriesSubject.getUserId(), userLogged.getId());
    }

    @Test
    void viewBookSeriesSubjectById() {
        Long bookSeriesId = 1L;
        Subject subject = Subject.builder().name("subject").build();
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .subject(subject)
                .bookVolumes(new ArrayList<>()).build();
        bookSeriesSubjectList.add(bookSeriesSubject);
        BookSeries bookSeries = BookSeries.builder()
                .bookSeriesSubjects(bookSeriesSubjectList)
                .build();
        Mockito.when(bookSeriesRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(bookSeries));
        List<SubjectShowDTOResponse> rs = bookSeriesSubjectService.viewBookSeriesSubjectById(bookSeriesId);
        assertEquals(rs.size(), 1);
        assertEquals(rs.get(0).getName(), "subject");
    }

    @Test
    void viewBookSeriesSubjectById_When_NotFound() {
        Long bookSeriesId = 1L;
        Mockito.when(bookSeriesRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            bookSeriesSubjectService.viewBookSeriesSubjectById(bookSeriesId);
        }, "BookSeries is not found");
    }

    @Test
    void searchBookSeriesSubject() {
        BookSeries bs = BookSeries.builder().build();
        BookSeriesSubjectDTOFilter bookSeriesSubjectDTOFilter = BookSeriesSubjectDTOFilter.builder().build();
        Long bookSeriesId = 1L;
        PagingDTOResponse pagingDTOResponse = PagingDTOResponse.builder()
                .totalPage(10L)
                .totalElement(100L)
                .data(new ArrayList<>())
                .build();
        Mockito.when(bookSeriesRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(bs));
        Mockito.when(bookSeriesSubjectCriteria.searchBookSeriesSubject(Mockito.any(), Mockito.any()))
                .thenReturn(pagingDTOResponse);
        PagingDTOResponse rs = bookSeriesSubjectService.searchBookSeriesSubject(bookSeriesSubjectDTOFilter, bookSeriesId);
        assertEquals(rs.getTotalPage(), 10L);
        assertEquals(rs.getTotalElement(), 100L);
    }

    @Test
    void searchBookSeriesSubject_WhenNotFound() {
        BookSeries bs = BookSeries.builder().build();
        BookSeriesSubjectDTOFilter bookSeriesSubjectDTOFilter = BookSeriesSubjectDTOFilter.builder().build();
        Long bookSeriesId = 1L;
        PagingDTOResponse pagingDTOResponse = PagingDTOResponse.builder()
                .totalPage(10L)
                .totalElement(100L)
                .data(new ArrayList<>())
                .build();
        Mockito.when(bookSeriesRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            bookSeriesSubjectService.viewBookSeriesSubjectById(bookSeriesId);
        }, messageException.MSG_BOOK_SERIES_NOT_FOUND);
    }

    @Test
    void getListBookSeriesSubject() {
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        Subject subject = Subject.builder().name("ABCD").build();
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder().id(10L).subject(subject).build();
        bookSeriesSubjectList.add(bookSeriesSubject);
        Mockito.when(bookSeriesSubjectRepository.findAll()).thenReturn(bookSeriesSubjectList);
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        List<BookSeriesSubjectDTOResponse> bookSeriesSubjectDTOResponseList = bookSeriesSubjectService.getListBookSeriesSubject();
        assertEquals(bookSeriesSubjectDTOResponseList.size(), 1);
        assertEquals(bookSeriesSubjectDTOResponseList.get(0).getId(), 10L);
    }

    @Test
    void getListBookSeriesSubject_When_UserNotFound() {
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        Subject subject = Subject.builder().name("ABCD").build();
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder().id(10L).subject(subject).build();
        bookSeriesSubjectList.add(bookSeriesSubject);
        Mockito.when(bookSeriesSubjectRepository.findAll()).thenReturn(bookSeriesSubjectList);
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(NoSuchElementException.class, () -> {
            bookSeriesSubjectService.getListBookSeriesSubject();
        }, messageException.MSG_BOOK_SERIES_NOT_FOUND);
    }
}