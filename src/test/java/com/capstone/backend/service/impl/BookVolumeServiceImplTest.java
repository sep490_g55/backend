package com.capstone.backend.service.impl;

import com.capstone.backend.entity.*;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.bookvolume.BookVolumeDTOFilter;
import com.capstone.backend.model.dto.bookvolume.BookVolumeDTORequest;
import com.capstone.backend.model.dto.bookvolume.BookVolumeDTOResponse;
import com.capstone.backend.repository.BookSeriesSubjectRepository;
import com.capstone.backend.repository.BookVolumeRepository;
import com.capstone.backend.repository.ChapterRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.BookVolumeCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class BookVolumeServiceImplTest {
    @Mock
    BookVolumeRepository bookVolumeRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @Mock
    ChapterRepository chapterRepository;
    @Mock
    BookSeriesSubjectRepository bookSeriesSubjectRepository;
    @Mock
    BookVolumeCriteria bookVolumeCriteria;
    @Mock
    UserHelper userHelper;

    @InjectMocks
    BookVolumeServiceImpl bookVolumeService;
    User userLogged;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung");
        userLogged.setId(1L);
    }

    @Test
    void createBookVolume() {
        BookSeriesSubject bookSeriesSubject = new BookSeriesSubject();
        bookSeriesSubject.setId(1L);

        BookVolume bookVolume = BookVolume.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Tap 1")
                .userId(userLogged.getId())
                .bookSeriesSubject(bookSeriesSubject)
                .build();
        Mockito.when(bookVolumeRepository.save(bookVolume)).thenReturn(bookVolume);
        assertEquals(bookVolume, bookVolumeRepository.save(bookVolume));
    }

    @Test
    void createBookVolume_When_DuplicateName() {
        // input
        BookVolumeDTORequest request = new BookVolumeDTORequest("Tập 2");
        BookSeriesSubject bookSeriesSubject = new BookSeriesSubject();
        bookSeriesSubject.setId(1L);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(bookVolumeRepository.findByName(Mockito.eq("Tập 2"), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(new BookVolume()));
        // test accept
        assertThrows(ApiException.class, () -> {
            bookVolumeService.createBookVolume(request, bookSeriesSubject.getId());
        }, "Duplicate bookVolume name");
    }

    @Test
    void createBookVolume_When_Success() {
        BookVolumeDTOResponse bookVolumeDTOResponse = new BookVolumeDTOResponse();
        bookVolumeDTOResponse.setName("Tập 3");

        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Rap11")
                .userId(1L)
                .build();

        Subject subject = Subject.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Toan Hinh")
                .userId(userLogged.getId())
                .build();

        BookSeriesSubject bookSeriesSubject = new BookSeriesSubject();
        bookSeriesSubject.setId(1L);
        bookSeriesSubject.setActive(true);
        bookSeriesSubject.setBookSeries(bookSeries);
        bookSeriesSubject.setSubject(subject);

        BookVolumeDTORequest request = new BookVolumeDTORequest("Tập 3");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        BookVolume bookVolume = BookVolume.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .bookSeriesSubject(bookSeriesSubject)
                .build();

        Mockito.when(bookVolumeRepository.findByName(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.empty());
        Mockito.when(bookSeriesSubjectRepository.findById(Mockito.any())).thenReturn(Optional.of(new BookSeriesSubject()));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        Mockito.when(bookVolumeRepository.save(Mockito.any())).thenReturn(bookVolume);
        bookVolumeDTOResponse = BookVolumeDTOResponse.builder()
                .name(bookVolume.getName())
                .id(bookVolume.getId())
                .active(bookVolume.getActive())
                .createdAt(bookVolume.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        BookVolumeDTOResponse bookVolumeDTOResponse1 = bookVolumeService.createBookVolume(request, 2L);
        assertEquals(bookVolumeDTOResponse1.getCreator(), bookVolumeDTOResponse.getCreator());
        assertEquals(bookVolumeDTOResponse1.getCreatedAt(), bookVolumeDTOResponse.getCreatedAt());
        assertEquals(bookVolumeDTOResponse1.getId(), bookVolumeDTOResponse.getId());
        assertEquals(bookVolumeDTOResponse1.getName(), bookVolumeDTOResponse.getName());

    }

    @Test
    void updateVolume_When_requestNull() {
        // input
        BookVolumeDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            bookVolumeService.updateBookVolume(1L, request);
        }, "NullPointerException");
    }

    @Test
    void update_with_duplicate_BookVolume() {
        BookVolumeDTORequest request = new BookVolumeDTORequest("Tap 1");
        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Rap11")
                .userId(1L)
                .build();

        Subject subject = Subject.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Toan Hinh")
                .userId(userLogged.getId())
                .build();

        BookSeriesSubject bookSeriesSubject = new BookSeriesSubject();
        bookSeriesSubject.setId(1L);
        bookSeriesSubject.setActive(true);
        bookSeriesSubject.setBookSeries(bookSeries);
        bookSeriesSubject.setSubject(subject);

        Long id = 1L;
        BookVolume bookVolume = BookVolume.builder().bookSeriesSubject(bookSeriesSubject).build();
        BookVolume bookVolume1 = BookVolume.builder().build();
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(bookVolumeRepository.findById(Mockito.eq(id)))
                .thenReturn(Optional.of(bookVolume));
        Mockito.when(bookVolumeRepository.findByName(
                        Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(bookVolume1));
        // test accept
        assertThrows(ApiException.class, () -> {
            bookVolumeService.updateBookVolume(id, request);
        }, "Duplicate book Volume name");

    }

    @Test
    void update_with_Success() {
        BookVolumeDTOResponse bookVolumeDTOResponse = new BookVolumeDTOResponse();
        bookVolumeDTOResponse.setName("Tập 3");

        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Rap11")
                .userId(1L)
                .build();

        Subject subject = Subject.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Toan Hinh")
                .userId(userLogged.getId())
                .build();

        BookSeriesSubject bookSeriesSubject = new BookSeriesSubject();
        bookSeriesSubject.setId(1L);
        bookSeriesSubject.setActive(true);
        bookSeriesSubject.setBookSeries(bookSeries);
        bookSeriesSubject.setSubject(subject);

        BookVolumeDTORequest request = new BookVolumeDTORequest("Tập 3");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        BookVolume bookVolume = BookVolume.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .bookSeriesSubject(bookSeriesSubject)
                .build();

        Mockito.when(bookVolumeRepository.findByName(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.empty());
        Mockito.when(bookSeriesSubjectRepository.findById(Mockito.any())).thenReturn(Optional.of(new BookSeriesSubject()));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        Mockito.when(bookVolumeRepository.save(Mockito.any())).thenReturn(bookVolume);
        bookVolumeDTOResponse = BookVolumeDTOResponse.builder()
                .name(bookVolume.getName())
                .id(bookVolume.getId())
                .active(bookVolume.getActive())
                .createdAt(bookVolume.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        BookVolumeDTOResponse bookVolumeDTOResponse1 = bookVolumeService.updateBookVolume(2L, request);
        assertEquals(bookVolumeDTOResponse1.getCreator(), bookVolumeDTOResponse.getCreator());
        assertEquals(bookVolumeDTOResponse1.getCreatedAt(), bookVolumeDTOResponse.getCreatedAt());
        assertEquals(bookVolumeDTOResponse1.getId(), bookVolumeDTOResponse.getId());
        assertEquals(bookVolumeDTOResponse1.getName(), bookVolumeDTOResponse.getName());

    }

    @Test
    void changeStatus_When_ThrowEx() {
        Long id = 1L;
        BookSeriesSubject bookSeriesSubjectList = new BookSeriesSubject();

        BookVolume bookVolume = BookVolume.builder()
                .userId(userLogged.getId())
                .bookSeriesSubject(bookSeriesSubjectList)
                .chapterList(new ArrayList<>())
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("name")
                .build();
        Mockito.when(bookVolumeRepository.findById(id)).thenReturn(Optional.empty());
        // test accept
        assertThrows(ApiException.class, () -> {
            bookVolumeService.changeStatus(id);
        }, "Can not change status BookVolume because Chapter already exists");
    }

    @Test
    void changeStatus_When_Success_True_To_False() {
        Long id = 1L;
        BookSeriesSubject bookSeriesSubjectList = new BookSeriesSubject();

        BookVolume bookVolume = BookVolume.builder()
                .userId(userLogged.getId())
                .bookSeriesSubject(bookSeriesSubjectList)
                .chapterList(new ArrayList<>())
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("name")
                .build();
        Mockito.when(bookVolumeRepository.findById(id)).thenReturn(Optional.of(bookVolume));
        // test accept
        Mockito.when(bookVolumeRepository.save(Mockito.any())).thenReturn(bookVolume);
        bookVolumeService.changeStatus(id);
//        assertTrue(result);
        assertEquals(bookVolume.getActive(), false);
    }

    @Test
    void searchBookVolume_When_Ex() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Rap11")
                .userId(1L)
                .build();

        Subject subject = Subject.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Toan Hinh")
                .userId(userLogged.getId())
                .build();

        BookSeriesSubject bookSeriesSubject = new BookSeriesSubject();
        bookSeriesSubject.setId(1L);
        bookSeriesSubject.setActive(true);
        bookSeriesSubject.setBookSeries(bookSeries);
        bookSeriesSubject.setSubject(subject);
        BookVolumeDTOFilter bookSeriesDTOFilter = new BookVolumeDTOFilter();
        bookSeriesDTOFilter.setName("abcd");
        Mockito.when(bookVolumeRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            bookVolumeService.searchBookVolume(bookSeriesDTOFilter, bookSeriesSubject.getId());
        }, "messageException.MSG_USER_NOT_FOUND");
    }

    @Test
    void searchBookVolume() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();

        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Rap11")
                .userId(1L)
                .build();

        Subject subject = Subject.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Toan Hinh")
                .userId(userLogged.getId())
                .build();

        BookSeriesSubject bookSeriesSubject = new BookSeriesSubject();
        bookSeriesSubject.setId(1L);
        bookSeriesSubject.setActive(true);
        bookSeriesSubject.setBookSeries(bookSeries);
        bookSeriesSubject.setSubject(subject);

        Mockito.when(bookVolumeCriteria.searchBookVolume(Mockito.any(), Mockito.any())).thenReturn(response);
        Mockito.when(bookSeriesSubjectRepository.findById(Mockito.any())).thenReturn(Optional.of(new BookSeriesSubject()));
        BookVolumeDTOFilter bookVolumeDTOFilter = new BookVolumeDTOFilter();
        bookVolumeDTOFilter.setName("Cánh Diều");
        PagingDTOResponse pagingDTOResponse = bookVolumeService.searchBookVolume(bookVolumeDTOFilter, bookSeriesSubject.getId());
        assertEquals(pagingDTOResponse.getTotalElement(), pagingDTOResponse.getTotalElement());
        assertEquals(pagingDTOResponse.getTotalPage(), pagingDTOResponse.getTotalPage());
    }

    @Test
    void viewBookVolumeById() {
        BookVolumeDTOResponse bookVolumeDTOResponse = BookVolumeDTOResponse.builder().id(10L).build();
        Long id = 1L;

        BookSeries bookSeries = BookSeries.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Rap11")
                .userId(1L)
                .build();

        Subject subject = Subject.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Toan Hinh")
                .userId(userLogged.getId())
                .build();

        BookSeriesSubject bookSeriesSubject = new BookSeriesSubject();
        bookSeriesSubject.setId(1L);
        bookSeriesSubject.setActive(true);
        bookSeriesSubject.setBookSeries(bookSeries);
        bookSeriesSubject.setSubject(subject);

        BookVolume bookVolume = BookVolume.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Canh Dieu")
                .userId(userLogged.getId())
                .bookSeriesSubject(bookSeriesSubject)
                .build();

        Mockito.when(bookVolumeRepository.findById(id)).thenReturn(Optional.of(bookVolume));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        BookVolumeDTOResponse bookVolumeDTOResponse1 = bookVolumeService.viewBookVolumeById(id);
        assertEquals(bookVolumeDTOResponse1.getId(), 10L);
        assertEquals(bookVolumeDTOResponse1.getName(), "Canh Dieu");

    }

    @Test
    void viewBookVolumeById_When_BookVolumeNotFound() {

        Long id = 1L;
        BookVolume bookVolume = BookVolume.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name("Canh Dieu")
                .userId(userLogged.getId())
                .build();
        Mockito.when(bookVolumeRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            bookVolumeService.viewBookVolumeById(id);
        }, "BookVolume is not found");
    }
}