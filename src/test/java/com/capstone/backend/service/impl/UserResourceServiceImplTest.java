package com.capstone.backend.service.impl;

import com.capstone.backend.entity.Resource;
import com.capstone.backend.entity.User;
import com.capstone.backend.entity.UserResource;
import com.capstone.backend.entity.UserResourcePermission;
import com.capstone.backend.entity.type.ActionType;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.userresource.UserResourceRequest;
import com.capstone.backend.repository.ResourceRepository;
import com.capstone.backend.repository.UserResourcePermissionRepository;
import com.capstone.backend.repository.UserResourceRepository;
import com.capstone.backend.repository.criteria.UserResourceCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserResourceServiceImplTest {
    @Mock
    ResourceRepository resourceRepository;
    @Mock
    UserResourceRepository userResourceRepository;
    @Mock
    UserHelper userHelper;
    @Mock
    MessageException messageException;
    @Mock
    UserResourceCriteria userResourceCriteria;
    @Mock
    UserResourcePermissionRepository userResourcePermissionRepository;
    @InjectMocks
    UserResourceServiceImpl userResourceService;

    @Test
    public void testActionResourceLike() {
        // Arrange
        User mockUserLoggedIn = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);
        UserResourceRequest mockRequest = Mockito.mock(UserResourceRequest.class);

        Mockito.when(mockRequest.getResourceId()).thenReturn(1L);
        Mockito.when(mockRequest.getActionType()).thenReturn(ActionType.LIKE);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUserLoggedIn);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));

        // Act
        userResourceService.actionResource(mockRequest);

        // Assert
        Mockito.verify(userResourceRepository, times(1)).save(Mockito.any(UserResource.class));
    }

    @Test
    public void testActionResourceUnlike() {
        // Arrange
        User mockUserLoggedIn = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);
        UserResourceRequest mockRequest = Mockito.mock(UserResourceRequest.class);

        Mockito.when(mockRequest.getResourceId()).thenReturn(1L);
        Mockito.when(mockRequest.getActionType()).thenReturn(ActionType.UNLIKE);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUserLoggedIn);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));

        // Act
        userResourceService.actionResource(mockRequest);

        // Assert
        Mockito.verify(userResourceRepository, times(1)).save(Mockito.any(UserResource.class));
    }

    @Test
    public void testActionResourceSaved() {
        // Arrange
        User mockUserLoggedIn = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);
        UserResourceRequest mockRequest = Mockito.mock(UserResourceRequest.class);

        Mockito.when(mockRequest.getResourceId()).thenReturn(1L);
        Mockito.when(mockRequest.getActionType()).thenReturn(ActionType.SAVED);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUserLoggedIn);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));

        // Act
        userResourceService.actionResource(mockRequest);

        // Assert
        Mockito.verify(userResourceRepository, times(1)).save(Mockito.any(UserResource.class));
    }

    @Test
    public void testActionResourceUnsaved() {
        // Arrange
        User mockUserLoggedIn = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);
        UserResourceRequest mockRequest = Mockito.mock(UserResourceRequest.class);

        Mockito.when(mockRequest.getResourceId()).thenReturn(1L);
        Mockito.when(mockRequest.getActionType()).thenReturn(ActionType.UNSAVED);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUserLoggedIn);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));

        // Act
        userResourceService.actionResource(mockRequest);

        // Assert
        Mockito.verify(userResourceRepository, times(0)).save(Mockito.any(UserResource.class));
    }

    @Test
    public void testActionResourceDownload() {
        // Arrange
        User mockUserLoggedIn = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);
        UserResourceRequest mockRequest = Mockito.mock(UserResourceRequest.class);

        Mockito.when(mockRequest.getResourceId()).thenReturn(1L);
        Mockito.when(mockRequest.getActionType()).thenReturn(ActionType.DOWNLOAD);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUserLoggedIn);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));

        // Act
        userResourceService.actionResource(mockRequest);

        // Assert
        Mockito.verify(userResourceRepository, times(1)).save(Mockito.any(UserResource.class));
    }

    @Test
    void viewSearchUserResourceSaved() {
    }

    @Test
    void viewSearchUserResourceShared() {
    }

    @Test
    void viewSearchMyUserResource() {
    }

    @Test
    public void testDeleteSavedResourceSuccess() {
        // Arrange
        User mockUser = Mockito.mock(User.class);
        UserResource mockUserResource = Mockito.mock(UserResource.class);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUser);
        Mockito.when(userResourceRepository.findUserResourceHasActionType(mockUser.getId(), 1L, ActionType.SAVED))
                .thenReturn(Optional.of(mockUserResource));

        // Act
        userResourceService.deleteSavedResource(1L);

        // Assert
        Mockito.verify(userResourceRepository, times(1))
                .deleteUserResourceHasActionType(mockUser.getId(), 1L, ActionType.SAVED);
    }

    @Test
    public void testDeleteSavedResourceUserResourceNotFound() {
        // Arrange
        User mockUser = Mockito.mock(User.class);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUser);
        Mockito.when(userResourceRepository.findUserResourceHasActionType(mockUser.getId(), 1L, ActionType.SAVED))
                .thenReturn(Optional.empty());

        ApiException actualThrow = assertThrows(ApiException.class, () -> userResourceService.deleteSavedResource(1L));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_USER_RESOURCE_NOT_FOUND, actualThrow.getError().getMessage());

        // Assert
        Mockito.verify(userResourceRepository, times(0)).deleteUserResourceHasActionType(mockUser.getId(), 1L, ActionType.SAVED);
    }

    @Test
    public void testDeleteSharedResourceSuccess() {
        // Arrange
        User mockUser = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);
        UserResourcePermission mockUserResourcePermission = Mockito.mock(UserResourcePermission.class);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUser);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));
        Mockito.when(userResourcePermissionRepository.findByUserAndResourceAndPermission(mockUser, mockResource))
                .thenReturn(Optional.of(mockUserResourcePermission));

        // Act
        userResourceService.deleteSharedResource(1L);

        // Assert
        Mockito.verify(userResourcePermissionRepository, times(1)).delete(mockUserResourcePermission);
    }

    @Test
    public void testDeleteSharedResourceResourceNotFound() {
        // Arrange
        User mockUser = Mockito.mock(User.class);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUser);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.empty());

        ApiException actualThrow = assertThrows(ApiException.class, () -> userResourceService.deleteSavedResource(1L));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_USER_RESOURCE_NOT_FOUND, actualThrow.getError().getMessage());

        // Assert
        Mockito.verify(userResourcePermissionRepository, times(0)).delete(Mockito.any(UserResourcePermission.class));
    }

    @Test
    public void testDeleteSharedResourceUserResourcePermissionNotFound() {
        // Arrange
        User mockUser = Mockito.mock(User.class);
        Resource mockResource = Mockito.mock(Resource.class);

        Mockito.when(userHelper.getUserLogin()).thenReturn(mockUser);
        Mockito.when(resourceRepository.findById(1L)).thenReturn(Optional.of(mockResource));
        Mockito.when(userResourcePermissionRepository.findByUserAndResourceAndPermission(mockUser, mockResource)).thenReturn(Optional.empty());

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> userResourceService.deleteSavedResource(1L));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_USER_RESOURCE_PERMISSION_NOT_FOUND, actualThrow.getError().getMessage());

        // Assert
        Mockito.verify(userResourcePermissionRepository, times(0)).delete(Mockito.any(UserResourcePermission.class));
    }

}