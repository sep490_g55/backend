package com.capstone.backend.service.impl;

import com.capstone.backend.entity.Comment;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.reportcomment.ReportCommentDTORequest;
import com.capstone.backend.repository.CommentRepository;
import com.capstone.backend.repository.ReportCommentRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.utils.FileHelper;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
class ReportCommentServiceImplTest {
    @Mock
    CommentRepository commentRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    ReportCommentRepository reportCommentRepository;
    @Mock
    MessageException messageException;
    @InjectMocks
    ReportCommentServiceImpl reportCommentService;
    @Mock
    UserHelper userHelper;

    @Test
    public void createReportComment_shouldThrowExceptionWhenCommentNotFound() {
        // Arrange
        ReportCommentDTORequest request = new ReportCommentDTORequest();
        request.setCommentId(1L);
        request.setMessage("This comment is offensive.");

        // Mock dependencies
        Mockito.when(commentRepository.findById(request.getCommentId())).thenReturn(Optional.empty());

        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> reportCommentService.createReportComment(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_COMMENT_NOT_FOUND, actualThrow.getError().getMessage());
    }

    @Test
    public void testCreateReportComment_NoStandardWord_Fail() throws Exception {
        // Mock dependencies
        CommentRepository mockCommentRepository = Mockito.mock(CommentRepository.class);
        UserHelper mockUserHelper = Mockito.mock(UserHelper.class);
        ReportCommentRepository mockReportCommentRepository = Mockito.mock(ReportCommentRepository.class);

        // Setup mocks
        Long commentId = 1L;
        String message = "This comment is offensive!"; // contains non-standard word
        User commenter = new User();
        User reporter = new User();

        Mockito.when(commentRepository.findById(commentId)).thenReturn(Optional.of(Comment.builder().commentId(1L).build()));
        Mockito.when(userHelper.getUserLogin()).thenReturn(reporter);
        Mockito.mockStatic(FileHelper.class);
        Mockito.when(FileHelper.checkContentInputValid(message)).thenReturn(true); // content contains non-standard word

        // Call the method
        ReportCommentDTORequest request = ReportCommentDTORequest.builder().commentId(commentId).message(message).build();
        // Act
        ApiException actualThrow = assertThrows(ApiException.class, () -> reportCommentService.createReportComment(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_COMMENT_NOT_FOUND, actualThrow.getError().getMessage());
    }

}