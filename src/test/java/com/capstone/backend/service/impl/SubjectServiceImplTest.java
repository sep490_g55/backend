package com.capstone.backend.service.impl;

import com.capstone.backend.entity.BookSeriesSubject;
import com.capstone.backend.entity.Subject;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.subject.SubjectDTOFilter;
import com.capstone.backend.model.dto.subject.SubjectDTORequest;
import com.capstone.backend.model.dto.subject.SubjectDTOResponse;
import com.capstone.backend.repository.BookSeriesSubjectRepository;
import com.capstone.backend.repository.SubjectRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.SubjectCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class SubjectServiceImplTest {
    @Mock
    SubjectRepository subjectRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @Mock
    BookSeriesSubjectRepository bookSeriesSubjectRepository;
    @Mock
    SubjectCriteria subjectCriteria;
    @Mock
    UserHelper userHelper;

    @InjectMocks
    SubjectServiceImpl subjectService;

    User userLogged;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung");
        userLogged.setId(1L);
    }

    @Test
    void createSubject_with_Duplicate_Name() {
        // input
        SubjectDTORequest request = new SubjectDTORequest("Toán");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(subjectRepository.findByName(request.getName(), 0L))
                .thenReturn(Optional.of(new Subject()));
        // test accept
        assertThrows(ApiException.class, () -> {
            subjectService.createSubject(request);
        }, "Duplicate subject name");
    }

    @Test
    void createSubject_When_Success() {
        SubjectDTOResponse subjectDTOResponse = new SubjectDTOResponse();
        subjectDTOResponse.setName("Lịch Sử");
        SubjectDTORequest request = new SubjectDTORequest("Lịch Sử");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Subject subject = Subject.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .build();
        Mockito.when(userRepository.findById(userLogged.getId())).thenReturn(Optional.of(userLogged));
        Mockito.when(subjectRepository.save(Mockito.any())).thenReturn(subject);
        subjectDTOResponse = SubjectDTOResponse.builder()
                .name(subject.getName())
                .id(subject.getId())
                .active(subject.getActive())
                .createdAt(subject.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        SubjectDTOResponse subjectDTOResponse1 = subjectService.createSubject(request);
        assertEquals(subjectDTOResponse1.getCreator(), subjectDTOResponse.getCreator());
        assertEquals(subjectDTOResponse1.getCreatedAt(), subjectDTOResponse.getCreatedAt());
        assertEquals(subjectDTOResponse1.getId(), subjectDTOResponse.getId());
        assertEquals(subjectDTOResponse1.getName(), subjectDTOResponse.getName());
    }

    @Test
    void updateSubject_When_DuplicateName() {
        SubjectDTORequest request = new SubjectDTORequest("Lịch Sử");
        Long id = 1L;
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(subjectRepository.findByName(request.getName(), id)).thenReturn(Optional.of(new Subject()));
        // test accept
        assertThrows(ApiException.class, () -> {
            subjectService.updateSubject(id, request);
        }, "Duplicate Subject name");
    }

    @Test
    void updateSubject_When_Success() {
        SubjectDTORequest request = new SubjectDTORequest("Sinh");
        Long id = 1L;
        Subject subject = Subject.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .build();
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(subjectRepository.findByName(request.getName(), id)).thenReturn(Optional.empty());
        // test accept
        Subject subject1 = Subject.builder().userId(userLogged.getId()).build();
        Mockito.when(subjectRepository.findById(id)).thenReturn(Optional.of(subject1));
        Mockito.when(userRepository.findById(userLogged.getId())).thenReturn(Optional.of(userLogged));
        Mockito.when(subjectRepository.save(Mockito.any())).thenReturn(subject);
        SubjectDTOResponse subjectDTOResponse1 = subjectService.updateSubject(id, request);
        SubjectDTOResponse subjectDTOResponse = SubjectDTOResponse.builder()
                .name(subject.getName())
                .id(subject.getId())
                .active(subject.getActive())
                .createdAt(subject.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();

        assertEquals(subjectDTOResponse1.getCreator(), subjectDTOResponse.getCreator());
        assertEquals(subjectDTOResponse1.getCreatedAt(), subjectDTOResponse.getCreatedAt());
        assertEquals(subjectDTOResponse1.getId(), subjectDTOResponse.getId());
        assertEquals(subjectDTOResponse1.getName(), subjectDTOResponse.getName());
    }

    @Test
    void changeStatus_When_ThrowEx() {
        Long id = 1L;
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        bookSeriesSubjectList.add(new BookSeriesSubject());
//        bookSeriesList.add(new BookSeries());
        Subject subject = Subject.builder()
                .userId(userLogged.getId())
                .bookSeriesSubjects(bookSeriesSubjectList)
                .build();
        Mockito.when(subjectRepository.findById(id)).thenReturn(Optional.of(subject));
        // test accept
        assertThrows(ApiException.class, () -> {
            subjectService.changeStatus(id);
        }, "Can not change status Subject because BookVolume already exists");
    }

    @Test
    void changeStatus_When_Success_False_To_True() {
        Long id = 1L;
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        Subject subject = Subject.builder()
                .userId(userLogged.getId())
                .bookSeriesSubjects(bookSeriesSubjectList)
                .bookSeriesSubjects(bookSeriesSubjectList)
                .active(false)
                .build();
        Mockito.when(subjectRepository.findById(id)).thenReturn(Optional.of(subject));
        // test accept
        Mockito.when(subjectRepository.save(Mockito.any())).thenReturn(subject);
        subjectService.changeStatus(id);
//        assertTrue(result);
        assertEquals(subject.getActive(), true);
    }

    @Test
    void changeStatus_When_Success_True_To_False() {
        Long id = 1L;
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        Subject subject = Subject.builder()
                .userId(userLogged.getId())
                .bookSeriesSubjects(bookSeriesSubjectList)
                .bookSeriesSubjects(bookSeriesSubjectList)
                .active(true)
                .build();
        Mockito.when(subjectRepository.findById(id)).thenReturn(Optional.of(subject));
        // test accept
        Mockito.when(subjectRepository.save(Mockito.any())).thenReturn(subject);
        subjectService.changeStatus(id);
//        assertTrue(result);
        assertEquals(subject.getActive(), false);
    }

    @Test
    void searchSubject() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        Mockito.when(subjectCriteria.searchSubject(Mockito.any())).thenReturn(response);
        PagingDTOResponse pagingDTOResponse = subjectService.searchSubject(SubjectDTOFilter.builder().build());
        assertEquals(pagingDTOResponse.getTotalElement(), pagingDTOResponse.getTotalElement());
        assertEquals(pagingDTOResponse.getTotalPage(), pagingDTOResponse.getTotalPage());
    }

    @Test
    void viewSubjectById() {
        Long id = 1L;
        List<BookSeriesSubject> bookSeriesSubjectList = new ArrayList<>();
        bookSeriesSubjectList.add(new BookSeriesSubject());
        Subject subject = Subject.builder()
                .userId(userLogged.getId())
                .bookSeriesSubjects(bookSeriesSubjectList)
                .active(false)
                .createdAt(LocalDateTime.now())
                .name("test")
                .id(id)
                .build();
        Mockito.when(subjectRepository.findById(id)).thenReturn(Optional.of(subject));
        Mockito.when(userRepository.findById(subject.getUserId())).thenReturn(Optional.of(userLogged));
        SubjectDTOResponse subjectDTOResponse = SubjectDTOResponse.builder()
                .active(false)
                .id(id)
                .name("test")
                .createdAt(subject.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        SubjectDTOResponse subjectDTOResponse1 = subjectService.viewSubjectById(id);
        assertEquals(subjectDTOResponse1.getCreator(), subjectDTOResponse.getCreator());
        assertEquals(subjectDTOResponse1.getCreatedAt(), subjectDTOResponse.getCreatedAt());
        assertEquals(subjectDTOResponse1.getId(), subjectDTOResponse.getId());
        assertEquals(subjectDTOResponse1.getName(), subjectDTOResponse.getName());
    }
}