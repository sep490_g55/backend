package com.capstone.backend.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.capstone.backend.entity.User;
import com.capstone.backend.entity.type.ActionType;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.profle.ProfileDTOUpdate;
import com.capstone.backend.model.dto.user.*;
import com.capstone.backend.model.mapper.UserMapper;
import com.capstone.backend.repository.*;
import com.capstone.backend.repository.criteria.UserCriteria;
import com.capstone.backend.service.FileService;
import com.capstone.backend.utils.DataHelper;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.S3Util;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static com.capstone.backend.utils.Constants.DEFAULT_PASSWORD;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
class UserServiceImplTest {
    @InjectMocks
    UserServiceImpl userService;
    @Mock
    UserHelper userHelper;
    @Mock
    ClassRepository classRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    FileService fileService;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    MessageException messageException;
    @Mock
    UserResourceRepository userResourceRepository;
    @Mock
    S3Util s3Util;
    @Mock
    UserCriteria userCriteria;
    @Mock
    UserRoleRepository userRoleRepository;
    @Mock
    RoleRepository roleRepository;
    @Mock
    UserMapper userMapper;
    @Mock
    AmazonS3 s3Client;

    User userLogged;
    User user;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung-admin");
        userLogged.setId(1L);

        user = new User();
        user.setUsername("dung-user");
        user.setId(2L);
        user.setActive(true);
        user.setUserRoleList(new HashSet<>());
    }

    @Test
    void searchUser() {
        PagingDTOResponse pagingDTOResponse = PagingDTOResponse.builder()
                .totalElement(100L)
                .totalPage(10L)
                .build();
        Mockito.when(userCriteria.searchUser(Mockito.any())).thenReturn(pagingDTOResponse);
        PagingDTOResponse rs = userService.searchUser(new UserDTOFilter());
        assertEquals(rs.getTotalPage(), 10L);
        assertEquals(rs.getTotalElement(), 100L);
    }

    @Test
    void resetPasswordUser() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        Mockito.when(userRepository.save(Mockito.eq(user))).thenReturn(user);
        boolean rs = userService.resetPasswordUser(user.getId());
        assertEquals(user.getPassword(), passwordEncoder.encode(DEFAULT_PASSWORD));
        assertTrue(rs);
    }

    @Test
    void resetPasswordUser_When_UserNotfound() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            userService.resetPasswordUser(user.getId());
        }, messageException.MSG_USER_NOT_FOUND);
    }

    @Test
    void getUserById() throws MalformedURLException {
        List<Long> role = new ArrayList<>();
        UserViewDTOResponse userViewDTOResponse = UserViewDTOResponse.builder().username("tien dung").build();
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.ofNullable(user));
        Mockito.when(s3Client.generatePresignedUrl(Mockito.any()))
                .thenReturn(new URL("https://www.facebook.com/"));
        Mockito.when(userRepository.findRoleIdBy(user.getId())).thenReturn(role);
        UserViewDTOResponse userDTOUpdate = userService.getUserById(user.getId());
        assertEquals(userDTOUpdate.getRoles().size(), 0L);
        assertEquals(userDTOUpdate.getUsername(), "tien dung");
    }

    @Test
    void getUserById_When_UserNotfound() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            userService.resetPasswordUser(user.getId());
        }, messageException.MSG_USER_NOT_FOUND);
    }

    @Test
    void updateUser() {
        UserDTOUpdate request = UserDTOUpdate.builder().roles(new ArrayList<>()).build();
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(user));
        Boolean rs = userService.updateUser(request, 1L);
        assertTrue(rs);
    }

    @Test
    void updateUser_When_UserNotfound() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            userService.resetPasswordUser(user.getId());
        }, messageException.MSG_USER_NOT_FOUND);
    }

    @Test
    void changeStatus_When_ThrowEx() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            userService.resetPasswordUser(user.getId());
        }, messageException.MSG_USER_NOT_FOUND);
    }

    @Test
    void changeStatus_When_Success() {
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));

        Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);
        Boolean result = userService.changeActive(user.getId());
        assertTrue(result);
        assertEquals(user.getActive(), false);

    }

    @Test
    public void updateProfile_shouldThrowExceptionWhenPhoneExists() {
        // Arrange
        ProfileDTOUpdate profileDTOUpdate = new ProfileDTOUpdate();
        profileDTOUpdate.setPhone("0987654321");

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);

        Optional<User> existingUser = Optional.of(new User());
        Mockito.when(userRepository.findByPhone(profileDTOUpdate.getPhone(), userLoggedIn.getId())).thenReturn(existingUser);

        ApiException actualThrow = assertThrows(ApiException.class, () -> userService.updateProfile(profileDTOUpdate));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Phone is existed", actualThrow.getError().getMessage());
    }

    @Test
    public void changeAvatar_shouldThrowExceptionWhenInvalidFileType() throws IOException {
        // Arrange
        MultipartFile avatar = mock(MultipartFile.class);
        Mockito.when(avatar.getOriginalFilename()).thenReturn("avatar.exe");

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);

        // Verify interactions
        ApiException actualThrow = assertThrows(ApiException.class, () -> userService.changeAvatar(avatar));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Update file name extension such as (png, jpg, jpeg)", actualThrow.getError().getMessage());

        // Verify interactions
        Mockito.verify(fileService, times(0)).setAvatar(avatar, userLoggedIn);
        Mockito.verify(userRepository, times(0)).save(userLoggedIn);
    }

    @Test
    public void changePassword_shouldChangePasswordSuccessfully() {
        // Arrange
        UserChangePasswordDTORequest request = new UserChangePasswordDTORequest();
        request.setCurrentPassword("currentPassword");
        request.setNewPassword("newPassword");
        request.setConfirmationPassword("newPassword");

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(passwordEncoder.matches("currentPassword", userLoggedIn.getPassword())).thenReturn(true);

        // Act
        boolean result = userService.changePassword(request);

        // Assert
        Assertions.assertTrue(result);
        Mockito.verify(passwordEncoder, times(1)).encode("newPassword");
        Mockito.verify(userRepository, times(1)).save(userLoggedIn);
    }

    @Test
    public void changePassword_shouldThrowExceptionWhenCurrentPasswordIncorrect() {
        // Arrange
        UserChangePasswordDTORequest request = new UserChangePasswordDTORequest();
        request.setCurrentPassword("incorrectPassword");
        request.setNewPassword("newPassword");
        request.setConfirmationPassword("newPassword");

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(passwordEncoder.matches("incorrectPassword", userLoggedIn.getPassword())).thenReturn(false);

        // Verify interactions
        ApiException actualThrow = assertThrows(ApiException.class, () -> userService.changePassword(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_USER_WRONG_PASSWORD, actualThrow.getError().getMessage());

        // Verify interactions
        Mockito.verify(passwordEncoder, times(0)).encode("newPassword");
        Mockito.verify(userRepository, times(0)).save(userLoggedIn);
    }

    @Test
    public void changePassword_shouldThrowExceptionWhenNewPasswordAndConfirmationPasswordDoNotMatch() {
        // Arrange
        UserChangePasswordDTORequest request = new UserChangePasswordDTORequest();
        request.setCurrentPassword("currentPassword");
        request.setNewPassword("newPassword");
        request.setConfirmationPassword("mismatchedPassword");

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);
        Mockito.when(passwordEncoder.matches("currentPassword", userLoggedIn.getPassword())).thenReturn(true);

        // Act
        // Verify interactions
        ApiException actualThrow = assertThrows(ApiException.class, () -> userService.changePassword(request));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_USER_NOT_MATCH_PASSWORD, actualThrow.getError().getMessage());

        // Verify interactions
        Mockito.verify(passwordEncoder, times(0)).encode("newPassword");
        Mockito.verify(userRepository, times(0)).save(userLoggedIn);
    }

    @Test
    public void previewInfo_shouldReturnPreviewInfoDTOResponse() {
        // Arrange
        S3Util s3Util = Mockito.mock(S3Util.class);

        User user = Mockito.mock(User.class);
        Mockito.mockStatic(DataHelper.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(user);

        // Mock dependencies
        User userLoggedIn = Mockito.mock(User.class);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLoggedIn);

        // Mock resource counts
        Mockito.when(userResourceRepository.countUserResourceByType(userLoggedIn.getId(), ActionType.SAVED)).thenReturn(10L);
        Mockito.when(userResourceRepository.countUserResourceByType(userLoggedIn.getId(), ActionType.LIKE)).thenReturn(20L);
        Mockito.when(userResourceRepository.countResourceUploadedByUser(userLoggedIn.getId())).thenReturn(30L);

        // Act
        PreviewInfoDTOResponse response = userService.previewInfo();

        Mockito.verify(userResourceRepository, times(1)).countUserResourceByType(userLoggedIn.getId(), ActionType.SAVED);
        Mockito.verify(userResourceRepository, times(1)).countUserResourceByType(userLoggedIn.getId(), ActionType.LIKE);
        Mockito.verify(userResourceRepository, times(1)).countResourceUploadedByUser(userLoggedIn.getId());
    }


    @Test
    public void changeActive_shouldChangeActiveUserStatus() {
        // Arrange
        Long userId = 1L;
        User user = new User();
        user.setId(userId);
        user.setActive(false);

        // Mock dependencies
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        // Act
        boolean result = userService.changeActive(userId);

        // Assert
        Assertions.assertTrue(result);
        Assertions.assertTrue(user.getActive());
        Mockito.verify(userRepository, times(1)).findById(userId);
        Mockito.verify(userRepository, times(1)).save(user);
    }

    @Test
    public void updateUser_shouldThrowExceptionWhenUserNotFound() {
        // Arrange
        Long userId = 1L;

        // Mock dependencies
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // Verify interactions
        ApiException actualThrow = assertThrows(ApiException.class, () -> userService.updateUser(null, userId));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_USER_NOT_FOUND, actualThrow.getError().getMessage());
    }


}