package com.capstone.backend.service.impl;

import com.capstone.backend.entity.BookSeries;
import com.capstone.backend.entity.Class;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.exception.ApiExceptionHandler;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.classes.ClassDTOFilter;
import com.capstone.backend.model.dto.classes.ClassDTORequest;
import com.capstone.backend.model.dto.classes.ClassDTOResponse;
import com.capstone.backend.repository.ClassRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.ClassesCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ClassServiceImplTest {

    @InjectMocks
    ClassServiceImpl classService;

    @Mock
    ClassRepository classRepository;
    @Mock
    ClassesCriteria classCriteria;
    @Mock
    UserHelper userHelper;
    @InjectMocks
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @Mock
    ApiExceptionHandler apiExceptionHandler;

    User userLogged;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung");
        userLogged.setId(1L);
    }

    @Test
    void createClass_When_DuplicateName() {
        // input
        ClassDTORequest request = new ClassDTORequest("Lop 3");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(classRepository.findByName(request.getName(), 0L))
                .thenReturn(Optional.of(new Class()));
        // test accept
        assertThrows(ApiException.class, () -> {
            classService.createClass(request);
        }, "Duplicate class name");
    }

    @Test
    void createClass_When_requestNull() {
        // input
        ClassDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            classService.createClass(request);
        }, "NullPointerException");
    }

    @Test
    void createClass_When_Success() {
        ClassDTOResponse classDTOResponse = new ClassDTOResponse();
        classDTOResponse.setName("Class-OK");
        ClassDTORequest request = new ClassDTORequest("Class-OK");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Class classEntity = Class.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .build();
        Mockito.when(userRepository.findById(userLogged.getId())).thenReturn(Optional.of(userLogged));
        Mockito.when(classRepository.save(Mockito.any())).thenReturn(classEntity);
        classDTOResponse = ClassDTOResponse.builder()
                .name(classEntity.getName())
                .id(classEntity.getId())
                .active(classEntity.getActive())
                .createdAt(classEntity.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        ClassDTOResponse classDTOResponse1 = classService.createClass(request);
        assertEquals(classDTOResponse1.getCreator(), classDTOResponse.getCreator());
        assertEquals(classDTOResponse1.getCreatedAt(), classDTOResponse.getCreatedAt());
        assertEquals(classDTOResponse1.getId(), classDTOResponse.getId());
        assertEquals(classDTOResponse1.getName(), classDTOResponse.getName());
    }

    @Test
    void updateClass_When_DuplicateName() {
        ClassDTORequest request = new ClassDTORequest("Lop 3");
        Long id = 1L;
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(classRepository.findByName(request.getName(), id)).thenReturn(Optional.of(new Class()));
        // test accept
        assertThrows(ApiException.class, () -> {
            classService.updateClass(id, request);
        }, "Duplicate class name");
    }

    @Test
    void updateClass_When_requestNull() {
        // input
        ClassDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            classService.updateClass(1L, request);
        }, "NullPointerException");
    }

    @Test
    void updateClass_When_Success() {
        ClassDTORequest request = new ClassDTORequest("Class-Test-Update");
        Long id = 1L;
        Class classEntity = Class.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .build();
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(classRepository.findByName(request.getName(), id)).thenReturn(Optional.empty());
        // test accept
        Class classObject = Class.builder().userId(userLogged.getId()).build();
        Mockito.when(classRepository.findById(id)).thenReturn(Optional.of(classObject));
        Mockito.when(userRepository.findById(userLogged.getId())).thenReturn(Optional.of(userLogged));
        Mockito.when(classRepository.save(Mockito.any())).thenReturn(classEntity);
        ClassDTOResponse classDTOResponse1 = classService.updateClass(id, request);
        ClassDTOResponse classDTOResponse = ClassDTOResponse.builder()
                .name(classEntity.getName())
                .id(classEntity.getId())
                .active(classEntity.getActive())
                .createdAt(classEntity.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();

        assertEquals(classDTOResponse1.getCreator(), classDTOResponse.getCreator());
        assertEquals(classDTOResponse1.getCreatedAt(), classDTOResponse.getCreatedAt());
        assertEquals(classDTOResponse1.getId(), classDTOResponse.getId());
        assertEquals(classDTOResponse1.getName(), classDTOResponse.getName());
    }

    @Test
    void changeStatus_When_ThrowEx() {
        Long id = 1L;
        List<BookSeries> bookSeriesList = new ArrayList<>();
        bookSeriesList.add(new BookSeries());
//        bookSeriesList.add(new BookSeries());
        Class classObject = Class.builder()
                .userId(userLogged.getId())
                .bookSeriesList(bookSeriesList)
                .build();
        Mockito.when(classRepository.findById(id)).thenReturn(Optional.of(classObject));
        // test accept
        assertThrows(ApiException.class, () -> {
            classService.changeStatus(id);
        }, "Can not change status Class because Book Series already exists");
    }

    @Test
    void changeStatus_When_Success_False_To_True() {
        Long id = 1L;
        List<BookSeries> bookSeriesList = new ArrayList<>();
        Class classObject = Class.builder()
                .userId(userLogged.getId())
                .bookSeriesList(bookSeriesList)
                .active(false)
                .build();
        Mockito.when(classRepository.findById(id)).thenReturn(Optional.of(classObject));
        // test accept
        Mockito.when(classRepository.save(Mockito.any())).thenReturn(classObject);
        Boolean result = classService.changeStatus(id);
        assertTrue(result);
        assertEquals(classObject.getActive(), true);
    }

    @Test
    void changeStatus_When_Success_True_To_False() {
        Long id = 1L;
        List<BookSeries> bookSeriesList = new ArrayList<>();
        Class classObject = Class.builder()
                .userId(userLogged.getId())
                .bookSeriesList(bookSeriesList)
                .active(true)
                .build();
        Mockito.when(classRepository.findById(id)).thenReturn(Optional.of(classObject));
        // test accept
        Mockito.when(classRepository.save(Mockito.any())).thenReturn(classObject);
        Boolean result = classService.changeStatus(id);
        assertTrue(result);
        assertEquals(classObject.getActive(), false);
    }

    @Test
    void viewClassById() {
        Long id = 1L;
        List<BookSeries> bookSeriesList = new ArrayList<>();
        Class classObject = Class.builder()
                .userId(userLogged.getId())
                .bookSeriesList(bookSeriesList)
                .active(false)
                .createdAt(LocalDateTime.now())
                .name("test")
                .id(id)
                .build();
        Mockito.when(classRepository.findById(id)).thenReturn(Optional.of(classObject));
        Mockito.when(userRepository.findById(classObject.getUserId())).thenReturn(Optional.of(userLogged));
        ClassDTOResponse classDTOResponse = ClassDTOResponse.builder()
                .active(false)
                .id(id)
                .name("test")
                .createdAt(classObject.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        ClassDTOResponse classDTOResponse1 = classService.viewClassById(id);
        assertEquals(classDTOResponse1.getCreator(), classDTOResponse.getCreator());
        assertEquals(classDTOResponse1.getCreatedAt(), classDTOResponse.getCreatedAt());
        assertEquals(classDTOResponse1.getId(), classDTOResponse.getId());
        assertEquals(classDTOResponse1.getName(), classDTOResponse.getName());
    }

    @Test
    void searchClass() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        Mockito.when(classCriteria.searchClass(Mockito.any())).thenReturn(response);
        PagingDTOResponse pagingDTOResponse = classService.searchClass(ClassDTOFilter.builder().build());
        assertEquals(pagingDTOResponse.getTotalElement(), pagingDTOResponse.getTotalElement());
        assertEquals(pagingDTOResponse.getTotalPage(), pagingDTOResponse.getTotalPage());
    }


    @Test
    void getListClasses() {
        List<Class> listData = new ArrayList<>();
        Class entity = Class.builder()
                .id(1L)
                .name("Class A")
                .active(true)
                .createdAt(LocalDateTime.now())
                .build();
        listData.add(entity);

        Mockito.when(classRepository.findClassByActiveIsTrue()).thenReturn(listData);
        List<ClassDTOResponse> listResponse = classService.getListClasses();
        assertEquals(listResponse.size(), listData.size());
        assertEquals(listResponse.get(0).getId(), listData.get(0).getId());
        assertEquals(listResponse.get(0).getName(), listData.get(0).getName());
        assertEquals(listResponse.get(0).isActive(), listData.get(0).getActive());
        assertEquals(listResponse.get(0).getCreatedAt(), listData.get(0).getCreatedAt());

    }

    @Test
    void getListClassesBySubjectId_When_SubjectIdNull() {
        Long subjectId = null;
        Class entity = Class.builder()
                .id(1L)
                .name("Class A")
                .active(true)
                .createdAt(LocalDateTime.now())
                .build();
        List<Class> classes = new ArrayList<>();
        classes.add(entity);
        Mockito.when(classRepository.findClassByActiveIsTrue()).thenReturn(classes);
        List<ClassDTOResponse> classDTOResponseList = classService.getListClassesBySubjectId(null);
        assertEquals(classDTOResponseList.size(), classes.size());
        assertEquals(classDTOResponseList.get(0).getId(), classes.get(0).getId());
        assertEquals(classDTOResponseList.get(0).getName(), classes.get(0).getName());
        assertEquals(classDTOResponseList.get(0).isActive(), classes.get(0).getActive());
        assertEquals(classDTOResponseList.get(0).getCreatedAt(), classes.get(0).getCreatedAt());
    }

    @Test
    void getListClassesBySubjectId_When_SubjectId_NotNull() {
        Long subjectId = 1L;
        Class entity = Class.builder()
                .id(1L)
                .name("Class A")
                .active(true)
                .createdAt(LocalDateTime.now())
                .build();
        List<Class> classes = new ArrayList<>();
        classes.add(entity);
        Mockito.when(classRepository.findAllBySubjectId(subjectId)).thenReturn(classes);
        List<ClassDTOResponse> classDTOResponseList = classService.getListClassesBySubjectId(subjectId);
        assertEquals(classDTOResponseList.size(), classes.size());
        assertEquals(classDTOResponseList.get(0).getId(), classes.get(0).getId());
        assertEquals(classDTOResponseList.get(0).getName(), classes.get(0).getName());
        assertEquals(classDTOResponseList.get(0).isActive(), classes.get(0).getActive());
        assertEquals(classDTOResponseList.get(0).getCreatedAt(), classes.get(0).getCreatedAt());
    }
}