package com.capstone.backend.service.impl;

import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.resource.FileDTOResponse;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.S3Util;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
class FileServiceImplTest {
    @Mock
    MessageException messageException;
    @Mock
    S3Util s3Util;
    @InjectMocks
    FileServiceImpl fileService;

    @Test
    public void testUploadSingleFile_InvalidFileType() throws IOException {
        // Arrange
        MockMultipartFile mockMultipartFile = new MockMultipartFile(
                "file",
                "test.invalid",
                "application/octet-stream",
                "Invalid File".getBytes()
        );
        String fileName = "test.invalid";
        Long lessonId = null;

        // Act and Assert
        assertThrows(ApiException.class, () -> fileService.uploadSingleFile(mockMultipartFile, fileName, lessonId));
    }

    @Test
    public void testUploadSingleFile_NullLessonId() throws IOException {
        // Arrange
        MockMultipartFile mockMultipartFile = new MockMultipartFile(
                "file",
                "test.txt",
                "text/plain",
                "Hello, World!".getBytes()
        );
        String fileName = "test.txt";
        Long lessonId = null;

        // Act and Assert
        assertThrows(ApiException.class, () -> fileService.uploadSingleFile(mockMultipartFile, fileName, lessonId));

        // Example: verify that s3Util.uploadPhoto was NOT called
        Mockito.verify(s3Util, Mockito.never()).uploadPhoto(Mockito.anyString(), Mockito.any(File.class));

        // Add more assertions or verifications based on your specific requirements
    }


    @Test
    public void testUploadMultiFileCustomFilename() {
        // Arrange
        MultipartFile mockFile = Mockito.mock(MultipartFile.class);
        Mockito.when(mockFile.getOriginalFilename()).thenReturn("file.pdf");

        // Act
        List<FileDTOResponse> responses = fileService.uploadMultiFile(new MultipartFile[]{mockFile}, 1L, "custom_filename");

        // Assert
        Mockito.verify(s3Util, times(1)).convertMultiPartToFile(mockFile);
        Assertions.assertEquals(1, responses.size());
    }

    @Test
    public void testDeleteFileResourceSuccess() {
        // Arrange
        String mockFilename = "file.pdf";

        // Act
        fileService.deleteFileResource(mockFilename);

        // Assert
        Mockito.verify(s3Util, times(1)).deleteFile(mockFilename);
    }

    @Test
    public void testDownloadFileSuccess() {
        // Arrange
        String mockFilename = "file.pdf";
        byte[] mockFileContent = new byte[1024];
        Mockito.when(s3Util.downloadPhoto(mockFilename)).thenReturn(mockFileContent);

        // Act
        byte[] downloadedFileContent = fileService.downloadFile(mockFilename);

        // Assert
        Mockito.verify(s3Util, times(1)).downloadPhoto(mockFilename);
        Assertions.assertArrayEquals(mockFileContent, downloadedFileContent);
    }

    @Test
    public void testSetAvatarInvalidFileType() {
        // Arrange
        MultipartFile mockMultipartFile = Mockito.mock(MultipartFile.class);
        Mockito.when(mockMultipartFile.getOriginalFilename()).thenReturn("avatar.exe");

        User mockUserLoggedIn = Mockito.mock(User.class);
        // Act
        assertThrows(ApiException.class, () -> fileService.setAvatar(mockMultipartFile, mockUserLoggedIn));
    }

    @Test
    public void testSetAvatarSuccess() {
        // Arrange
        MultipartFile mockMultipartFile = Mockito.mock(MultipartFile.class);
        Mockito.when(mockMultipartFile.getOriginalFilename()).thenReturn("avatar.jpg");

        User mockUserLoggedIn = Mockito.mock(User.class);
        Mockito.when(mockUserLoggedIn.getUsername()).thenReturn("username");

        // Act
        String fileName = fileService.setAvatar(mockMultipartFile, mockUserLoggedIn);

        // Assert
        Mockito.verify(s3Util, times(1)).convertMultiPartToFile(mockMultipartFile);

    }

}