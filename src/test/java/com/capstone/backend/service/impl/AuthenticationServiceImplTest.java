package com.capstone.backend.service.impl;

import com.capstone.backend.entity.ConfirmationToken;
import com.capstone.backend.entity.Role;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.authentication.AuthenticationDTORequest;
import com.capstone.backend.model.dto.register.RegisterDTORequest;
import com.capstone.backend.model.dto.register.RegisterDTOResponse;
import com.capstone.backend.model.dto.register.RegisterDTOUpdate;
import com.capstone.backend.model.dto.user.UserEmailDTORequest;
import com.capstone.backend.model.dto.user.UserForgotPasswordDTORequest;
import com.capstone.backend.repository.*;
import com.capstone.backend.security.jwt.JwtService;
import com.capstone.backend.service.ConfirmationTokenService;
import com.capstone.backend.utils.EmailHandler;
import com.capstone.backend.utils.EmailHtml;
import com.capstone.backend.utils.MessageException;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
@MockitoSettings(strictness = Strictness.LENIENT)
class AuthenticationServiceImplTest {
    @InjectMocks //testing
    AuthenticationServiceImpl authenticationService;

    @Mock
    UserRepository userRepository;
    @Mock
    AuthenticationManager authenticationManager;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    JwtService jwtService;
    @Mock
    ConfirmationTokenService confirmationTokenService;
    @Mock
    RoleRepository roleRepository;
    @Mock
    EmailHandler emailHandler;
    @Mock
    MessageException messageException;
    @Mock
    ConfirmTokenRepository confirmTokenRepository;
    @Mock
    EmailHtml emailHtml;
    @Mock
    TokenRepository tokenRepository;
    @Mock
    UserRoleRepository userRoleRepository;

    @Test
    void login_with_email_not_exist() throws Exception {
        // Arrange
        String nonExistentEmail = "nonexistent@example.com";
        String password = "aA12345678";

        AuthenticationDTORequest authenticationDTORequest = AuthenticationDTORequest.builder()
                .email(nonExistentEmail)
                .password(password)
                .build();

        when(userRepository.findByUsernameOrEmailActive(nonExistentEmail)).thenReturn(java.util.Optional.empty());

        // Act & Assert
        ApiException actualThrow = assertThrows(
                ApiException.class,
                () -> authenticationService.login(authenticationDTORequest),
                "User is not found"
        );

        // Verify that the findByUsernameOrEmailActive method was called with the correct parameter
        verify(userRepository, times(1)).findByUsernameOrEmailActive(nonExistentEmail);

        // Assertions for ApiException properties
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("User is not found", actualThrow.getError().getMessage());
    }

    @Test
    void login_with_password_wrong() {
        AuthenticationDTORequest request = AuthenticationDTORequest.builder()
                .email("email@gmail.com")
                .password("password")
                .build();

        User user = User.builder()
                .id(1L)
                .email("email@gmail.com")
                .password("password")
                .build();

        when(userRepository.findByUsernameOrEmailActive("email@gmail.com")).thenReturn(Optional.of(user));
        when(authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken("email@gmail.com", "password")))
                .thenThrow(ApiException.class);

        ApiException actualThrow = assertThrows(
                ApiException.class,
                () -> authenticationService.login(request)
        );
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("Access denied, You have no permission", actualThrow.getError().getMessage());

    }

    @Test
    void login_with_blocked() {
        AuthenticationDTORequest request = AuthenticationDTORequest.builder()
                .email("email@gmail.com")
                .password("password")
                .build();

        when(userRepository.findByUsernameOrEmailActive("email@gmail.com")).thenReturn(Optional.empty());

        ApiException actualThrow = assertThrows(
                ApiException.class,
                () -> authenticationService.login(request)
        );
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals("User is not found", actualThrow.getError().getMessage());

    }

    @Test
    void register_email_exist() {
        // Given
        RegisterDTORequest registerDTORequest = RegisterDTORequest.builder()
                .email("email")
                .username("username")
                .password("password")
                .build();

        User userSaved = User.builder()
                .id(1L)
                .email("email")
                .username("username")
                .password("password")
                .build();

        when(userRepository.findByUsernameOrEmailActive("email")).thenReturn(Optional.of(userSaved));

        // When & Then
        ApiException actualException = assertThrows(ApiException.class, () -> authenticationService.register(registerDTORequest));
        assertEquals(HttpStatus.ACCEPTED, actualException.getStatus());
        assertEquals(messageException.MSG_USER_EMAIL_EXISTED, actualException.getError().getMessage());

        verify(userRepository, times(1)).findByUsernameOrEmailActive("email");

        // Verify that other methods were not called
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void register_email_not_exist_username_exist() {
        // Given
        RegisterDTORequest registerDTORequest = RegisterDTORequest.builder()
                .email("email")
                .username("username")
                .password("password")
                .build();

        User userSaved = User.builder()
                .id(1L)
                .email("email")
                .username("username")
                .password("password")
                .build();

        when(userRepository.findByUsernameOrEmailActive("email")).thenReturn(Optional.empty());
        when(userRepository.findByUsernameOrEmailActive("username")).thenReturn(Optional.of(userSaved));

        // When & Then
        ApiException actualException = assertThrows(ApiException.class, () -> authenticationService.register(registerDTORequest));
        assertEquals(HttpStatus.ACCEPTED, actualException.getStatus());
        assertEquals(messageException.MSG_USER_USERNAME_EXISTED, actualException.getError().getMessage());

        // Verify that findByUsernameOrEmailActive methods were called with the correct parameters
        verify(userRepository, times(1)).findByUsernameOrEmailActive("username");
        verify(userRepository, times(1)).findByUsernameOrEmailActive("email");

        // Verify that other methods were not called
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void register_success() {
        RegisterDTORequest request = RegisterDTORequest.builder()
                .username("username")
                .email("email")
                .password("password")
                .build();

        User userCreated = User.builder()
                .id(1L)
                .username("username")
                .email("email")
                .password("password")
                .build();

        Role role = Role.builder()
                .id(1L)
                .build();

        when(userRepository.findByUsernameOrEmailActive("username")).thenReturn(Optional.empty());
        when(userRepository.findByUsernameOrEmailActive("email")).thenReturn(Optional.empty());
        when(roleRepository.findByIdAndActiveTrue(1L)).thenReturn(Optional.of(role));
        when(userRepository.save(any(User.class))).thenReturn(userCreated);

        RegisterDTOResponse response = authenticationService.register(request);

        verify(userRepository, times(1)).save(any(User.class));
        verify(roleRepository, times(1)).findByIdAndActiveTrue(any());
        assertNotNull(response);
        assertEquals(userCreated.getId(), response.getId());
    }

    @Test
    void updateRegister_phone_exist() {
        RegisterDTOUpdate registerDTOUpdate = RegisterDTOUpdate.builder()
                .phone("0355166404")
                .build();

        when(userRepository.findByPhoneAndActiveTrue("0355166404")).thenReturn(Optional.of(User.builder().phone("0355166404").build()));

        ApiException actualException = assertThrows(ApiException.class, () -> authenticationService.updateRegister(registerDTOUpdate));
        assertEquals(HttpStatus.ACCEPTED, actualException.getStatus());
        assertEquals("Phone is existed", actualException.getError().getMessage());
    }

    @Test
    void updateRegister_user_not_found() {
        RegisterDTOUpdate registerDTOUpdate = RegisterDTOUpdate.builder()
                .phone("0355166404")
                .id(10L)
                .build();

        when(userRepository.findByPhoneAndActiveTrue("0355166404")).thenReturn(Optional.empty());
        when(userRepository.findById(10L)).thenReturn(Optional.empty());

        ApiException actualException = assertThrows(ApiException.class, () -> authenticationService.updateRegister(registerDTOUpdate));
        assertEquals(HttpStatus.ACCEPTED, actualException.getStatus());
        assertEquals("User is not found", actualException.getError().getMessage());
    }

    @Test
    void updateRegister_success() {
        RegisterDTOUpdate registerDTOUpdate = RegisterDTOUpdate.builder()
                .id(10L)
                .phone("0355166404")
                .build();

        User user = User.builder()
                .id(10L)
                .username("username")
                .phone("0355166404")
                .build();

        when(userRepository.findByPhoneAndActiveTrue("0355166404")).thenReturn(Optional.empty());
        when(userRepository.findById(10L)).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);
        when(confirmationTokenService.generateTokenEmail(any())).thenReturn("token");
        when(emailHtml.buildEmail(any(), any(), any(), any(), any())).thenReturn("content");
        doNothing().when(emailHandler).send(any());

        Boolean b = authenticationService.updateRegister(registerDTOUpdate);
    }

    @Test
    void forgotPassword_email_not_exist() {
        UserEmailDTORequest request = UserEmailDTORequest.builder()
                .email("email")
                .build();

        when(userRepository.findByUsernameOrEmailActive("email")).thenReturn(Optional.empty());
        ApiException actualException = assertThrows(ApiException.class, () -> authenticationService.forgotPassword(request));
        assertEquals(HttpStatus.ACCEPTED, actualException.getStatus());
        assertEquals("User is not found", actualException.getError().getMessage());
    }

    @Test
    void forgotPassword_email_not_active() {
        UserEmailDTORequest request = UserEmailDTORequest.builder()
                .email("email")
                .build();

        when(userRepository.findByUsernameOrEmailActive("email")).thenReturn(Optional.empty());
        ApiException actualException = assertThrows(ApiException.class, () -> authenticationService.forgotPassword(request));
        assertEquals(HttpStatus.ACCEPTED, actualException.getStatus());
        assertEquals("User is not found", actualException.getError().getMessage());
    }

    @Test
    void forgotPassword_success() {
        UserEmailDTORequest request = UserEmailDTORequest.builder()
                .email("email")
                .build();

        User user = User.builder()
                .id(10L)
                .username("username")
                .phone("0355166404")
                .build();

        when(userRepository.findByUsernameOrEmailActive("email")).thenReturn(Optional.of(user));
        when(confirmationTokenService.generateTokenEmail(any())).thenReturn("token");
        when(emailHtml.buildEmail(any(), any(), any(), any(), any())).thenReturn("content");
        doNothing().when(emailHandler).send(any());

        String link = authenticationService.forgotPassword(request);
    }

    @Test
    void changePasswordForgot_token_not_found() {
        UserForgotPasswordDTORequest request = UserForgotPasswordDTORequest.builder()
                .token("token")
                .confirmationPassword("confirm-password")
                .newPassword("new-password")
                .build();

        when(tokenRepository.findByToken("token")).thenReturn(Optional.empty());
        ApiException actualException = assertThrows(ApiException.class, () -> authenticationService.changePasswordForgot(request));
        assertEquals(HttpStatus.ACCEPTED, actualException.getStatus());
        assertEquals("Token is not found", actualException.getError().getMessage());
    }

    @Test
    void changePasswordForgot_confirm_at_not_empty() {
        UserForgotPasswordDTORequest request = UserForgotPasswordDTORequest.builder()
                .token("token")
                .confirmationPassword("confirm-password")
                .newPassword("new-password")
                .build();

        ConfirmationToken token = ConfirmationToken.builder()
                .token("token")
                .confirmedAt(null)
                .build();

        when(confirmTokenRepository.findByToken("token")).thenReturn(Optional.of(token));
        assertNull(token.getConfirmedAt());
    }

    @Test
    void changePasswordForgot_password_with_confirm_password_not_match() {
        UserForgotPasswordDTORequest request = UserForgotPasswordDTORequest.builder()
                .token("token")
                .confirmationPassword("password")
                .newPassword("password")
                .build();

        ConfirmationToken token = ConfirmationToken.builder()
                .token("token")
                .confirmedAt(null)
                .build();

        User user = User.builder().email("email").build();

        when(confirmTokenRepository.findByToken("token")).thenReturn(Optional.of(token));
        when(userRepository.findByUsernameOrEmailActive("email")).thenReturn(Optional.of(user));
        assertEquals(request.getConfirmationPassword(), request.getNewPassword());
    }

    @Test
    void changePasswordForgot_user_blocked() {
        UserForgotPasswordDTORequest request = UserForgotPasswordDTORequest.builder()
                .token("token")
                .confirmationPassword("confirm-password")
                .newPassword("new-password")
                .build();

        ConfirmationToken token = ConfirmationToken.builder()
                .token("token")
                .user(User.builder().build())
                .confirmedAt(null)
                .build();

        when(confirmTokenRepository.findByToken("token")).thenReturn(Optional.of(token));
        when(userRepository.findByUsernameOrEmailActive("email")).thenReturn(Optional.empty());

        ApiException actualException = assertThrows(ApiException.class, () -> authenticationService.changePasswordForgot(request));
        assertEquals(HttpStatus.ACCEPTED, actualException.getStatus());
        assertEquals("User is not found", actualException.getError().getMessage());
    }
}