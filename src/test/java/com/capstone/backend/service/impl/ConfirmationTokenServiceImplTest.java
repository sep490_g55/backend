package com.capstone.backend.service.impl;

import com.capstone.backend.entity.ConfirmationToken;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.repository.ConfirmTokenRepository;
import com.capstone.backend.utils.MessageException;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
class ConfirmationTokenServiceImplTest {
    @Mock
    ConfirmTokenRepository confirmTokenRepository;
    @Mock
    MessageException messageException;
    @InjectMocks
    ConfirmationTokenServiceImpl confirmationTokenService;

    @Test
    public void testGenerateTokenEmailSuccess() {
        // Arrange
        User mockUser = Mockito.mock(User.class);
        String expectedToken = UUID.randomUUID().toString();

        // Act
        String actualToken = confirmationTokenService.generateTokenEmail(mockUser);

        // Assert
        Mockito.verify(confirmTokenRepository, times(1)).save(Mockito.any(ConfirmationToken.class));
    }


    @Test
    public void testGoToForgotPasswordSuccess() {
        // Arrange
        ConfirmationToken mockConfirmationToken = Mockito.mock(ConfirmationToken.class);
        Mockito.when(mockConfirmationToken.getConfirmedAt()).thenReturn(null);
        Mockito.when(mockConfirmationToken.getExpiresAt()).thenReturn(LocalDateTime.now().plusHours(1));
        Mockito.when(confirmTokenRepository.findByToken("token")).thenReturn(Optional.of(mockConfirmationToken));

        // Act
        String actualURL = confirmationTokenService.goToForgotPassword("token");

        // Assert
        Mockito.verify(confirmTokenRepository, times(1)).findByToken("token");
        Assertions.assertEquals("confirm-password", actualURL);
    }

    @Test
    public void testGoToForgotPasswordTokenConfirmed() {
        // Arrange
        ConfirmationToken mockConfirmationToken = Mockito.mock(ConfirmationToken.class);
        Mockito.when(mockConfirmationToken.getConfirmedAt()).thenReturn(LocalDateTime.now());
        Mockito.when(confirmTokenRepository.findByToken("token")).thenReturn(Optional.of(mockConfirmationToken));

        ApiException actualThrow = assertThrows(ApiException.class, () -> confirmationTokenService.goToForgotPassword("token"));
        assertEquals(HttpStatus.ACCEPTED, actualThrow.getStatus());
        assertEquals(messageException.MSG_TOKEN_NOT_FOUND, actualThrow.getError().getMessage());
    }
}