package com.capstone.backend.service.impl;

import com.capstone.backend.entity.Tag;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.tag.PagingTagDTOResponse;
import com.capstone.backend.model.dto.tag.TagDTOFilter;
import com.capstone.backend.model.dto.tag.TagDTORequest;
import com.capstone.backend.model.dto.tag.TagDTOResponse;
import com.capstone.backend.repository.TagRepository;
import com.capstone.backend.repository.criteria.TagCriteria;
import com.capstone.backend.utils.FileHelper;
import com.capstone.backend.utils.MessageException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TagServiceImplTest {
    @InjectMocks
    TagServiceImpl tagServiceImpl;
    @Mock
    TagRepository tagRepository;
    @Mock
    TagCriteria tagCriteria;
    @Mock
    MessageException messageException;

    @Test
    void createTag_with_tagName_is_null() {
        TagDTORequest tagDTORequest = TagDTORequest.builder()
                .name(null)
                .build();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<TagDTORequest>> violations = validator.validate(tagDTORequest);

        assertEquals(1, violations.size());

        // Check that the violation message is as expected
        ConstraintViolation<TagDTORequest> violation = violations.iterator().next();
        assertEquals("Tag cannot be empty", violation.getMessage());

        Mockito.verify(tagRepository, never()).save(any(Tag.class));
    }

    @Test
    void createTag_with_tagName_greater_than_25() {
        TagDTORequest tagDTORequest = TagDTORequest.builder()
                .name("sachgiaokhoasachbaitapsachluyenchu")
                .build();
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<TagDTORequest>> violations = validator.validate(tagDTORequest);

        assertEquals(1, violations.size());

        // Check that the violation message is as expected
        ConstraintViolation<TagDTORequest> violation = violations.iterator().next();
        assertEquals("Tag length cannot be greater than 25", violation.getMessage());
        Mockito.verify(tagRepository, never()).save(any(Tag.class));
    }

    @Test
    void createTag_with_tagName_existed() {
        TagDTORequest tagDTORequest = TagDTORequest.builder()
                .name("sử học")
                .build();
        when(tagRepository.findByNameEqualsIgnoreCaseAndActiveTrue(tagDTORequest.getName()))
                .thenReturn(Optional.of(new Tag()));
        ApiException actualThrow = assertThrows(ApiException.class, () -> tagServiceImpl.createTag(tagDTORequest));

        assertEquals(HttpStatus.BAD_REQUEST, actualThrow.getStatus());
        assertEquals(messageException.MSG_TAG_EXISTED, actualThrow.getError().getMessage());
        Mockito.verify(tagRepository, never()).save(any(Tag.class));
    }

    @Test
    void createTag_with_tagName_violate_standard() {
        TagDTORequest tagDTORequest = TagDTORequest.builder()
                .name("sử học")
                .build();
        ApiException actualThrow = null;

        try (MockedStatic<FileHelper> fileHelper = Mockito.mockStatic(FileHelper.class)) {
            fileHelper.when(() -> FileHelper.checkContentInputValid(tagDTORequest.getName()))
                    .thenReturn(true);
            actualThrow = assertThrows(ApiException.class, () -> tagServiceImpl.createTag(tagDTORequest));
        }

        assertEquals(HttpStatus.BAD_REQUEST, actualThrow.getStatus());
        assertEquals(messageException.MSG_TEXT_NO_STANDARD_WORD, actualThrow.getError().getMessage());
        Mockito.verify(tagRepository, never()).save(any(Tag.class));
    }

    @Test
    void createTag_success() {
        TagDTORequest tagDTORequest = TagDTORequest.builder()
                .name("lich su")
                .build();
        Tag savedTag = Tag.builder()
                .id(1l)
                .name("lich su")
                .build();

        when(tagRepository.save(any(Tag.class))).thenReturn(savedTag);
        TagDTOResponse actualTagDTOResponse = tagServiceImpl.createTag(tagDTORequest);
        Mockito.verify(tagRepository, times(1)).save(any(Tag.class));
        assertEquals("lich su", actualTagDTOResponse.getName());
    }

    @Test
    void testDisableTag_Success() {
        // Arrange
        long tagId = 1L;
        Tag tag = Tag.builder()
                .id(1l)
                .name("hello")
                .active(true)
                .build();

        // Mock the behavior of tagRepository
        when(tagRepository.findById(tagId)).thenReturn(java.util.Optional.of(tag));
        when(tagRepository.save(tag)).thenReturn(tag);

        // Act
        TagDTOResponse result = tagServiceImpl.disableTag(tagId);

        // Assert
        assertFalse(tag.getActive());
        assertEquals(tagId, result.getId()); // Ensure the returned DTO has the correct ID
    }

    @Test
    void disableTag_tagNotFound() {
        long notExistedTagId = 999L;

        when(tagRepository.findById(notExistedTagId)).thenReturn(Optional.empty());

        ApiException actualThrow = assertThrows(ApiException.class, () -> tagServiceImpl.disableTag(notExistedTagId));
        assertEquals(HttpStatus.NOT_FOUND, actualThrow.getStatus());
        assertEquals(messageException.MSG_TAG_NOT_FOUND, actualThrow.getMessage());

        // Verify that tagRepository.save(tag) is not called
        Mockito.verify(tagRepository, never()).save(any(Tag.class));
    }

    //TH1: Search when search query is empty
    //TH2: Search when entering an invalid search term and no results are returned
    //TH3: Search when entering a valid search term and correct results are returned
    //TH4: Search when entering a search term with multiple words and corrects results are displayed
    //TH5: Search when entering a search term with special character
    //TH6: Search when entering a search term with upper case letters
    //TH7: Search when entering a search term with lower case letters
    //TH8: Search when entering a search term mix between lower case and upper case letters

    @Test
    void searchTags() {
        // Create a TagDTOFilter
        TagDTOFilter tagDTOFilter = new TagDTOFilter();
        tagDTOFilter.setName("example");
        List<TagDTOResponse> tagDTOResponseList = null;
        PagingTagDTOResponse expectedResult = PagingTagDTOResponse.builder()
                .totalElement(2L)
                .totalPage(1L)
                .data(tagDTOResponseList)
                .build();

        when(tagCriteria.searchTag(tagDTOFilter)).thenReturn(expectedResult);

        PagingTagDTOResponse result = tagServiceImpl.searchTags(tagDTOFilter);
        verify(tagCriteria, times(1)).searchTag(tagDTOFilter);
        assertSame(expectedResult, result);
    }

    @Test
    void getTagByID_tagNotExist() {
        long notExistTagId = 999L;

        when(tagRepository.findByIdAndActiveTrue(notExistTagId)).thenReturn(Optional.empty());

        ApiException exception = assertThrows(ApiException.class, () -> tagServiceImpl.getTagByID(notExistTagId));
        assertEquals(messageException.MSG_TAG_NOT_FOUND, exception.getMessage());
        verify(tagRepository, times(1)).findByIdAndActiveTrue(notExistTagId);
    }

    @Test
    void getTagByID_tagFound() {
        long tagId = 1L;
        Tag tag = Tag.builder()
                .id(1L)
                .name("example")
                .active(true)
                .build();

        when(tagRepository.findByIdAndActiveTrue(tagId)).thenReturn(Optional.of(tag));

        TagDTOResponse result = tagServiceImpl.getTagByID(tagId);
        verify(tagRepository, times(1)).findByIdAndActiveTrue(tagId);
        assertEquals(tagId, result.getId());
        assertEquals("example", result.getName());
    }

}