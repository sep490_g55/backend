package com.capstone.backend.service.impl;

import com.capstone.backend.entity.BookVolume;
import com.capstone.backend.entity.Chapter;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.chapter.ChapterDTOFilter;
import com.capstone.backend.model.dto.chapter.ChapterDTORequest;
import com.capstone.backend.model.dto.chapter.ChapterDTOResponse;
import com.capstone.backend.repository.BookVolumeRepository;
import com.capstone.backend.repository.ChapterRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.ChapterCriteria;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class ChapterServiceImplTest {
    @InjectMocks
    ChapterServiceImpl chapterService;
    @Mock
    ChapterRepository chapterRepository;
    @Mock
    BookVolumeRepository bookVolumeRepository;
    @Mock
    ChapterCriteria chapterCriteria;
    @Mock
    UserHelper userHelper;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;

    User userLogged;

    @BeforeEach
    void initUserLogged() {
        userLogged = new User();
        userLogged.setUsername("dung");
        userLogged.setId(1L);
    }

    @Test
    void createChapter_When_DuplicateName() {
        // input
        ChapterDTORequest request = new ChapterDTORequest("Chuong 1");
        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(chapterRepository.findByName(Mockito.eq("Chuong 1"), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(new Chapter()));
        // test accept
        assertThrows(ApiException.class, () -> {
            chapterService.createChapter(request, bookVolume.getId());
        }, "Duplicate bookVolume name");
    }

    @Test
    void createChapter_When_requestNull() {
        // input
        ChapterDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            chapterService.createChapter(request, 1L);
        }, "NullPointerException");
    }

    @Test
    void createChapter_When_Success() {
        ChapterDTOResponse chapterDTOResponse = new ChapterDTOResponse();
        chapterDTOResponse.setName("Chuong 1");

        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        bookVolume.setName("Tap 1");
        bookVolume.setActive(true);
        bookVolume.setCreatedAt(LocalDateTime.now());

        ChapterDTORequest request = new ChapterDTORequest("Chuong 1");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Chapter chapter = Chapter.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .bookVolume(bookVolume)
                .build();

        Mockito.when(chapterRepository.findByName(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.empty());
        Mockito.when(bookVolumeRepository.findById(Mockito.any())).thenReturn(Optional.of(new BookVolume()));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        Mockito.when(chapterRepository.save(Mockito.any())).thenReturn(chapter);
        chapterDTOResponse = ChapterDTOResponse.builder()
                .name(chapter.getName())
                .id(chapter.getId())
                .active(chapter.getActive())
                .createdAt(chapter.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        ChapterDTOResponse chapterDTOResponse1 = chapterService.createChapter(request, 1L);
        assertEquals(chapterDTOResponse1.getCreator(), chapterDTOResponse.getCreator());
        assertEquals(chapterDTOResponse1.getCreatedAt(), chapterDTOResponse.getCreatedAt());
        assertEquals(chapterDTOResponse1.getId(), chapterDTOResponse.getId());
        assertEquals(chapterDTOResponse1.getName(), chapterDTOResponse.getName());

    }

    @Test
    void update_with_duplicate_Chapter() {
        ChapterDTORequest request = new ChapterDTORequest("Chuong 1");
        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        Long id = 1L;
        Chapter chapter = Chapter.builder()
                .bookVolume(bookVolume)
                .build();
        Chapter chapter1 = Chapter.builder().build();
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Mockito.when(chapterRepository.findById(Mockito.eq(id)))
                .thenReturn(Optional.of(chapter));
        Mockito.when(chapterRepository.findByName(
                        Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(Optional.of(chapter1));
        // test accept
        assertThrows(ApiException.class, () -> {
            chapterService.updateChapter(id, request);
        }, "Duplicate chapter name");

    }

    @Test
    void updateChapter_When_requestNull() {
        // input
        ChapterDTORequest request = null;
        assertThrows(NullPointerException.class, () -> {
            chapterService.updateChapter(1L, request);
        }, "NullPointerException");
    }

    @Test
    void updateChapter_When_Success() {
        ChapterDTOResponse chapterDTOResponse = new ChapterDTOResponse();
        chapterDTOResponse.setName("Chuong 1");

        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        bookVolume.setName("Tap 1");
        bookVolume.setActive(true);
        bookVolume.setCreatedAt(LocalDateTime.now());

        ChapterDTORequest request = new ChapterDTORequest("Chuong 1");
        Mockito.when(userHelper.getUserLogin()).thenReturn(userLogged);
        Chapter chapter = Chapter.builder()
                .active(true)
                .createdAt(LocalDateTime.now())
                .name(request.getName())
                .userId(userLogged.getId())
                .bookVolume(bookVolume)
                .build();

        Mockito.when(chapterRepository.findByName(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(Optional.empty());
        Mockito.when(bookVolumeRepository.findById(Mockito.any())).thenReturn(Optional.of(new BookVolume()));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        Mockito.when(chapterRepository.save(Mockito.any())).thenReturn(chapter);
        chapterDTOResponse = ChapterDTOResponse.builder()
                .name(chapter.getName())
                .id(chapter.getId())
                .active(chapter.getActive())
                .createdAt(chapter.getCreatedAt())
                .creator(userLogged.getUsername())
                .build();
        ChapterDTOResponse chapterDTOResponse1 = chapterService.createChapter(request, 1L);
        assertEquals(chapterDTOResponse1.getCreator(), chapterDTOResponse.getCreator());
        assertEquals(chapterDTOResponse1.getCreatedAt(), chapterDTOResponse.getCreatedAt());
        assertEquals(chapterDTOResponse1.getId(), chapterDTOResponse.getId());
        assertEquals(chapterDTOResponse1.getName(), chapterDTOResponse.getName());

    }

    @Test
    void changeStatus_When_ThrowEx() {
        Long id = 1L;
        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        bookVolume.setName("Tap 1");
        bookVolume.setActive(true);
        bookVolume.setCreatedAt(LocalDateTime.now());

//        bookSeriesList.add(new BookSeries());
        Chapter chapter = Chapter.builder()
                .userId(userLogged.getId())
                .bookVolume(bookVolume)
                .name("chuong 2")
                .active(true)
                .build();
        Mockito.when(chapterRepository.findById(id)).thenReturn(Optional.of(chapter));
        // test accept
        assertThrows(NullPointerException.class, () -> {
            chapterService.changeStatus(id);
        }, "Can not change status Chapter because Lesson already exists");
    }

    @Test
    void changeStatus_When_Success_False_To_True() {
        Long id = 1L;
        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        bookVolume.setName("Tap 1");
        bookVolume.setActive(true);
        bookVolume.setCreatedAt(LocalDateTime.now());

        Chapter chapter = Chapter.builder()
                .userId(userLogged.getId())
                .bookVolume(bookVolume)
                .active(false)
                .createdAt(LocalDateTime.now())
                .name("name")
                .build();
        Mockito.when(chapterRepository.findById(id)).thenReturn(Optional.of(chapter));
        // test accept
        Mockito.when(chapterRepository.save(Mockito.any())).thenReturn(chapter);
        chapterService.changeStatus(id);
//        assertTrue(result);
        assertEquals(chapter.getActive(), true);
    }

    @Test
    void searchChapter_When_Ex() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        ChapterDTOFilter chapterDTOFilter = new ChapterDTOFilter();
        chapterDTOFilter.setName("Chuong 1");
        Mockito.when(bookVolumeRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            chapterService.searchChapter(chapterDTOFilter, bookVolume.getId());
        }, "messageException.MSG_USER_NOT_FOUND");
    }

    @Test
    void searchChapter() {
        PagingDTOResponse response = PagingDTOResponse.builder()
                .totalElement(10L)
                .totalPage(5L)
                .build();
        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        Mockito.when(chapterCriteria.searchChapter(Mockito.any(), Mockito.any())).thenReturn(response);
        Mockito.when(bookVolumeRepository.findById(Mockito.any())).thenReturn(Optional.of(new BookVolume()));
        ChapterDTOFilter chapterDTOFilter = new ChapterDTOFilter();
        chapterDTOFilter.setName("Chuong 2");
        PagingDTOResponse pagingDTOResponse = chapterService.searchChapter(chapterDTOFilter, bookVolume.getId());
        assertEquals(pagingDTOResponse.getTotalElement(), pagingDTOResponse.getTotalElement());
        assertEquals(pagingDTOResponse.getTotalPage(), pagingDTOResponse.getTotalPage());
    }

    @Test
    void viewChapterById() {
        ChapterDTOResponse chapterDTOResponse = ChapterDTOResponse.builder().id(10L).build();
        Long id = 1L;
        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        bookVolume.setName("Tap 1");
        bookVolume.setActive(true);
        bookVolume.setCreatedAt(LocalDateTime.now());

//        bookSeriesList.add(new BookSeries());
        Chapter chapter = Chapter.builder()
                .id(10L)
                .userId(userLogged.getId())
                .bookVolume(bookVolume)
                .name("chuong 2")
                .active(true)
                .build();
        Mockito.when(chapterRepository.findById(id)).thenReturn(Optional.of(chapter));
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(userLogged));
        ChapterDTOResponse chapterDTOResponse1 = chapterService.viewChapterById(id);
        assertEquals(chapterDTOResponse1.getId(), 10L);
        assertEquals(chapterDTOResponse1.getName(), "chuong 2");
    }

    @Test
    void viewChapterById_When_ChapterNotFound() {
        ChapterDTOResponse chapterDTOResponse = ChapterDTOResponse.builder().id(10L).build();
        Long id = 1L;
        BookVolume bookVolume = new BookVolume();
        bookVolume.setId(1L);
        bookVolume.setName("Tap 1");
        bookVolume.setActive(true);
        bookVolume.setCreatedAt(LocalDateTime.now());

//        bookSeriesList.add(new BookSeries());
        Chapter chapter = Chapter.builder()
                .id(10L)
                .userId(userLogged.getId())
                .bookVolume(bookVolume)
                .name("chuong 2")
                .active(true)
                .build();
        Mockito.when(chapterRepository.findById(id)).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            chapterService.viewChapterById(id);
        }, "Not Found Chapter");
    }
}