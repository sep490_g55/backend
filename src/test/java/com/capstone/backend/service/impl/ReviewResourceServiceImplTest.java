package com.capstone.backend.service.impl;

import com.capstone.backend.entity.Resource;
import com.capstone.backend.entity.User;
import com.capstone.backend.entity.type.ApproveType;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.reportresource.ResourceReportDTOResponse;
import com.capstone.backend.model.dto.reviewResource.ReviewResourceDTORequest;
import com.capstone.backend.repository.ResourceRepository;
import com.capstone.backend.repository.UserResourcePermissionRepository;
import com.capstone.backend.repository.criteria.ReviewResourceCriteria;
import com.capstone.backend.utils.EmailHandler;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.S3Util;
import com.capstone.backend.utils.UserHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

@ExtendWith(MockitoExtension.class)
class ReviewResourceServiceImplTest {
    @InjectMocks
    ReviewResourceServiceImpl reviewResourceServiceImpl;
    @Mock
    ResourceRepository resourceRepository;
    @Mock
    UserResourcePermissionRepository userResourcePermissionRepository;
    @Mock
    EmailHandler emailHandler;
    @Mock
    MessageException messageException;
    @Mock
    UserHelper userHelper;
    @Mock
    ReviewResourceCriteria reviewResourceCriteria;
    @Mock
    private S3Util s3Util;

    @Test
    void reviewResource_with_resource_not_exist() {
        long notExistedResourceId = 999L;
        ReviewResourceDTORequest request = ReviewResourceDTORequest.builder()
                .resourceId(notExistedResourceId)
                .message("accept")
                .build();
        Mockito.when(resourceRepository.findById(notExistedResourceId)).thenReturn(Optional.empty());
        ApiException actualThrow = assertThrows(ApiException.class, () ->
                reviewResourceServiceImpl.reviewResource(request, true));
        assertEquals(HttpStatus.NOT_FOUND, actualThrow.getStatus());
        assertEquals(messageException.MSG_RESOURCE_NOT_FOUND, actualThrow.getMessage());

        Mockito.verify(resourceRepository, Mockito.never()).save(Mockito.any());
        Mockito.verify(userResourcePermissionRepository, Mockito.never()).save(Mockito.any());
        Mockito.verify(emailHandler, Mockito.never()).send(Mockito.any());
    }

    @Test
    void reviewResource_with_resourceId_null() {
        openMocks(this);

        ReviewResourceDTORequest request = ReviewResourceDTORequest.builder()
                .resourceId(null)
                .message("accept")
                .build();

        Mockito.when(resourceRepository.findById(null)).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> reviewResourceServiceImpl.reviewResource(request, true));

        Mockito.verify(resourceRepository, Mockito.never()).save(Mockito.any());
        Mockito.verify(userResourcePermissionRepository, Mockito.never()).save(Mockito.any());
        Mockito.verify(emailHandler, Mockito.never()).send(Mockito.any());
    }

    @Test
    void reviewResource_success() {
        openMocks(this); // Initialize mocks

        long resourceId = 123L;
        String authorUsername = "author";
        String authorEmail = "author@example.com";

        User reviewPerson = User.builder()
                .username("moderator").build();

        Resource existingResource = Resource.builder()
                .id(resourceId)
                .approveType(ApproveType.UNACCEPTED)
                .author(User.builder().username(authorUsername).email(authorEmail).build())
                .build();

        ReviewResourceDTORequest request = ReviewResourceDTORequest.builder()
                .resourceId(resourceId)
                .message("accept")
                .build();

        Mockito.when(resourceRepository.findById(resourceId)).thenReturn(Optional.of(existingResource));
        Mockito.when(userHelper.getUserLogin()).thenReturn(reviewPerson);

        assertDoesNotThrow(() -> reviewResourceServiceImpl.reviewResource(request, true));

        assertEquals(ApproveType.ACCEPTED, existingResource.getApproveType());

        Mockito.verify(resourceRepository, Mockito.times(1)).save(existingResource);
        Mockito.verify(userResourcePermissionRepository, Mockito.times(1)).save(Mockito.any());
        Mockito.verify(emailHandler, Mockito.times(1)).send(Mockito.any());

        Mockito.verify(emailHandler).send(Mockito.argThat(emailInfo ->
                emailInfo.getTo().equals(authorEmail) &&
                        emailInfo.getContent().contains("Chúc mừng!") &&
                        emailInfo.getContent().contains("Tài liệu của bạn đã được thông qua.") &&
                        emailInfo.getSubject().equals("[EMSS] Tài liệu của bạn đã được kiểm duyệt.")
        ));
    }

    @Test
    void getResourceReportById_notFound() {
        long notExistResourceId = 999L;

        when(resourceRepository.findById(notExistResourceId)).thenReturn(Optional.empty());
        ApiException exception = assertThrows(ApiException.class, () -> reviewResourceServiceImpl.getResourceReportById(notExistResourceId));
        assertEquals(messageException.MSG_RESOURCE_NOT_FOUND, exception.getMessage());
        verify(resourceRepository, times(1)).findById(notExistResourceId);
    }

    @Test
    void getResourceReportById_success() {
        openMocks(this);

        long existingResourceId = 123L;

        Resource existingResource = Resource.builder()
                .id(existingResourceId)
                .name("Sample Resource")
                .author(User.builder().username("author").email("author@example.com").build())
                .build();

        when(resourceRepository.findById(existingResourceId)).thenReturn(Optional.of(existingResource));
        ResourceReportDTOResponse report = reviewResourceServiceImpl.getResourceReportById(existingResourceId);

        assertEquals(existingResource.getName(), report.getName());

        Mockito.verify(resourceRepository, Mockito.never()).save(Mockito.any());
    }
}