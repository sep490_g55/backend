package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.*;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.bookvolume.BookVolumeDTOFilter;
import com.capstone.backend.repository.SubjectRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.utils.MessageException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class BookVolumeCriteriaTest {
    @Mock
    EntityManager em;
    @Mock
    SubjectRepository subjectRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    private BookVolumeCriteria bookVolumeCriteria;

    @Test
    void searchBookVolume() {
        // Arrange
        BookVolumeDTOFilter bookVolumeDTOFilter = new BookVolumeDTOFilter();
        Long bookSeriesSubjectId = 1L;
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .bookVolumes(new ArrayList<>())
                .subject(new Subject())
                .bookSeries(new BookSeries())
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        User user = User.builder().id(1L).username("dung").build();
        BookVolume b = BookVolume.builder()
                .name("Tap 1")
                .id(1L)
                .active(true)
                .bookSeriesSubject(bookSeriesSubject)
                .createdAt(LocalDateTime.now()).build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<BookVolume> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(BookVolume.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(b)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        // Act
        PagingDTOResponse result = bookVolumeCriteria.searchBookVolume(bookVolumeDTOFilter, bookSeriesSubjectId);

        // Assert
        assertEquals(10, result.getTotalElement());
    }

    @Test
    void searchBookVolume_When_UserNotFound() {
        // Arrange
        BookVolumeDTOFilter bookVolumeDTOFilter = new BookVolumeDTOFilter();
        Long bookSeriesSubjectId = 1L;
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .bookVolumes(new ArrayList<>())
                .subject(new Subject())
                .bookSeries(new BookSeries())
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        User user = User.builder().id(1L).username("dung").build();
        BookVolume b = BookVolume.builder()
                .name("Tap 1")
                .id(1L)
                .active(true)
                .bookSeriesSubject(bookSeriesSubject)
                .createdAt(LocalDateTime.now()).build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<BookVolume> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(BookVolume.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(b)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> {
            bookVolumeCriteria.searchBookVolume(bookVolumeDTOFilter, bookSeriesSubjectId);
        }, messageException.MSG_USER_NOT_FOUND);
    }
}