package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.BookSeries;
import com.capstone.backend.entity.Class;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.bookseries.BookSeriesDTOFilter;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.utils.MessageException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class BookSeriesCriteriaTest {

    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    private BookSeriesCriteria bookSeriesCriteria;
    @Mock
    private EntityManager em;

    @Test
    void searchBookSeries() {
        // Arrange
        BookSeriesDTOFilter bookSeriesDTOFilter = new BookSeriesDTOFilter();
        Long classId = 1L;
        Class classObject = Class.builder()
                .name("class")
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        User user = User.builder().id(1L).username("dung").build();
        BookSeries b = BookSeries.builder()
                .name("Canh Dieu")
                .id(1L)
                .active(true)
                .classObject(classObject)
                .createdAt(LocalDateTime.now()).build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<BookSeries> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(BookSeries.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(b)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        // Act
        PagingDTOResponse result = bookSeriesCriteria.searchBookSeries(bookSeriesDTOFilter, classId);

        // Assert
        assertEquals(10, result.getTotalElement());

    }

    @Test
    void searchBookSeries_When_UserNotFound() {
        // Arrange
        BookSeriesDTOFilter bookSeriesDTOFilter = new BookSeriesDTOFilter();
        Long classId = 1L;
        Class classObject = Class.builder()
                .name("class")
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        User user = User.builder().id(1L).username("dung").build();
        BookSeries b = BookSeries.builder()
                .name("Canh Dieu")
                .id(1L)
                .active(true)
                .classObject(classObject)
                .createdAt(LocalDateTime.now()).build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<BookSeries> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(BookSeries.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(b)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        // Act
//        PagingDTOResponse result = bookSeriesCriteria.searchBookSeries(bookSeriesDTOFilter, classId);
        assertThrows(ApiException.class, () -> {
            bookSeriesCriteria.searchBookSeries(bookSeriesDTOFilter, classId);
        }, messageException.MSG_USER_NOT_FOUND);
        // Assert
//        assertEquals(10, result.getTotalElement());

    }

}