package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.user.UserDTOFilter;
import com.capstone.backend.repository.ResourceRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.utils.MessageException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class UserCriteriaTest {
    @Mock
    EntityManager em;
    @Mock
    ResourceRepository resourceRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    MessageException messageException;
    @InjectMocks
    private UserCriteria userCriteria;

    @Test
    void searchUser() {
        UserDTOFilter userDTOFilter = new UserDTOFilter();
        User user = User.builder()
                .id(1L)
                .username("dung")
                .active(true)
                .school("High")
                .gender(true)
                .firstname("nguyen")
                .lastname("tien")
                .password("123456")
                .build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<User> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(User.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(user)); // Assuming a single result

        // Act
        PagingDTOResponse result = userCriteria.searchUser(userDTOFilter);

        // Assert
        assertEquals(10, result.getTotalElement());
    }

    @Test
    void searchUser_When_UserNotFound() {
        UserDTOFilter userDTOFilter = new UserDTOFilter();
        User user = User.builder()
                .id(1L)
                .username("dung")
                .active(true)
                .school("High")
                .gender(true)
                .firstname("nguyen")
                .lastname("tien")
                .password("123456")
                .build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<User> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(User.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(user)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> {
            userCriteria.searchUser(userDTOFilter);
        }, messageException.MSG_USER_NOT_FOUND);
    }
}
