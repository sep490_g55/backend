package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.BookVolume;
import com.capstone.backend.entity.Chapter;
import com.capstone.backend.entity.Lesson;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.lesson.LessonDTOFilter;
import com.capstone.backend.repository.ChapterRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.utils.MessageException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class LessonCriteriaTest {
    @Mock
    EntityManager em;
    @Mock
    ChapterRepository chapterRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    private LessonCriteria lessonCriteria;

    @Test
    void searchLesson() {
        // Arrange
        LessonDTOFilter lessonDTOFilter = new LessonDTOFilter();
        Long chapterId = 1L;
        BookVolume b = BookVolume.builder()
                .name("Tap 1")
                .id(1L)
                .active(true)
                .createdAt(LocalDateTime.now()).build();
        User user = User.builder().id(1L).username("dung").build();
        Chapter chapter = Chapter.builder()
                .name("Chuong 1")
                .id(1L)
                .active(true)
                .bookVolume(b)
                .createdAt(LocalDateTime.now()).build();
        Lesson lesson = Lesson.builder()
                .name("Bai 1")
                .id(1L)
                .active(true)
                .chapter(chapter)
                .createdAt(LocalDateTime.now()).build();


        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<Lesson> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(Lesson.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(lesson)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        // Act
        PagingDTOResponse result = lessonCriteria.searchLesson(lessonDTOFilter, chapterId);

        // Assert
        assertEquals(10, result.getTotalElement());
    }

    @Test
    void searchLesson_When_UserNotFound() {
        // Arrange
        LessonDTOFilter lessonDTOFilter = new LessonDTOFilter();
        Long chapterId = 1L;
        BookVolume b = BookVolume.builder()
                .name("Tap 1")
                .id(1L)
                .active(true)
                .createdAt(LocalDateTime.now()).build();
        User user = User.builder().id(1L).username("dung").build();
        Chapter chapter = Chapter.builder()
                .name("Chuong 1")
                .id(1L)
                .active(true)
                .bookVolume(b)
                .createdAt(LocalDateTime.now()).build();
        Lesson lesson = Lesson.builder()
                .name("Bai 1")
                .id(1L)
                .active(true)
                .chapter(chapter)
                .createdAt(LocalDateTime.now()).build();


        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<Lesson> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(Lesson.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(lesson)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> {
            lessonCriteria.searchLesson(lessonDTOFilter, chapterId);
        }, messageException.MSG_USER_NOT_FOUND);
    }
}