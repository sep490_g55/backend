package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.*;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.chapter.ChapterDTOFilter;
import com.capstone.backend.repository.BookVolumeRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.utils.MessageException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class ChapterCriteriaTest {
    @Mock
    EntityManager em;
    @Mock
    BookVolumeRepository bookVolumeRepository;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    private ChapterCriteria chapterCriteria;

    @Test
    void searchChapter() {
        // Arrange
        ChapterDTOFilter chapterDTOFilter = new ChapterDTOFilter();
        Long bookVolumeId = 1L;
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .bookVolumes(new ArrayList<>())
                .subject(new Subject())
                .bookSeries(new BookSeries())
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        BookVolume b = BookVolume.builder()
                .name("Tap 1")
                .id(1L)
                .active(true)
                .bookSeriesSubject(bookSeriesSubject)
                .createdAt(LocalDateTime.now()).build();
        User user = User.builder().id(1L).username("dung").build();
        Chapter chapter = Chapter.builder()
                .name("Chuong 1")
                .id(1L)
                .active(true)
                .bookVolume(b)
                .createdAt(LocalDateTime.now()).build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<Chapter> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(Chapter.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(chapter)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        // Act
        PagingDTOResponse result = chapterCriteria.searchChapter(chapterDTOFilter, bookVolumeId);

        // Assert
        assertEquals(10, result.getTotalElement());
    }

    @Test
    void searchChapter_When_UserNotFound() {
        // Arrange
        ChapterDTOFilter chapterDTOFilter = new ChapterDTOFilter();
        Long bookVolumeId = 1L;
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .bookVolumes(new ArrayList<>())
                .subject(new Subject())
                .bookSeries(new BookSeries())
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        BookVolume b = BookVolume.builder()
                .name("Tap 1")
                .id(1L)
                .active(true)
                .bookSeriesSubject(bookSeriesSubject)
                .createdAt(LocalDateTime.now()).build();
        User user = User.builder().id(1L).username("dung").build();
        Chapter chapter = Chapter.builder()
                .name("Chuong 1")
                .id(1L)
                .active(true)
                .bookVolume(b)
                .createdAt(LocalDateTime.now()).build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<Chapter> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(Chapter.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(chapter)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());
        assertThrows(ApiException.class, () -> {
            chapterCriteria.searchChapter(chapterDTOFilter, bookVolumeId);
        }, messageException.MSG_USER_NOT_FOUND);
    }

}