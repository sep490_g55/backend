package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.BookSeries;
import com.capstone.backend.entity.BookSeriesSubject;
import com.capstone.backend.entity.Subject;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.bookseriesSubject.BookSeriesSubjectDTOFilter;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.utils.MessageException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class BookSeriesSubjectCriteriaTest {
    @Mock
    EntityManager em;
    @Mock
    UserRepository userRepository;
    @Mock
    MessageException messageException;
    @InjectMocks
    private BookSeriesSubjectCriteria bookSeriesSubjectCriteria;

    @Test
    void searchBookSeriesSubject() {
        BookSeriesSubjectDTOFilter bookSeriesSubjectDTOFilter = new BookSeriesSubjectDTOFilter();
        Long bookSeriesId = 1L;
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .bookVolumes(new ArrayList<>())
                .subject(new Subject())
                .bookSeries(new BookSeries())
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        User user = User.builder().id(1L).username("dung").build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<BookSeriesSubject> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(BookSeriesSubject.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(bookSeriesSubject)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        // Act
        PagingDTOResponse result = bookSeriesSubjectCriteria.searchBookSeriesSubject(bookSeriesSubjectDTOFilter, bookSeriesId);

        // Assert
        assertEquals(10, result.getTotalElement());
    }

    @Test
    void searchBookSeriesSubject_When_UserNotFound() {
        BookSeriesSubjectDTOFilter bookSeriesSubjectDTOFilter = new BookSeriesSubjectDTOFilter();
        Long bookSeriesId = 1L;
        BookSeriesSubject bookSeriesSubject = BookSeriesSubject.builder()
                .bookVolumes(new ArrayList<>())
                .subject(new Subject())
                .bookSeries(new BookSeries())
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        User user = User.builder().id(1L).username("dung").build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<BookSeriesSubject> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(BookSeriesSubject.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(bookSeriesSubject)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> {
            bookSeriesSubjectCriteria.searchBookSeriesSubject(bookSeriesSubjectDTOFilter, bookSeriesId);
        }, messageException.MSG_USER_NOT_FOUND);
    }
}