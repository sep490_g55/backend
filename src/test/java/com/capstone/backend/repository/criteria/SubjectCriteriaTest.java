package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.Subject;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.subject.SubjectDTOFilter;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.utils.MessageException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class SubjectCriteriaTest {
    @Mock
    EntityManager em;
    @Mock
    MessageException messageException;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    private SubjectCriteria subjectCriteria;

    @Test
    void searchSubject() {
        // Arrange
        SubjectDTOFilter subjectDTOFilter = new SubjectDTOFilter();
        Subject subject = Subject.builder()
                .name("Toan")
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        User user = User.builder().id(1L).username("dung").build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<Subject> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(Subject.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(subject)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
        // Act
        PagingDTOResponse result = subjectCriteria.searchSubject(subjectDTOFilter);

        // Assert
        assertEquals(10, result.getTotalElement());
    }

    @Test
    void searchSubject_When_UserNotFound() {
        // Arrange
        SubjectDTOFilter subjectDTOFilter = new SubjectDTOFilter();
        Subject subject = Subject.builder()
                .name("Toan")
                .id(1L)
                .createdAt(LocalDateTime.now())
                .active(true)
                .build();
        User user = User.builder().id(1L).username("dung").build();

        Query queryMock = Mockito.mock(Query.class);
        TypedQuery<Subject> typedQueryMock = Mockito.mock(TypedQuery.class);
        Mockito.when(em.createQuery(Mockito.any(String.class))).thenReturn(queryMock);
        Mockito.when(em.createQuery(Mockito.any(String.class), Mockito.eq(Subject.class))).thenReturn(typedQueryMock);
        Mockito.when(queryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(queryMock);
        Mockito.when(typedQueryMock.setParameter(Mockito.any(String.class), Mockito.any())).thenReturn(typedQueryMock);
        Mockito.when(queryMock.getSingleResult()).thenReturn(10L); // Assuming there are 10 results
        Mockito.when(typedQueryMock.getResultList()).thenReturn(List.of(subject)); // Assuming a single result
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.empty());

        assertThrows(ApiException.class, () -> {
            subjectCriteria.searchSubject(subjectDTOFilter);
        }, messageException.MSG_USER_NOT_FOUND);
    }
}