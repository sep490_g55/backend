package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.Tag;
import com.capstone.backend.model.dto.tag.PagingTagDTOResponse;
import com.capstone.backend.model.dto.tag.TagDTOFilter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TagCriteriaTest {
    @InjectMocks
    TagCriteria tagCriteria;
    @Mock
    EntityManager em;
    //TH1: Search when search query is empty
    //TH2: Search when entering an invalid search term and no results are returned
    //TH3: Search when entering a valid search term and correct results are returned
    //TH4: Search when entering a search term with multiple words and corrects results are displayed
    //TH5: Search when entering a search term with special character
    //TH6: Search when entering a search term with upper case letters
    //TH7: Search when entering a search term with lower case letters
    //TH8: Search when entering a search term mix between lower case and upper case letters

    @Test
    void searchTag_with_empty_name() {
        // Mock data
        TagDTOFilter tagDTOFilter = new TagDTOFilter();
        tagDTOFilter.setName("");

        Query mockCountQuery = mock(Query.class);
        TypedQuery<Tag> mockTagTypedQuery = mock(TypedQuery.class);

        when(em.createQuery(anyString())).thenReturn(mockCountQuery);
        when(em.createQuery(anyString(), eq(Tag.class))).thenReturn(mockTagTypedQuery);
        when(mockCountQuery.getSingleResult()).thenReturn(0L);
        when(mockTagTypedQuery.getResultList()).thenReturn(Collections.emptyList());

        PagingTagDTOResponse result = tagCriteria.searchTag(tagDTOFilter);

        verify(em, times(1)).createQuery(anyString());
        verify(em, times(1)).createQuery(anyString(), eq(Tag.class));

        assertNotNull(result);
        assertEquals(0, result.getTotalElement());
        assertEquals(0, result.getTotalPage());
    }

    //TH2: Search when entering an invalid search term and no results are returned
    @Test
    void searchTag_with_invalid_search() {
        TagDTOFilter tagDTOFilter = new TagDTOFilter();
        tagDTOFilter.setName("hello");

        Query mockCountQuery = mock(Query.class);
        TypedQuery<Tag> mockTagTypedQuery = mock(TypedQuery.class);

        when(em.createQuery(anyString())).thenReturn(mockCountQuery);
        when(em.createQuery(anyString(), eq(Tag.class))).thenReturn(mockTagTypedQuery);


    }
}

