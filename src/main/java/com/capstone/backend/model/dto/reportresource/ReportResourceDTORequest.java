package com.capstone.backend.model.dto.reportresource;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReportResourceDTORequest {
    @NotBlank(message = "Report message is mandatory")
    String message;

    @NotNull(message = "Resource id is mandatory")
    Long resourceId;
}
