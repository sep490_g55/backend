package com.capstone.backend.model.dto.lesson;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LessonDTORequest {

    @NotBlank(message = "Lesson name is mandatory")
    @Size(min = 4, message = "Lesson name is greater than 3")
    String name;
}
