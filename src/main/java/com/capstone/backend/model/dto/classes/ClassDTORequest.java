package com.capstone.backend.model.dto.classes;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ClassDTORequest {
    @NotBlank(message = "Class name is mandatory")
    @Size(min = 4, message = "Class name is greater than 3")
    String name;
}
