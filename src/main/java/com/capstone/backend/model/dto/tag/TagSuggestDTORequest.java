package com.capstone.backend.model.dto.tag;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TagSuggestDTORequest {
    @NotNull(message = "Tag suggest is mandatory")
    String tagSuggest;
}
