package com.capstone.backend.model.dto.bookseriesSubject;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChangeSubjectDTORequest {
    @NotEmpty(message = "List subject is mandatory")
    List<Long> subjects;
}
