package com.capstone.backend.model.dto.chapter;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChapterDTORequest {
    @NotBlank(message = "Chapter name is mandatory")
    @Size(min = 4, message = "Chapter name is greater than 3")
    String name;
}
