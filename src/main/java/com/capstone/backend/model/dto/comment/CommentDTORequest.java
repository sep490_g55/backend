package com.capstone.backend.model.dto.comment;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CommentDTORequest {

    @NotNull(message = "Resource is mandatory")
    Long resourceId;

    Long commentRootId;

    @NotBlank(message = "Content is mandatory")
    String content;
}
