package com.capstone.backend.model.dto.bookvolume;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookVolumeDTORequest {
    @NotBlank(message = "Book volume name is mandatory")
    @Size(min = 4, message = "Book volume name is greater than 3")
    String name;
}
