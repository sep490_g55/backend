package com.capstone.backend.model.dto.resource;

import com.capstone.backend.model.dto.tag.TagSuggestDTOResponse;
import com.capstone.backend.model.dto.user.UserDTOResponse;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ResourceDetailDTOResponse {
    Boolean isLike;
    Boolean isUnlike;
    Long numberOfLike;
    Long numberOfUnlike;
    Boolean isSave;
    Long viewCount;
    List<TagSuggestDTOResponse> listTagRelate;
    ResourceDTOResponse resourceDTOResponse;
    UserDTOResponse owner;
}
