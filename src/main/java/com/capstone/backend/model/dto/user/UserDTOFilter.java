package com.capstone.backend.model.dto.user;

import com.capstone.backend.model.dto.BaseFilter;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDTOFilter extends BaseFilter {
    String name;
    Long roleId;
    Long classId;
    Boolean active;
}
