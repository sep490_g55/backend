package com.capstone.backend.model.dto.tag;


import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)

public class TagDTORequest {
    @Length(max = 50, message = "Tag length cannot be greater than 50")
    @NotEmpty(message = "Tag cannot be empty")
    String name;
}
