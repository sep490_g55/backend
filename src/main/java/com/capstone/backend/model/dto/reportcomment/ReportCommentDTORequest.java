package com.capstone.backend.model.dto.reportcomment;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReportCommentDTORequest {
    @NotBlank(message = "Report message is mandatory")
    String message;

    @NotNull(message = "Comment id is mandatory")
    Long commentId;
}
