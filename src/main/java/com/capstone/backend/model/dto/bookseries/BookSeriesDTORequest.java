package com.capstone.backend.model.dto.bookseries;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BookSeriesDTORequest {

    @NotBlank(message = "Book series name is mandatory")
    @Size(min = 4, message = "Book series name is greater than 3")
    String name;
}
