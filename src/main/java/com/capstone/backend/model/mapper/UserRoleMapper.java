package com.capstone.backend.model.mapper;

import com.capstone.backend.entity.Role;
import com.capstone.backend.entity.User;
import com.capstone.backend.entity.UserRole;
import com.capstone.backend.utils.DateTimeHelper;

public class UserRoleMapper {
    public static UserRole toUserRole(User user, Role role) {
        return UserRole.builder()
                .user(user)
                .role(role)
                .active(true)
                .createdAt(DateTimeHelper.getTimeNow())
                .build();
    }
}
