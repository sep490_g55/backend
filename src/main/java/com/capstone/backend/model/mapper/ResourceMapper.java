package com.capstone.backend.model.mapper;

import com.capstone.backend.entity.Class;
import com.capstone.backend.entity.*;
import com.capstone.backend.entity.type.ResourceType;
import com.capstone.backend.model.dto.resource.*;
import com.capstone.backend.model.dto.tag.TagSuggestDTOResponse;
import com.capstone.backend.utils.DataHelper;
import com.capstone.backend.utils.S3Util;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ResourceMapper {
    public static String getThumbnail(Resource resource, S3Util s3Util) {
        String thumbnailSrc = null;
        boolean isJpg = ResourceType.getListImages().stream()
                .anyMatch(r -> r == resource.getResourceType());
        if (isJpg) {
            return DataHelper.getLinkResource(resource, s3Util);
        } else return DataHelper.getLinkThumbnail(resource, s3Util);
    }

    public static ResourceDTOResponse toResourceDTOResponse(Resource resource, S3Util s3Util) {
        return ResourceDTOResponse.builder()
                .id(resource.getId())
                .name(resource.getName())
                .description(resource.getDescription())
                .resourceType(resource.getResourceType())
                .createdAt(resource.getCreatedAt())
                .active(resource.getActive())
                .approveType(resource.getApproveType())
                .visualType(resource.getVisualType())
                .thumbnailSrc(getThumbnail(resource, s3Util))
                .resourceSrc(s3Util.getPresignedUrl(resource.getResourceSrc()).toString())
                .point(resource.getPoint())
                .size(resource.getSize())
                .build();
    }

    public static ResourceViewDTOResponse toResourceViewDTOResponse(Resource resource, boolean isSave, S3Util s3Util) {
        String resourceSrc = resource.getResourceType() == ResourceType.MP4
                ? DataHelper.getLinkResource(resource, s3Util) : null;
        return ResourceViewDTOResponse.builder()
                .id(resource.getId())
                .thumbnailSrc(getThumbnail(resource, s3Util))
                .point(resource.getPoint())
                .name(resource.getName())
                .isSave(isSave)
                .resourceType(resource.getResourceType())
                .viewCount(resource.getViewCount())
                .resourceSrc(resourceSrc)
                .build();
    }

    public static ResourceMediaDTOCriteria toResourceMediaDTOCriteria(ResourceTag resourceTag) {
        return ResourceMediaDTOCriteria.builder()
                .tableType(resourceTag.getTableType())
                .detailId(resourceTag.getDetailId())
                .build();
    }

    public static UserSharedDTOResponse toUserSharedDTOResponse(User user, String permission, S3Util s3Util) {
        return UserSharedDTOResponse.builder()
                .userShareId(user.getId())
                .email(user.getEmail())
                .username(user.getUsername())
                .permission(permission)
                .avatar(DataHelper.getLinkAvatar(user, s3Util))
                .build();
    }

    public static ResourceShowDTOResponse toResourceDTOUpdateResponse(Resource resource) {
        Class classObject = null;
        Chapter chapter = null;
        BookVolume bookVolume = null;
        Subject subject = resource.getSubject();
        BookSeries bookSeries = null;
        Lesson lesson = resource.getLesson();
        if (lesson != null) {
            bookSeries = subject.getBookSeriesSubjects().get(0).getBookSeries();
            classObject = bookSeries.getClassObject();
            chapter = lesson.getChapter();
            bookVolume = chapter.getBookVolume();
        }
        List<TagSuggestDTOResponse> tagList = resource.getResourceTagList().stream()
                .map(ResourceMapper::toTagSuggestDTOResponse)
                .toList();

        return ResourceShowDTOResponse.builder()
                .id(resource.getId())
                .approveType(resource.getApproveType())
                .description(resource.getDescription())
                .name(resource.getName())
                .resourceSrc(resource.getResourceSrc())
                .resourceType(resource.getResourceType())
                .lessonId(lesson != null ? lesson.getId() : null)
                .chapterId(chapter != null ? chapter.getId() : null)
                .bookVolumeId(bookVolume != null ? bookVolume.getId() : null)
                .subjectId(subject.getId())
                .bookSeriesId(bookSeries != null ? bookSeries.getId() : null)
                .classId(classObject != null ? classObject.getId() : null)
                .tagList(tagList)
                .visualType(resource.getVisualType())
                .viewCount(resource.getViewCount())
                .build();
    }

    public static TagSuggestDTOResponse toTagSuggestDTOResponse(ResourceTag resourceTag) {
        return TagSuggestDTOResponse.builder()
                .tagId(resourceTag.getTag().getId())
                .tagName(resourceTag.getTag().getName())
                .build();
    }
}
