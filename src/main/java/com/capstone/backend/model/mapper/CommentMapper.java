package com.capstone.backend.model.mapper;

import com.capstone.backend.entity.Comment;
import com.capstone.backend.entity.ReportComment;
import com.capstone.backend.model.dto.comment.CommentDTOResponse;
import com.capstone.backend.model.dto.comment.CommentReportDetailDTOResponse;
import com.capstone.backend.model.dto.comment.UserReportComment;
import com.capstone.backend.utils.DataHelper;
import com.capstone.backend.utils.S3Util;

import java.util.List;

import static com.capstone.backend.utils.Constants.API_VERSION;
import static com.capstone.backend.utils.Constants.HOST;

public class CommentMapper {
    public static CommentDTOResponse toCommentDTOResponse(Comment comment, S3Util s3Util) {
        Long commentIdRoot = null;
        if (comment.getCommentRoot() != null) {
            commentIdRoot = comment.getCommentRoot().getCommentId();
        }
        return CommentDTOResponse.builder()
                .commentId(comment.getCommentId())
                .commenterDTOResponse(UserMapper.toUserDTOResponse(comment.getCommenter(), s3Util))
                .content(comment.getContent())
                .createdAt(comment.getCreatedAt())
                .active(comment.getActive())
                .commentRootId(commentIdRoot)
                .build();
    }

    public static UserReportComment toUserReportComment(ReportComment reportComment, S3Util s3Util) {
        Comment comment = reportComment.getComment();
        String fullName = comment.getCommenter().getFirstname() + " " + comment.getCommenter().getLastname();
        return UserReportComment.builder()
                .fullName(fullName)
                .content(reportComment.getMessage())
                .avatar(DataHelper.getLinkAvatar(comment.getCommenter(), s3Util))
                .build();
    }

    public static CommentReportDetailDTOResponse toCommentReportDetailDTOResponse(Comment comment, S3Util s3Util) {
        String fullName = comment.getCommenter().getFirstname() + " " + comment.getCommenter().getLastname();
        List<UserReportComment> userReportComments = comment.getReportCommentList().stream()
                .map(urc -> CommentMapper.toUserReportComment(urc, s3Util))
                .toList();
        return CommentReportDetailDTOResponse.builder()
                .commentId(comment.getCommentId())
                .name(comment.getResource().getName())
                .fullName(fullName)
                .avatar(DataHelper.getLinkAvatar(comment.getCommenter(), s3Util))
                .content(comment.getContent())
                .createdAt(comment.getCreatedAt())
                .linkResource(HOST + API_VERSION + "/resource/detail/" + comment.getResource().getId())
                .userReportComments(userReportComments)
                .build();
    }
}
