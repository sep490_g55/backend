package com.capstone.backend.repository;

import com.capstone.backend.entity.Resource;
import com.capstone.backend.entity.ResourceTag;
import com.capstone.backend.entity.Tag;
import com.capstone.backend.entity.type.TabResourceType;
import com.capstone.backend.entity.type.TableType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ResourceTagRepository extends JpaRepository<ResourceTag, Long> {

    @Query("select rt.tag from ResourceTag rt where rt.resource.id = :resourceId and rt.active = true and rt.tag.active = true order by rt.tag.createdAt")
    List<Tag> findAllTagName(Long resourceId);

    // ----------------------------------------------------------------------
    @Query("select r from Resource r where r.tabResourceType = :tabResourceType and r.id != :resourceId and " +
            "r.lesson.id = :lessonId and r.approveType = 'ACCEPTED' and r.visualType = 'PUBLIC' order by r.viewCount desc, r.createdAt desc")
    List<Resource> findAllResourceByLessonIdSameResourceTypeTest(TabResourceType tabResourceType, Long resourceId, Long lessonId);

    @Query("select distinct rt.resource from ResourceTag rt where rt.resource.tabResourceType = :tabResourceType and " +
            " rt.resource.id != :resourceId and rt.tag.name in (:tags) and rt.resource.visualType = 'PUBLIC' and " +
            " rt.resource.approveType = 'ACCEPTED' and rt.active = true and rt.resource.active = true")
    List<Resource> findAllResourceByTagNameSameResourceTypeTest(TabResourceType tabResourceType, Long resourceId, List<String> tags, Pageable pageable);

    @Query("select distinct r from Resource r where r.tabResourceType = :tabResourceType and r.subject.id = :subjectId " +
            " and r.approveType = 'ACCEPTED' and  r.visualType = 'PUBLIC' and r.active = true")
    List<Resource> findResourceBySubjectSameResourceTypeTest(TabResourceType tabResourceType, Long subjectId, Pageable pageable);

    @Query("select distinct r from Resource r where r.tabResourceType = :tabResourceType and r.approveType = 'ACCEPTED' and " +
            " r.visualType = 'PUBLIC' and r.active = true order by r.viewCount desc")
    List<Resource> findResourceByViewSameResourceTypeTest(TabResourceType tabResourceType, Pageable pageable);
    // ----------------------------------------------------------------------

    @Query("select rt from ResourceTag rt where rt.tag.id in (:tagIds) and  rt.active = true")
    Set<ResourceTag> findResourceTagByTagId(List<Long> tagIds);

    @Query("SELECT rt FROM ResourceTag rt WHERE rt.tableType= :tableType AND rt.detailId = :id AND rt.tag.active = TRUE AND rt.active = TRUE")
    List<ResourceTag> getAllResourceTagByTableTypeAndID(TableType tableType, long id);

    @Query("SELECT rt.tag FROM ResourceTag rt WHERE rt.tableType= :tableType AND rt.detailId = :id AND rt.tag.active = TRUE AND rt.active = TRUE")
    List<Tag> getAllTagByTableTypeAndID(TableType tableType, long id);

    @Query("SELECT rt FROM ResourceTag rt WHERE rt.tableType=?1 AND rt.detailId=?2 AND rt.tag.id = ?3 AND rt.active=TRUE")
    Optional<ResourceTag> findByTagIdAndByTableNameAndRowID(TableType tableType, long rowId, long tagId);

    @Query("select rt from ResourceTag rt where rt.resource.id = :resourceId and rt.tag.id = :tagId and rt.active = :active")
    ResourceTag findByTagAndResource(Long tagId, Long resourceId, Boolean active);

    @Query("select rt from ResourceTag rt where rt.resource.id = :resourceId and rt.active = true")
    List<ResourceTag> findByResourceId(Long resourceId);
}
