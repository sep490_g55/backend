package com.capstone.backend.repository;

import com.capstone.backend.entity.SystemPermission;
import com.capstone.backend.entity.type.MethodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SystemPermissionRepository extends JpaRepository<SystemPermission, Long> {

    @Query("select urp.permission from UserRolePermission  urp where urp.role.id = :roleId " +
            " and urp.active = true and urp.role.active = true and urp.permission.active = true")
    List<SystemPermission> findSystemPermissionByRoleId(Long roleId);

    @Query("select sp from SystemPermission sp where sp.path = :path and sp.methodType = :methodType and sp.id != :id")
    public Optional<SystemPermission> findByPathAndMethod(String path, MethodType methodType, Long id);

    @Query("select sp from SystemPermission sp where sp.active = true")
    public List<SystemPermission> findAll();
}
