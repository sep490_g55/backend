package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.Resource;
import com.capstone.backend.entity.type.ApproveType;
import com.capstone.backend.entity.type.VisualType;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.reportresource.ResourceReportDTOResponse;
import com.capstone.backend.model.dto.resource.ResourceDTOResponse;
import com.capstone.backend.model.dto.reviewResource.MaterialReviewDTOFilter;
import com.capstone.backend.model.dto.reviewResource.MediaReviewDTOFilter;
import com.capstone.backend.model.mapper.ReportResourceMapper;
import com.capstone.backend.model.mapper.ResourceMapper;
import com.capstone.backend.utils.S3Util;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@Repository
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ReviewResourceCriteria {
    EntityManager em;
    S3Util s3Util;

    public PagingDTOResponse searchReviewMaterial(MaterialReviewDTOFilter materialReviewDTOFilter, boolean isReport) {
        StringBuilder sql = new StringBuilder("select distinct re from Resource re left join re.lesson le left join"
                + " le.chapter cha left join cha.bookVolume bv left join bv.bookSeriesSubject bss left join"
                + " bss.subject su left join bss.bookSeries bs left join bs.classObject c where 1 = 1");
        em.createQuery(sql.toString());

        ApproveType approveType = ApproveType.UNACCEPTED;
        if (isReport) {
            approveType = ApproveType.ACCEPTED;
            String newSql = Pattern.compile("where 1 = 1").matcher(sql)
                    .replaceAll(" join re.reportResourceList rr where 1 = 1");
            sql = new StringBuilder(newSql);
        }
        System.out.println(sql);
        Map<String, Object> params = new HashMap<>();
        sql.append(" and re.visualType NOT IN (:excludeVisualType) and le.id != null ");
        params.put("excludeVisualType", VisualType.PRIVATE);

        if (materialReviewDTOFilter.getSubjectId() != null) {
            sql.append(" and su.id = :subjectId");
            params.put("subjectId", materialReviewDTOFilter.getSubjectId());
        }
        if (materialReviewDTOFilter.getClassId() != null) {
            sql.append(" and c.id = :classId");
            params.put("classId", materialReviewDTOFilter.getClassId());
        }
        if (materialReviewDTOFilter.getBookSeriesId() != null) {
            sql.append(" and bs.id = :bookSeriesId");
            params.put("bookSeriesId", materialReviewDTOFilter.getBookSeriesId());
        }
        if (materialReviewDTOFilter.getBookVolumeId() != null) {
            sql.append(" and bv.id = :bookVolumeId");
            params.put("bookVolumeId", materialReviewDTOFilter.getBookVolumeId());
        }
        if (materialReviewDTOFilter.getChapterId() != null) {
            sql.append(" and cha.id = :chapterId");
            params.put("chapterId", materialReviewDTOFilter.getChapterId());
        }
        if (materialReviewDTOFilter.getLessonId() != null) {
            sql.append(" and le.id = :lessonId");
            params.put("lessonId", materialReviewDTOFilter.getLessonId());
        }
        if (materialReviewDTOFilter.getTabResourceType() != null) {
            sql.append(" and re.tabResourceType = :tabResourceType");
            params.put("tabResourceType", materialReviewDTOFilter.getTabResourceType());
        }
        if (materialReviewDTOFilter.getVisualType() != null) {
            sql.append(" and re.visualType = :visualType");
            params.put("visualType", materialReviewDTOFilter.getVisualType());
        }

        if (materialReviewDTOFilter.getResourceType() != null) {
            sql.append(" and re.resourceType = :resourceType");
            params.put("resourceType", materialReviewDTOFilter.getResourceType());
        }

        sql.append(" and re.approveType = :approveType and re.active = true ");
        params.put("approveType", approveType);

        if (materialReviewDTOFilter.getName() != null) {
            sql.append(" and re.name like :name ");
            params.put("name", "%" + materialReviewDTOFilter.getName() + "%");
        }

        sql.append(" order by re.createdAt DESC");

        Long pageIndex = materialReviewDTOFilter.getPageIndex();
        Long pageSize = materialReviewDTOFilter.getPageSize();
        return getPagingResourceDTOResponse(sql.toString(), pageIndex, pageSize, params, isReport);
    }

    public Object searchReviewMedia(MediaReviewDTOFilter mediaReviewDTOFilter, boolean isReport) {
        StringBuilder sql = new StringBuilder("select distinct re from Resource re join re.subject su where 1 = 1");
        em.createQuery(sql.toString());
        ApproveType approveType = ApproveType.UNACCEPTED;
        if (isReport) {
            approveType = ApproveType.ACCEPTED;
            String newSql = Pattern.compile("where 1 = 1").matcher(sql)
                    .replaceAll(" join re.reportResourceList rr where 1 = 1 and rr.active = true");
            sql = new StringBuilder(newSql);
        }
        Map<String, Object> params = new HashMap<>();
        sql.append(" and re.visualType NOT IN (:excludeVisualType) and re.lesson.id = null");
        params.put("excludeVisualType", VisualType.PRIVATE);

        if (mediaReviewDTOFilter.getTabResourceType() != null) {
            sql.append(" and re.tabResourceType = :tabResourceType");
            params.put("tabResourceType", mediaReviewDTOFilter.getTabResourceType());
        }
        if (mediaReviewDTOFilter.getVisualType() != null) {
            sql.append(" and re.visualType = :visualType");
            params.put("visualType", mediaReviewDTOFilter.getVisualType());
        }

        if (mediaReviewDTOFilter.getResourceType() != null) {
            sql.append(" and re.resourceType = :resourceType");
            params.put("resourceType", mediaReviewDTOFilter.getResourceType());
        }

        sql.append(" and re.approveType = :approveType and re.active = true ");
        params.put("approveType", approveType);

        if (mediaReviewDTOFilter.getName() != null) {
            sql.append(" and re.name like :name ");
            params.put("name", "%" + mediaReviewDTOFilter.getName() + "%");
        }
        sql.append(" order by re.createdAt DESC");

        Long pageIndex = mediaReviewDTOFilter.getPageIndex();
        Long pageSize = mediaReviewDTOFilter.getPageSize();
        return getPagingResourceDTOResponse(sql.toString(), pageIndex, pageSize, params, isReport);
    }

    private PagingDTOResponse getPagingResourceDTOResponse(String sql, Long pageIndex, Long pageSize, Map<String, Object> params, boolean isReport) {
        Query countQuery = em.createQuery(sql.replace("select distinct re", "select count(distinct re.id)"));
        TypedQuery<Resource> resourceTypedQuery = em.createQuery(sql, Resource.class);
        params.forEach((k, v) -> {
            resourceTypedQuery.setParameter(k, v);
            countQuery.setParameter(k, v);
        });

        resourceTypedQuery.setFirstResult((int) ((pageIndex - 1) * pageSize));
        resourceTypedQuery.setMaxResults(Math.toIntExact(pageSize));
        List<Resource> resourceList = resourceTypedQuery.getResultList();

        Long totalResource = (Long) countQuery.getSingleResult();
        long totalPage = totalResource / pageSize;
        if (totalResource % pageSize != 0) {
            totalPage++;
        }

        if (isReport) {
            List<ResourceReportDTOResponse> resourceReportDTOResponses
                    = resourceList.stream().map(rr -> ReportResourceMapper.toResourceReportDTOResponse(rr, s3Util)).toList();
            return PagingDTOResponse.builder()
                    .totalElement(totalResource)
                    .totalPage(totalPage)
                    .data(assignIdReport(resourceReportDTOResponses))
                    .build();
        }
        List<ResourceDTOResponse> resourceDTOResponseList = resourceList.stream()
                .map(resource -> ResourceMapper.toResourceDTOResponse(resource, s3Util)).toList();
        return PagingDTOResponse.builder()
                .totalElement(totalResource)
                .totalPage(totalPage)
                .data(assignId(resourceDTOResponseList))
                .build();
    }

    public List<ResourceDTOResponse> assignId(List<ResourceDTOResponse> resourceDTOResponses) {
        return resourceDTOResponses.stream()
                .peek(ur -> {
                    String name = ur.getName().concat("(").concat(ur.getId().toString()).concat(")");
                    ur.setName(name);
                }).toList();
    }

    public List<ResourceReportDTOResponse> assignIdReport(List<ResourceReportDTOResponse> resourceReportDTOResponses) {
        return resourceReportDTOResponses.stream()
                .peek(ur -> {
                    String name = ur.getName().concat("(").concat(ur.getId().toString()).concat(")");
                    ur.setName(name);
                }).toList();
    }
}
