package com.capstone.backend.repository.criteria;

import com.capstone.backend.entity.Resource;
import com.capstone.backend.entity.User;
import com.capstone.backend.entity.type.ActionType;
import com.capstone.backend.entity.type.ApproveType;
import com.capstone.backend.entity.type.VisualType;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.materials.MaterialsFilterDTORequest;
import com.capstone.backend.model.dto.resource.ResourceViewDTOResponse;
import com.capstone.backend.model.mapper.ResourceMapper;
import com.capstone.backend.repository.UserResourceRepository;
import com.capstone.backend.utils.CheckPermissionResource;
import com.capstone.backend.utils.S3Util;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MaterialsCriteria {
    S3Util s3Util;
    EntityManager em;
    UserHelper userHelper;
    UserResourceRepository userResourceRepository;
    CheckPermissionResource checkPermissionResource;

    public PagingDTOResponse searchMaterials(MaterialsFilterDTORequest request) {
        User userLoggedIn = userHelper.getUserLogin();
        StringBuilder sql = new StringBuilder("select re from Resource re left join re.lesson le left join " +
                "le.chapter cha left join cha.bookVolume bv left join bv.bookSeriesSubject bvs left join " +
                "bvs.subject s left join bvs.bookSeries bs left join bs.classObject c where re.active = true " +
                "and c.active = true and bs.active = true and s.active = true and bv.active = true " +
                "and cha.active = true and le.active = true and bvs.active = true ");

        Map<String, Object> params = new HashMap<>();

        if (request.getClassId() != null) {
            sql.append(" and c.id = :classId and c.active = true");
            params.put("classId", request.getClassId());
        }

        if (request.getBookSeriesId() != null) {
            sql.append(" and bs.id = :bookSeriesId and bs.active = true");
            params.put("bookSeriesId", request.getBookSeriesId());
        }

        if (request.getSubjectId() != null) {
            sql.append(" and bvs.id = :subjectId and bvs.active = true");
            params.put("subjectId", request.getSubjectId());
        }

        if (request.getBookVolumeId() != null) {
            sql.append(" and bv.id = :bookVolumeId and bv.active = true");
            params.put("bookVolumeId", request.getBookVolumeId());
        }

        if (request.getChapterId() != null) {
            sql.append(" and cha.id = :chapterId and cha.active = true");
            params.put("chapterId", request.getChapterId());
        }

        if (request.getLessonId() != null) {
            sql.append(" and le.id = :lessonId and le.active = true");
            params.put("lessonId", request.getLessonId());
        }

        sql.append(" and re.tabResourceType = :tabResourceType");
        params.put("tabResourceType", request.getTabResourceType());

        sql.append(" and re.approveType = :approveType ");
        params.put("approveType", ApproveType.ACCEPTED);

        sql.append(" and re.visualType = :visualType ");
        params.put("visualType", VisualType.PUBLIC);

        sql.append(" order by re.viewCount desc, re.createdAt desc ");

        Long pageIndex = request.getPageIndex();
        Long pageSize = request.getPageSize();

        Query countQuery = em.createQuery(sql.toString().replace("select re", "select count(re.id)"));

        TypedQuery<Resource> resourceTypedQuery = em.createQuery(sql.toString(), Resource.class);
        params.forEach((k, v) -> {
            resourceTypedQuery.setParameter(k, v);
            countQuery.setParameter(k, v);
        });

        resourceTypedQuery.setFirstResult((int) ((pageIndex - 1) * pageSize));
        resourceTypedQuery.setMaxResults(Math.toIntExact(pageSize));
        List<Resource> resourceList = resourceTypedQuery.getResultList().stream()
                .filter(checkPermissionResource::needCheckPermissionSearchResource)
                .toList();

        long totalResource = (Long) countQuery.getSingleResult();
        long totalPage = totalResource / pageSize;
        if (totalResource % pageSize != 0) {
            totalPage++;
        }

        List<ResourceViewDTOResponse> resourceViewDTOResponses = resourceList.stream()
                .map(resource -> {
                    boolean isSave = false;
                    if (userLoggedIn != null)
                        isSave = userResourceRepository
                                .findUserResourceHasActionType(userLoggedIn.getId(), resource.getId(), ActionType.SAVED)
                                .isPresent();
                    return ResourceMapper.toResourceViewDTOResponse(resource, isSave, s3Util);
                })
                .toList();

        return PagingDTOResponse.builder()
                .totalPage(totalPage)
                .totalElement(totalResource)
                .data(resourceViewDTOResponses)
                .build();
    }
}
