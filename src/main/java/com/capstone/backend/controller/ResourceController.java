package com.capstone.backend.controller;

import com.capstone.backend.entity.type.VisualType;
import com.capstone.backend.model.dto.materials.MaterialsFilterDTORequest;
import com.capstone.backend.model.dto.resource.ResourceDTOUpdateRequest;
import com.capstone.backend.model.dto.resource.ResourceMediaDTOFilter;
import com.capstone.backend.model.dto.resource.ResourceSharedDTORequest;
import com.capstone.backend.model.dto.resource.UserShareSuggestDTORequest;
import com.capstone.backend.model.dto.tag.TagSuggestDTORequest;
import com.capstone.backend.service.ResourceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import static com.capstone.backend.utils.Constants.API_VERSION;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(API_VERSION + "/resource")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Tag(name = "Resource", description = "API for Resource")
@CrossOrigin
public class ResourceController {
    ResourceService resourceService;

    @GetMapping("/relate/{id}")
    public ResponseEntity<?> getListResourceRelates(@PathVariable Long id) {
        return ResponseEntity.ok(resourceService.getListResourceRelates(id));
    }

    @GetMapping("/comment-list/{id}")
    public ResponseEntity<?> getListCommentsResource(@PathVariable Long id) {
        return ResponseEntity.ok(resourceService.getListCommentsResource(id));
    }

    @GetMapping("/visual/{id}")
    public ResponseEntity<?> changeVisualType(
            @PathVariable Long id,
            @RequestParam VisualType type
    ) {
        return ResponseEntity.ok(resourceService.changeVisualType(id, type));
    }

    //checked permission
    @GetMapping("/detail/{id}")
    @Operation(summary = "See a detail resource (comment, resource, like, unlike, resource relate ...)")
    public ResponseEntity<?> getResourceDetailById(@Valid @PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(resourceService.getResourceDetailById(id));
    }

    //checked permission
    @GetMapping("/materials")
    public ResponseEntity<?> searchMaterials(
            @Valid @ModelAttribute MaterialsFilterDTORequest request
    ) {
        return ResponseEntity.ok(resourceService.searchMaterials(request));
    }

    @Operation(summary = "Tag for search media")
    @PostMapping("/tags")
    public ResponseEntity<?> getListTagsSuggest(@RequestBody TagSuggestDTORequest request) {
        return ResponseEntity.ok(resourceService.getListTagsSuggest(request));
    }

    @Operation(summary = "Tag for add resource")
    @PostMapping("/tags-global")
    public ResponseEntity<?> getListTagsGlobalSuggest(@RequestBody TagSuggestDTORequest request) {
        return ResponseEntity.ok(resourceService.getListTagsGlobalSuggest(request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> viewResource(@PathVariable Long id) {
        return ResponseEntity.ok(resourceService.viewResource(id));
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> updateResource(
            @PathVariable Long id,
            @ModelAttribute ResourceDTOUpdateRequest request,
            @RequestParam(value = "file", required = false) MultipartFile[] file
    ) {
        request.setId(id);
        return ResponseEntity.ok(resourceService.updateResource(request, file));
    }

    //check permission resource
    @GetMapping("/medias")
    public ResponseEntity<?> searchMediaResource(
            @Valid @ModelAttribute ResourceMediaDTOFilter resourceDTOFilter
    ) {
        return ResponseEntity.ok(resourceService.searchMediaResource(resourceDTOFilter));
    }

    @GetMapping("/download/{filename:.+}")
    @Operation(summary = "Download a resource")
    public ResponseEntity<?> downloadResource(@PathVariable(name = "filename", required = true) String fileName) {
        byte[] file = resourceService.downloadResource(fileName);
        return ResponseEntity.ok()
                .header(
                        HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + fileName + "\""
                )
                .body(file);
    }

    //check permission resource
    @GetMapping("/share/{resourceId}")
    public ResponseEntity<?> viewResourceShareById(@PathVariable Long resourceId) {
        return ResponseEntity.ok(resourceService.viewResourceShareById(resourceId));
    }

    //check permission resource
    @PostMapping("/share/suggest")
    public ResponseEntity<?> suggestionUserShare(@RequestBody UserShareSuggestDTORequest request) {
        return ResponseEntity.ok(resourceService.suggestionUserShare(request.getText()));
    }

    //check permission resource
    @PostMapping("/share/{resourceId}")
    public ResponseEntity<?> shareResource(
            @RequestBody ResourceSharedDTORequest request,
            @PathVariable Long resourceId
    ) {
        request.setResourceId(resourceId);
        return ResponseEntity.ok(resourceService.shareResource(request));
    }

}
