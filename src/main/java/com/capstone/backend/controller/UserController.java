package com.capstone.backend.controller;

import com.capstone.backend.model.dto.user.UserChangePasswordDTORequest;
import com.capstone.backend.model.dto.user.UserDTOFilter;
import com.capstone.backend.model.dto.user.UserDTOUpdate;
import com.capstone.backend.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import static com.capstone.backend.utils.Constants.API_VERSION;

@RestController
@RequiredArgsConstructor
@RequestMapping(API_VERSION + "/users")
@Tag(name = "User", description = "API for User")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@CrossOrigin
public class UserController {
    UserService userService;

    @PatchMapping("/change-password")
    public ResponseEntity<?> changePassword(
            @RequestBody UserChangePasswordDTORequest request
    ) {
        return ResponseEntity.ok(userService.changePassword(request));
    }

    @GetMapping("/preview")
    public ResponseEntity<?> previewInfo() {
        return ResponseEntity.ok(userService.previewInfo());
    }

    @Operation(summary = "Search User")
    @GetMapping("/display")
    public ResponseEntity<?> searchUser(@ModelAttribute UserDTOFilter userDTOFilter) {
        return ResponseEntity.ok(userService.searchUser(userDTOFilter));
    }

    @Operation(summary = "Update User")
    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@Valid @RequestBody UserDTOUpdate request, @PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(userService.updateUser(request, id));
    }

    @Operation(summary = "Reset Password User")
    @GetMapping("/reset/{id}")
    public ResponseEntity<?> resetPasswordUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.resetPasswordUser(id));
    }

    @Operation(summary = "Get user by Id")
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @Operation(summary = "Change active User")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> changeActive(@PathVariable @NotEmpty Long id) {
        return ResponseEntity.ok(userService.changeActive(id));
    }
}
