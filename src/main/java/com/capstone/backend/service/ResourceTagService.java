package com.capstone.backend.service;

import com.capstone.backend.model.dto.resourcetag.ResourceTagDTOResponse;
import com.capstone.backend.model.dto.resourcetag.ResourceTagDTOUpdate;
import com.capstone.backend.model.dto.resourcetag.ResourceTagDetailDTOResponse;

import java.util.List;

public interface ResourceTagService {
    List<ResourceTagDTOResponse> getAllResourceTagByTableTypeAndID(String tableType, long detailId);

    ResourceTagDTOResponse disableTagFromResource(long id);

    ResourceTagDetailDTOResponse updateResourceTag(ResourceTagDTOUpdate request);
}
