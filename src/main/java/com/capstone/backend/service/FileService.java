package com.capstone.backend.service;

import com.capstone.backend.entity.User;
import com.capstone.backend.model.dto.resource.FileDTOResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {
    List<FileDTOResponse> uploadMultiFile(MultipartFile[] files, Long lessonId, String filenameOption);

    byte[] downloadFile(String fileName);

    String setAvatar(MultipartFile avatar, User userLoggedIn);

    FileDTOResponse uploadSingleFile(MultipartFile multipartFile, String fileName, Long lessonId);

    void deleteFileResource(String filename);
}
