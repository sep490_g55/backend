package com.capstone.backend.service;

import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.subject.SubjectDTOFilter;
import com.capstone.backend.model.dto.subject.SubjectDTORequest;
import com.capstone.backend.model.dto.subject.SubjectDTOResponse;

import java.util.List;

public interface SubjectService {
    SubjectDTOResponse createSubject(SubjectDTORequest request);


    SubjectDTOResponse updateSubject(Long id, SubjectDTORequest request);

    void changeStatus(Long id);

    PagingDTOResponse searchSubject(SubjectDTOFilter subjectDTOFilter);

    SubjectDTOResponse viewSubjectById(Long id);

    List<SubjectDTOResponse> getListSubjects();

    List<SubjectDTOResponse> getListSubjectsByBookSeries(Long bookSeriesId);
}
