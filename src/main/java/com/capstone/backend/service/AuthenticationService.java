package com.capstone.backend.service;

import com.capstone.backend.model.dto.authentication.AuthenticationDTORequest;
import com.capstone.backend.model.dto.authentication.AuthenticationDTOResponse;
import com.capstone.backend.model.dto.register.RegisterDTORequest;
import com.capstone.backend.model.dto.register.RegisterDTOResponse;
import com.capstone.backend.model.dto.register.RegisterDTOUpdate;
import com.capstone.backend.model.dto.user.UserEmailDTORequest;
import com.capstone.backend.model.dto.user.UserForgotPasswordDTORequest;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AuthenticationService {
    RegisterDTOResponse register(RegisterDTORequest request);

    AuthenticationDTOResponse login(AuthenticationDTORequest request) throws Exception;

    Boolean updateRegister(RegisterDTOUpdate request);

    String forgotPassword(UserEmailDTORequest request);

    Boolean changePasswordForgot(UserForgotPasswordDTORequest request);

    Boolean logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication);
}
