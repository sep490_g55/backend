package com.capstone.backend.service;


import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.profle.ProfileDTOResponse;
import com.capstone.backend.model.dto.profle.ProfileDTOUpdate;
import com.capstone.backend.model.dto.user.*;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {
    ProfileDTOResponse viewProfile();

    ProfileDTOResponse updateProfile(ProfileDTOUpdate profileDTOUpdate);

    ProfileDTOResponse changeAvatar(MultipartFile avatar);

    Boolean changePassword(UserChangePasswordDTORequest request);

    PreviewInfoDTOResponse previewInfo();

    PagingDTOResponse searchUser(UserDTOFilter userDTOFilter);

    boolean resetPasswordUser(Long id);

    UserViewDTOResponse getUserById(Long id);

    Boolean updateUser(UserDTOUpdate request, Long id);

    boolean changeActive(Long id);
}
