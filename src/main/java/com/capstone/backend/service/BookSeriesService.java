package com.capstone.backend.service;

import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.bookseries.BookSeriesDTOFilter;
import com.capstone.backend.model.dto.bookseries.BookSeriesDTORequest;
import com.capstone.backend.model.dto.bookseries.BookSeriesDTOResponse;

import java.util.List;

public interface BookSeriesService {
    BookSeriesDTOResponse createBookSeries(BookSeriesDTORequest request, Long classId);

    BookSeriesDTOResponse updateBookSeries(Long id, BookSeriesDTORequest request);

    void changeStatus(Long id);

    PagingDTOResponse searchBookSeries(BookSeriesDTOFilter bookSeriesDTOFilter, Long classId);

    BookSeriesDTOResponse viewBookSeriesById(Long id);

    List<BookSeriesDTOResponse> getListBookSeriesByClassesSubjectId(Long subjectId, Long classId);

    List<BookSeriesDTOResponse> getListBookSeriesByClassId(Long classId);
}
