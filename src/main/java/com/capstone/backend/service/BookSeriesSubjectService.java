package com.capstone.backend.service;

import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.bookseriesSubject.BookSeriesSubjectDTOFilter;
import com.capstone.backend.model.dto.bookseriesSubject.BookSeriesSubjectDTOResponse;
import com.capstone.backend.model.dto.subject.SubjectShowDTOResponse;

import java.util.List;

public interface BookSeriesSubjectService {
    List<SubjectShowDTOResponse> viewBookSeriesSubjectById(Long bookSeriesId);

    PagingDTOResponse searchBookSeriesSubject(BookSeriesSubjectDTOFilter bookSeriesSubjectDTOFilter, Long bookSeriesId);

    List<BookSeriesSubjectDTOResponse> getListBookSeriesSubject();

    Boolean changeSubjectInBookSeries(List<Long> subjects, Long bookSeriesId);

    Boolean changeStatus(Long id);
}
