package com.capstone.backend.service;

import com.capstone.backend.entity.Tag;
import com.capstone.backend.model.dto.tag.PagingTagDTOResponse;
import com.capstone.backend.model.dto.tag.TagDTOFilter;
import com.capstone.backend.model.dto.tag.TagDTORequest;
import com.capstone.backend.model.dto.tag.TagDTOResponse;

public interface TagService {
    Tag saveTag(Tag tag);

    TagDTOResponse createTag(TagDTORequest tagDTORequest);

    TagDTOResponse disableTag(long id);

    TagDTOResponse getTagByID(long id);

    PagingTagDTOResponse searchTags(TagDTOFilter tagDTOFilter);
}
