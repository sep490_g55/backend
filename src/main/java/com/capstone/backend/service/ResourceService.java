package com.capstone.backend.service;

import com.capstone.backend.entity.type.VisualType;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.comment.CommentDetailDTOResponse;
import com.capstone.backend.model.dto.materials.MaterialsFilterDTORequest;
import com.capstone.backend.model.dto.resource.*;
import com.capstone.backend.model.dto.tag.TagSuggestDTORequest;
import com.capstone.backend.model.dto.tag.TagSuggestDTOResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ResourceService {
    List<ResourceDTOResponse> uploadResource(ResourceDTORequest request, MultipartFile[] files);

    ResourceDetailDTOResponse getResourceDetailById(Long id);

    PagingDTOResponse searchMediaResource(ResourceMediaDTOFilter resourceDTOFilter);

    PagingDTOResponse searchMaterials(MaterialsFilterDTORequest request);

    byte[] downloadResource(String fileName);

    Boolean shareResource(ResourceSharedDTORequest request);

    ResourceSharedDTOResponse viewResourceShareById(Long resourceId);

    List<UserSharedDTOResponse> suggestionUserShare(String text);

    List<TagSuggestDTOResponse> getListTagsSuggest(TagSuggestDTORequest request);

    List<TagSuggestDTOResponse> getListTagsGlobalSuggest(TagSuggestDTORequest request);

    ResourceShowDTOResponse viewResource(Long id);

    ResourceDTOResponse updateResource(ResourceDTOUpdateRequest request, MultipartFile[] file);

    Boolean deleteResource(Long id);

    Boolean changeVisualType(Long id, VisualType type);

    List<ResourceViewDTOResponse> getListResourceRelates(Long resourceId);

    List<CommentDetailDTOResponse> getListCommentsResource(Long id);
}
