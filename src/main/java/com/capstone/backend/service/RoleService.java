package com.capstone.backend.service;

import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.role.*;
import com.capstone.backend.utils.S3Util;

import java.util.List;

public interface RoleService {
    PagingDTOResponse viewSearchRole(RoleDTOFilter request);

    RoleDTODetailResponse getRoleById(Long id);

    RoleDTODetailResponse createRole(RoleDTORequest request);

    RoleDTODetailResponse updateRole(RoleDTOUpdate request);

    Boolean changeStatus(Long id);

    UserRoleDTOResponse getListRoleUser(S3Util s3Util);

    List<RoleDTODisplay> getListRoleActive();
}
