package com.capstone.backend.service;

import com.capstone.backend.entity.User;

public interface ConfirmationTokenService {
    String generateTokenEmail(User user);

    String goToForgotPassword(String token);
}
