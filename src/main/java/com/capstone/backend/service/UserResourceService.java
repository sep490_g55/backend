package com.capstone.backend.service;

import com.capstone.backend.model.dto.userresource.MyUserResourceDTOFilter;
import com.capstone.backend.model.dto.userresource.PagingUserResourceDTOResponse;
import com.capstone.backend.model.dto.userresource.UserResourceRequest;
import com.capstone.backend.model.dto.userresource.UserResourceSavedOrSharedDTOFilter;

public interface UserResourceService {
    Boolean actionResource(UserResourceRequest request);

    PagingUserResourceDTOResponse viewSearchUserResourceSaved(UserResourceSavedOrSharedDTOFilter request);

    PagingUserResourceDTOResponse viewSearchUserResourceShared(UserResourceSavedOrSharedDTOFilter request);

    PagingUserResourceDTOResponse viewSearchMyUserResource(MyUserResourceDTOFilter request);

    Boolean deleteSavedResource(Long id);

    Boolean deleteSharedResource(Long id);
}
