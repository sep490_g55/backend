package com.capstone.backend.service;

import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.systempermission.*;

import java.util.List;

public interface SystemPermissionService {
    PagingDTOResponse viewSearchPermission(SystemPermissionDTOFilter request);

    SystemPermissionDTOResponse getSystemPermissionById(Long id);

    SystemPermissionDTOResponse createSystemPermission(SystemPermissionDTORequest systemPermissionDTORequest);

    SystemPermissionDTOResponse updateSystemPermission(SystemPermissionDTOUpdate systemPermissionDTOUpdate);

    Boolean changeStatus(Long id);

    List<PermissionDTODisplay> getSystemPermissionsByRole(Long roleId);

    List<PermissionDTODisplay> getListSystemPermissions();
}
