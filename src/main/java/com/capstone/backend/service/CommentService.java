package com.capstone.backend.service;

import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.comment.*;

import java.util.List;

public interface CommentService {
    CommentDTOResponse createComment(CommentDTORequest request);

    List<CommentDetailDTOResponse> getListCommentDetailDTOResponse(Long resourceId);

    List<CommentDTOResponse> seeMoreReplyComment(Long id);

    CommentReportDetailDTOResponse getReportDetailComment(Long id);

    Boolean changeStatus(Long id);

    PagingDTOResponse searchComment(CommentDTOFilter commentDTOFilter);
}
