package com.capstone.backend.service;

import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.chapter.ChapterDTOFilter;
import com.capstone.backend.model.dto.chapter.ChapterDTORequest;
import com.capstone.backend.model.dto.chapter.ChapterDTOResponse;

import java.util.List;

public interface ChapterService {
    ChapterDTOResponse createChapter(ChapterDTORequest request, Long bookVolumeId);

    ChapterDTOResponse updateChapter(Long id, ChapterDTORequest request);

    void changeStatus(Long id);

    PagingDTOResponse searchChapter(ChapterDTOFilter chapterDTOFilter, Long bookVolumeId);

    ChapterDTOResponse viewChapterById(Long id);

    List<ChapterDTOResponse> getListChapterByBookVolumeId(Long bookVolumeId);
}
