package com.capstone.backend.service.impl;

import com.capstone.backend.entity.ConfirmationToken;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.repository.ConfirmTokenRepository;
import com.capstone.backend.service.ConfirmationTokenService;
import com.capstone.backend.utils.Constants;
import com.capstone.backend.utils.DateTimeHelper;
import com.capstone.backend.utils.MessageException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {
    ConfirmTokenRepository confirmTokenRepository;
    MessageException messageException;

    @Override
    public String generateTokenEmail(User user) {
        String token = UUID.randomUUID().toString();
        ConfirmationToken ct = ConfirmationToken.builder()
                .token(token)
                .createdAt(DateTimeHelper.getTimeNow())
                .expiresAt(DateTimeHelper.getTimeNow().plusMinutes(Constants.EMAIL_WAITING_EXPIRATION))
                .user(user)
                .build();
        confirmTokenRepository.save(ct);
        return token;
    }

    @Override
    public String goToForgotPassword(String token) {
        ConfirmationToken confirmationToken = confirmTokenRepository.findByToken(token)
                .orElseThrow(() -> ApiException.notFoundException(messageException.MSG_TOKEN_NOT_FOUND));

        if (confirmationToken.getConfirmedAt() != null) {
            throw ApiException.badRequestException(messageException.MSG_USER_EMAIL_CONFIRMED);
        }

        LocalDateTime expiredAt = confirmationToken.getExpiresAt();
        if (expiredAt.isBefore(DateTimeHelper.getTimeNow())) {
            throw ApiException.badRequestException(messageException.MSG_TOKEN_EXPIRED);
        }
        //link front-end
        return "confirm-password";
    }
}
