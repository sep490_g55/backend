package com.capstone.backend.service.impl;

import com.capstone.backend.entity.ReportResource;
import com.capstone.backend.entity.Resource;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.reportresource.ReportResourceDTORequest;
import com.capstone.backend.model.dto.userresource.PagingUserResourceDTOResponse;
import com.capstone.backend.model.dto.userresource.ReportResourceDTOFilter;
import com.capstone.backend.repository.ReportResourceRepository;
import com.capstone.backend.repository.ResourceRepository;
import com.capstone.backend.repository.criteria.UserResourceCriteria;
import com.capstone.backend.service.ReportResourceService;
import com.capstone.backend.utils.DateTimeHelper;
import com.capstone.backend.utils.FileHelper;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ReportResourceServiceImpl implements ReportResourceService {
    ResourceRepository resourceRepository;
    ReportResourceRepository reportResourceRepository;
    MessageException messageException;
    UserResourceCriteria userResourceCriteria;
    UserHelper userHelper;

    @Override
    public Boolean createReportResource(ReportResourceDTORequest request) {
        User reporter = userHelper.getUserLogin();
        Resource resource = resourceRepository.findById(request.getResourceId())
                .orElseThrow(() -> ApiException.notFoundException(messageException.MSG_RESOURCE_NOT_FOUND));

        if (Objects.equals(resource.getAuthor().getId(), reporter.getId())) {
            throw ApiException.badRequestException("You can not report your resource");
        }

        if (FileHelper.checkContentInputValid(request.getMessage()))
            throw ApiException.badRequestException(messageException.MSG_TEXT_NO_STANDARD_WORD);

        ReportResource reportResource = ReportResource.builder()
                .message(request.getMessage())
                .createdAt(DateTimeHelper.getTimeNow())
                .active(true)
                .reporter(reporter)
                .resource(resource)
                .build();

        reportResourceRepository.save(reportResource);
        return true;
    }

    @Override
    public PagingUserResourceDTOResponse viewSearchMyReportResource(ReportResourceDTOFilter request) {
        return userResourceCriteria.viewSearchMyReportResource(request);
    }
}
