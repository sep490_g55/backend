package com.capstone.backend.service.impl;

import com.capstone.backend.entity.Comment;
import com.capstone.backend.entity.ReportComment;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.reportcomment.ReportCommentDTORequest;
import com.capstone.backend.repository.CommentRepository;
import com.capstone.backend.repository.ReportCommentRepository;
import com.capstone.backend.service.ReportCommentService;
import com.capstone.backend.utils.DateTimeHelper;
import com.capstone.backend.utils.FileHelper;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ReportCommentServiceImpl implements ReportCommentService {
    CommentRepository commentRepository;
    UserHelper userHelper;
    ReportCommentRepository reportCommentRepository;
    MessageException messageException;

    @Override
    public Boolean createReportComment(ReportCommentDTORequest request) {
        Comment comment = commentRepository.findById(request.getCommentId())
                .orElseThrow(() -> ApiException.notFoundException(messageException.MSG_COMMENT_NOT_FOUND));
        User reporter = userHelper.getUserLogin();

        if (FileHelper.checkContentInputValid(request.getMessage()))
            throw ApiException.badRequestException(messageException.MSG_TEXT_NO_STANDARD_WORD);

        ReportComment reportComment = ReportComment.builder()
                .message(request.getMessage())
                .createdAt(DateTimeHelper.getTimeNow())
                .active(true)
                .comment(comment)
                .reporter(reporter)
                .build();

        if (comment.getCommenter() == reporter) {
            throw ApiException.badRequestException("You can not report your comment");
        }

        reportCommentRepository.save(reportComment);
        return true;
    }
}
