package com.capstone.backend.service.impl;

import com.capstone.backend.entity.*;
import com.capstone.backend.entity.type.*;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.comment.CommentDetailDTOResponse;
import com.capstone.backend.model.dto.materials.MaterialsFilterDTORequest;
import com.capstone.backend.model.dto.resource.*;
import com.capstone.backend.model.dto.tag.TagSuggestDTORequest;
import com.capstone.backend.model.dto.tag.TagSuggestDTOResponse;
import com.capstone.backend.model.dto.userresource.UserResourceRequest;
import com.capstone.backend.model.mapper.*;
import com.capstone.backend.repository.*;
import com.capstone.backend.repository.criteria.MaterialsCriteria;
import com.capstone.backend.repository.criteria.ResourceCriteria;
import com.capstone.backend.service.CommentService;
import com.capstone.backend.service.FileService;
import com.capstone.backend.service.ResourceService;
import com.capstone.backend.service.UserResourceService;
import com.capstone.backend.utils.*;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.Collator;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.capstone.backend.utils.Constants.*;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ResourceServiceImpl implements ResourceService {
    ResourceRepository resourceRepository;
    UserResourceRepository userResourceRepository;
    CommentService commentService;
    FileService fileService;
    TagRepository tagRepository;
    LessonRepository lessonRepository;
    SubjectRepository subjectRepository;
    UserHelper userHelper;
    ResourceTagRepository resourceTagRepository;
    UserResourcePermissionRepository userResourcePermissionRepository;
    ResourceCriteria resourceCriteria;
    MessageException messageException;
    MaterialsCriteria materialsCriteria;
    UserRepository userRepository;
    UserRoleRepository userRoleRepository;
    CheckPermissionResource checkPermissionResource;
    UserResourceService userResourceService;
    S3Util s3Util;

    public static <T> List<T> findDistinctElementsById(List<T> list, Function<T, Object> idExtractor) {
        Map<Object, T> distinctById = list.stream()
                .collect(Collectors.toMap(idExtractor, Function.identity(), (existing, replacement) -> existing));

        return distinctById.values().stream().toList();
    }

    private void saveToResourceTag(ResourceDTORequest request, Resource resource) {
        if (request.getTagList() != null) {
            Set<Long> tagIds = DataHelper.parseStringToLongSet(request.getTagList());
            List<ResourceTag> resourceTagList = tagIds.stream()
                    .map(tagId -> {
                        Tag tagObject = tagRepository.findById(tagId)
                                .orElseThrow(() -> ApiException.notFoundException(messageException.MSG_TAG_NOT_FOUND));
                        return ResourceTag.builder()
                                .tag(tagObject)
                                .resource(resource)
                                .detailId(resource.getId())
                                .tableType(TableType.resource_tbl)
                                .active(true)
                                .createdAt(DateTimeHelper.getTimeNow())
                                .build();
                    })
                    .collect(Collectors.toList());
            resource.setResourceTagList(resourceTagList);
        }
    }

    @Override
    public List<ResourceDTOResponse> uploadResource(ResourceDTORequest request, MultipartFile[] files) {
        User userLoggedIn = userHelper.getUserLogin();
        List<FileDTOResponse> fileDTOResponseList = fileService.uploadMultiFile(files, request.getLessonId(), request.getName());
        Subject subject = subjectRepository.findByIdAndActiveTrue(request.getSubjectId())
                .orElseThrow(() -> ApiException.notFoundException("Subject is not found"));

        Lesson lesson = (request.getLessonId() != null)
                ? lessonRepository.findById(request.getLessonId())
                .orElseThrow(() -> ApiException.notFoundException("Lesson is not found"))
                : null;

        return fileDTOResponseList.stream()
                .map(fileDTOResponse -> {
                    // Check document or media
                    boolean isMedia = ResourceType.getFeeList().stream()
                            .anyMatch(rt -> fileDTOResponse.getFileExtension().equalsIgnoreCase(rt.toString()));
                    Long pointResource = isMedia ? Constants.POINT_RESOURCE : 0;

                    TabResourceType tabResourceType = TabResourceType.findByTabResourceType(fileDTOResponse.getResourceType());

                    String resourceName;
                    if (request.getName() == null) {
                        resourceName = DataHelper.extractFilename(fileDTOResponse.getOriginalFileName());
                    } else {
                        resourceName = request.getName().trim();
                    }


                    ApproveType approveType = ApproveType.UNACCEPTED;
                    User moderator = null;
                    if (DataHelper.isRole(userLoggedIn, 2L)) {
                        approveType = ApproveType.ACCEPTED;
                        moderator = userRepository.findById(userLoggedIn.getId()).get();
                    }
                    Resource resource = Resource.builder()
                            .name(resourceName)
                            .description(request.getDescription())
                            .resourceType(fileDTOResponse.getResourceType())
                            .createdAt(DateTimeHelper.getTimeNow())
                            .active(true)
                            .approveType(approveType)
                            .visualType(VisualType.valueOf(request.getVisualType()))
                            .thumbnailSrc(fileDTOResponse.getThumbnailSrc())
                            .resourceSrc(fileDTOResponse.getResourceSrc())
                            .point(pointResource)
                            .size(fileDTOResponse.getSize())
                            .lesson(lesson)
                            .subject(subject)
                            .author(userLoggedIn)
                            .tabResourceType(tabResourceType)
                            .moderator(moderator)
                            .viewCount(0L)
                            .build();

                    resource = resourceRepository.save(resource);
                    saveToResourceTag(request, resource);

                    UserResourcePermission permission = UserResourcePermission.builder()
                            .user(userHelper.getUserLogin())
                            .resource(resource)
                            .active(true)
                            .createdAt(DateTimeHelper.getTimeNow())
                            .permission(CREATOR_RESOURCE_PERMISSION)
                            .build();
                    userResourcePermissionRepository.save(permission);

                    return ResourceMapper.toResourceDTOResponse(resource, s3Util);
                })
                .toList();
    }

    @Override
    public ResourceDetailDTOResponse getResourceDetailById(Long resourceId) {
        User userLoggedIn = userHelper.getUserLogin();
        Resource resource = resourceRepository.findById(resourceId)
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));
        resource.setViewCount(resource.getViewCount() + 1);
        resource = resourceRepository.save(resource);

        boolean isPermission = checkPermissionResource.needCheckPermissionResource(userLoggedIn, resource, PermissionResourceType.V);
        if (!isPermission) {
            throw ApiException.forBiddenException("Access denied, You have no permission");
        }

        boolean isLike = isUserActionType(userLoggedIn, resourceId, ActionType.LIKE);
        boolean isUnlike = isUserActionType(userLoggedIn, resourceId, ActionType.UNLIKE);
        long numberOfLike = countUserActionType(ActionType.LIKE, resourceId);
        long numberOfUnlike = countUserActionType(ActionType.UNLIKE, resourceId);

        List<TagSuggestDTOResponse> listTagNames = resourceTagRepository.findAllTagName(resourceId)
                .stream().map(t -> TagSuggestDTOResponse.builder()
                        .tagId(t.getId())
                        .tagName(t.getName())
                        .build()
                ).toList();

        ResourceDTOResponse resourceDTOResponse = ResourceMapper.toResourceDTOResponse(resource, s3Util);

        User owner = resource.getAuthor();

        return ResourceDetailDTOResponse.builder()
                .isLike(isLike)
                .isUnlike(isUnlike)
                .numberOfLike(numberOfLike)
                .numberOfUnlike(numberOfUnlike)
                .resourceDTOResponse(resourceDTOResponse)
                .listTagRelate(listTagNames)
                .isSave(isUserActionType(userLoggedIn, resourceId, ActionType.SAVED))
                .owner(UserMapper.toUserDTOResponse(owner, s3Util))
                .viewCount(resource.getViewCount())
                .build();
    }

    @Override
    public List<CommentDetailDTOResponse> getListCommentsResource(Long id) {
        User userLoggedIn = userHelper.getUserLogin();
        Resource resource = resourceRepository.findById(id)
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));

        boolean isPermission = checkPermissionResource.needCheckPermissionResource(userLoggedIn, resource, PermissionResourceType.V);
        if (!isPermission) {
            throw ApiException.forBiddenException("Access denied, You have no permission");
        }

        return commentService.getListCommentDetailDTOResponse(resource.getId());
    }

    @Override
    public List<ResourceViewDTOResponse> getListResourceRelates(Long resourceId) {
        User userLoggedIn = userHelper.getUserLogin();
        Resource resource = resourceRepository.findById(resourceId)
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));

        boolean isPermission = checkPermissionResource.needCheckPermissionResource(userLoggedIn, resource, PermissionResourceType.V);
        if (!isPermission) {
            throw ApiException.forBiddenException("Access denied, You have no permission");
        }

        List<String> listTagNameString = resourceTagRepository.findAllTagName(resourceId)
                .stream().map(t -> TagSuggestDTOResponse.builder().tagId(t.getId()).tagName(t.getName()).build())
                .map(TagSuggestDTOResponse::getTagName)
                .toList();

        return mediaRelateResource(resource, listTagNameString);
    }

    public List<ResourceViewDTOResponse> mediaRelateResource(Resource resource, List<String> listTagNames) {
        List<ResourceViewDTOResponse> relateLists = new ArrayList<>();
        int size;
        Pageable pageable;

        // tag
        if (listTagNames != null && !listTagNames.isEmpty() && resource.getLesson() == null) {
            pageable = PageRequest.of(0, SUGGEST_NUMBER_DOCUMENT);
            relateLists.addAll(getRelatedResources(resourceTagRepository
                    .findAllResourceByTagNameSameResourceTypeTest(resource.getTabResourceType(), resource.getId(), listTagNames, pageable)));
        }
        //lesson
        else if (resource.getLesson() != null) {
            pageable = PageRequest.of(0, SUGGEST_NUMBER_DOCUMENT);
            relateLists.addAll(getRelatedResources(
                    resourceTagRepository.findAllResourceByLessonIdSameResourceTypeTest(resource.getTabResourceType(), resource.getId(), resource.getLesson().getId())
            ));
        }

        // subject
        size = relateLists.size();
        if (size < SUGGEST_NUMBER_DOCUMENT && resource.getSubject() != null) {
            pageable = PageRequest.of(0, SUGGEST_NUMBER_DOCUMENT - size);
            relateLists.addAll(getRelatedResources(resourceTagRepository
                    .findResourceBySubjectSameResourceTypeTest(resource.getTabResourceType(), resource.getSubject().getId(), pageable)));
        }

        // view
        size = relateLists.size();
        if (size < SUGGEST_NUMBER_DOCUMENT) {
            pageable = PageRequest.of(0, SUGGEST_NUMBER_DOCUMENT - size);
            relateLists.addAll(getRelatedResources(resourceTagRepository
                    .findResourceByViewSameResourceTypeTest(resource.getTabResourceType(), pageable)));
        }

        return findDistinctElementsById(relateLists, ResourceViewDTOResponse::getId);
    }

    private List<ResourceViewDTOResponse> getRelatedResources(List<Resource> resources) {
        User userLoggedIn = userHelper.getUserLogin();
        return resources.stream()
                .map(r -> ResourceMapper.toResourceViewDTOResponse(r, isUserActionType(userLoggedIn, r.getId(), ActionType.SAVED), s3Util))
                .toList();
    }

    private Set<Long> getResourceIdsByCriteria(TableType tableType, Set<ResourceMediaDTOCriteria> criteriaList) {
        return criteriaList.stream()
                .filter(criteria -> criteria.getTableType() == tableType)
                .map(ResourceMediaDTOCriteria::getDetailId)
                .collect(Collectors.toSet());
    }

    @Override
    public PagingDTOResponse searchMediaResource(ResourceMediaDTOFilter resourceDTOFilter) {
        Set<ResourceMediaDTOCriteria> resourceMediaDTOCriteriaList = resourceTagRepository
                .findResourceTagByTagId(resourceDTOFilter.getListTags()).stream()
                .map(ResourceMapper::toResourceMediaDTOCriteria)
                .collect(Collectors.toSet());

        // Tạo một Map để lưu trữ các danh sách theo TableType
        Map<TableType, Set<Long>> tableTypeToIdsMap = new HashMap<>();

        // Sử dụng Map để gom nhóm các danh sách dựa trên TableType
        for (TableType type : TableType.values()) {
            tableTypeToIdsMap.computeIfAbsent(type, k -> getResourceIdsByCriteria(k, resourceMediaDTOCriteriaList));
        }

        Set<Long> ids = new HashSet<>();
        tableTypeToIdsMap.forEach((k, v) -> {
            ids.addAll(getResourceIdOfTableType(k, v));
        });

        return resourceCriteria.searchMediaResource(ids, resourceDTOFilter);
    }

    public Set<Long> getResourceIdOfTableType(TableType tableType, Set<Long> tableIds) {
        return switch (tableType) {
            case class_tbl -> resourceRepository.findResourceIdClass(tableIds);
            case book_series_tbl -> resourceRepository.findResourceIdBookSeries(tableIds);
            case book_series_subject_tbl -> resourceRepository.findResourceIdSubject(tableIds);
            case book_volume_tbl -> resourceRepository.findResourceIdBookVolume(tableIds);
            case chapter_tbl -> resourceRepository.findResourceIdChapter(tableIds);
            case lesson_tbl -> resourceRepository.findResourceIdLesson(tableIds);
            default -> tableIds;
        };
    }

    @Override
    public PagingDTOResponse searchMaterials(MaterialsFilterDTORequest request) {
        return materialsCriteria.searchMaterials(request);
    }

    @Override
    public byte[] downloadResource(String fileName) {
        Resource document = resourceRepository.findByResourceSrc(fileName)
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));
        User userLoggedIn = userHelper.getUserLogin();

        boolean isPermission = checkPermissionResource
                .needCheckPermissionResource(userLoggedIn, document, PermissionResourceType.D);
        if (!isPermission || userLoggedIn == null)
            throw ApiException.forBiddenException("Access denied, You have no permission");

        byte[] bytes = null;
        long pointRemain = userLoggedIn.getTotalPoint() - document.getPoint();
        if (pointRemain > 0) {
            bytes = fileService.downloadFile(fileName);
            userLoggedIn.setTotalPoint(pointRemain);
            userRepository.save(userLoggedIn);
        } else throw ApiException.forBiddenException("File download error");

        boolean action = userResourceService.actionResource(UserResourceRequest.builder()
                .actionType(ActionType.DOWNLOAD)
                .resourceId(document.getId())
                .build());
        if (!action) throw ApiException.badRequestException(messageException.MSG_INTERNAL_SERVER_ERROR);
        return bytes;
    }

    @Override
    public Boolean shareResource(ResourceSharedDTORequest request) {
        User userLoggedIn = userHelper.getUserLogin();
        Resource resource = resourceRepository.findById(request.getResourceId())
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));

        checkPermissionAndOwner(userLoggedIn, resource);

        resource.setVisualType(request.getVisualType());
        resource = resourceRepository.save(resource);

        List<Long> listUserIdShared = userResourcePermissionRepository
                .findByResource(resource.getId(), userLoggedIn.getId()).stream()
                .map(upr -> upr.getUser().getId())
                .toList();

        List<Long> listUserIdShareNew = request.getUserShareIds();

        List<Long> listAdded = listUserIdShareNew.stream().filter(e -> !listUserIdShared.contains(e)).toList();
        List<Long> listDeleted = listUserIdShared.stream().filter(e -> !listUserIdShareNew.contains(e)).toList();

        // Thêm quyền truy cập cho người dùng
        if (!listAdded.isEmpty()) {
            List<UserResourcePermission> userResourcePermissionAdds = addUserPermissions(listAdded, resource);
            userResourcePermissionRepository.saveAll(userResourcePermissionAdds);
        }

        // Xóa quyền truy cập của người dùng
        if (!listDeleted.isEmpty()) {
            deleteUserPermissions(listDeleted, resource);
        }
        return true;
    }

    private void checkPermissionAndOwner(User user, Resource resource) {
        if (user == null || !checkPermissionResource.needCheckPermissionResource(user, resource, PermissionResourceType.C)) {
            throw ApiException.forBiddenException(messageException.MSG_NO_PERMISSION);
        }

        User ownerResource = userResourcePermissionRepository.getUserOwnerResource(resource.getId());
        if (!Objects.equals(user.getId(), ownerResource.getId())) {
            throw ApiException.internalServerException(messageException.MSG_NO_PERMISSION);
        }
    }

    private List<UserResourcePermission> addUserPermissions(List<Long> userIds, Resource resource) {
        return userIds.stream()
                .map(userId -> {
                    User user = userRepository.findById(userId)
                            .orElseThrow(() -> ApiException.notFoundException(messageException.MSG_USER_NOT_FOUND));
                    return UserResourceMapper.toUserResourcePermission(user, resource);
                })
                .toList();
    }

    private void deleteUserPermissions(List<Long> userIds, Resource resource) {
        userIds.forEach(userId -> {
            User user = userRepository.findById(userId)
                    .orElseThrow(() -> ApiException.notFoundException(messageException.MSG_USER_NOT_FOUND));
            userResourcePermissionRepository.findByUserAndResource(user, resource)
                    .ifPresent(userResourcePermissionRepository::delete);
        });
    }

    @Override
    public ResourceSharedDTOResponse viewResourceShareById(Long resourceId) {
        var userLoggedIn = userHelper.getUserLogin();
        var ownerResource = userResourcePermissionRepository.getUserOwnerResource(resourceId);
        var resource = resourceRepository.findById(resourceId)
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));

        //owner of resource can view
        if (!Objects.equals(userLoggedIn.getId(), ownerResource.getId())) {
            throw ApiException.internalServerException("Access denied, You have no permission");
        }

        //check permission resource
        boolean isPermission = checkPermissionResource
                .needCheckPermissionResource(userLoggedIn, resource, PermissionResourceType.C);
        if (!isPermission)
            throw ApiException.forBiddenException(messageException.MSG_NO_PERMISSION);

        var userResourcePermissions = new ArrayList<>(userResourcePermissionRepository
                .findByResource(resource.getId(), ownerResource.getId()).stream()
                .sorted(Comparator.comparing(up -> up.getUser().getUsername()))
                .toList());

        var userSharedDTOResponses = userResourcePermissions.stream()
                .map(user -> ResourceMapper.toUserSharedDTOResponse(user.getUser(), Constants.SHARED_RESOURCE_PERMISSION_MESSAGE, s3Util))
                .peek(user -> {
                    if (Objects.equals(user.getUserShareId(), ownerResource.getId())) {
                        user.setPermission(CREATOR_RESOURCE_PERMISSION_MESSAGE);
                    }
                })
                .toList();

        return ResourceSharedDTOResponse.builder()
                .resourceId(resource.getId())
                .name(resource.getName())
                .userSharedDTOResponses(userSharedDTOResponses)
                .visualType(resource.getVisualType())
                .build();
    }

    @Override
    public List<UserSharedDTOResponse> suggestionUserShare(String text) {
        User userLoggedIn = userHelper.getUserLogin();
        List<User> users = userRoleRepository.findTeacherByUsernameOrEmailContaining(text, userLoggedIn.getId());
        return users.stream()
                .map(user -> ResourceMapper.toUserSharedDTOResponse(user, Constants.SHARED_RESOURCE_PERMISSION_MESSAGE, s3Util))
                .toList();
    }

    @Override
    public List<TagSuggestDTOResponse> getListTagsSuggest(TagSuggestDTORequest request) {
        Collator collator = Collator.getInstance(new Locale("vi", "VN"));
        return tagRepository
                .findAllByNameContainsAndActive(request.getTagSuggest()).stream()
                .map(TagMapper::toTagSuggestDTOResponse)
                .sorted(Comparator.comparing(TagSuggestDTOResponse::getTagName, collator))
                .toList();
    }

    @Override
    public List<TagSuggestDTOResponse> getListTagsGlobalSuggest(TagSuggestDTORequest request) {
        Collator collator = Collator.getInstance(new Locale("vi", "VN"));
        return tagRepository
                .findGlobalTagByNameContainsAndActive(request.getTagSuggest()).stream()
                .map(TagMapper::toTagSuggestDTOResponse)
                .sorted(Comparator.comparing(TagSuggestDTOResponse::getTagName, collator))
                .toList();
    }

    @Override
    public ResourceShowDTOResponse viewResource(Long id) {
        User user = userHelper.getUserLogin();
        var resource = resourceRepository.findByIdAndActiveTrue(id)
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));
        boolean isPermission = checkPermissionResource
                .needCheckPermissionResource(user, resource, PermissionResourceType.V);
        if (resource.getAuthor() != user || !isPermission)
            throw ApiException.forBiddenException("Access denied, You have no permission");
        return ResourceMapper.toResourceDTOUpdateResponse(resource);
    }

    @Override
    public ResourceDTOResponse updateResource(ResourceDTOUpdateRequest request, MultipartFile[] file) {
        Set<Long> tagList = new HashSet<>();
        System.out.println(request.getTagList());
        if (request.getTagList() != null) {
            tagList = DataHelper.parseStringToLongSet(request.getTagList());
        }
        User user = userHelper.getUserLogin();
        if (file != null && file.length > 1) {
            throw ApiException.badRequestException("Choose only one file");
        }

        var data = new Object() {
            final Resource resource = resourceRepository.findByIdAndActiveTrue(request.getId())
                    .orElseThrow(() -> ApiException.notFoundException(messageException.MSG_RESOURCE_NOT_FOUND));
        };
        Resource resourceTemp = data.resource;

        boolean isPermission = checkPermissionResource
                .needCheckPermissionResource(user, data.resource, PermissionResourceType.U);
        if (data.resource.getAuthor() != user || !isPermission)
            throw ApiException.forBiddenException(messageException.MSG_NO_PERMISSION);
        FileDTOResponse fileDTOResponse = new FileDTOResponse();

        //media
        if (request.getLessonId() == null) {
            if (file != null) {
                String filename = file[0].getOriginalFilename();
                String extension = DataHelper.extractFileExtension(filename).toUpperCase();
                List<ResourceType> resourceTypes = ResourceType.getFeeList();
                boolean isResourceType = resourceTypes.stream().anyMatch(rt -> rt.toString().equalsIgnoreCase(extension));
                if (isResourceType) {
                    throw ApiException.conflictResourceException("Update file name extension such as (png, jpg, jpeg, mp3, mp4)");
                }
            }

            //change resource tag
            List<Long> tagRequest = tagList.stream().toList();
            List<Long> tagAvailable = resourceTagRepository.findByResourceId(data.resource.getId()).stream()
                    .map(ResourceTag::getTag)
                    .map(Tag::getId)
                    .toList();

            List<ResourceTag> listAddResourceTags = tagRequest.stream()
                    .filter(tag -> !tagAvailable.contains(tag))
                    .map(tagId -> ResourceTagMapper.toResourceTag(tagRepository.findByIdAndActiveTrue(tagId).orElseThrow(), data.resource))
                    .toList();

            resourceTagRepository.saveAll(listAddResourceTags);

            List<ResourceTag> listDeleteResourceTags = tagAvailable.stream()
                    .filter(tag -> !tagRequest.contains(tag))
                    .map(tagId -> resourceTagRepository.findByTagAndResource(tagId, data.resource.getId(), true))
                    .toList();

            resourceTagRepository.deleteAll(listDeleteResourceTags);

        } else {
            resourceTemp.setLesson(lessonRepository.findById(request.getLessonId()).get());
        }

        if (file != null) {
            fileService.deleteFileResource(data.resource.getResourceSrc());
            fileDTOResponse = fileService.uploadMultiFile(file, request.getLessonId(), request.getName()).get(0);
            resourceTemp.setResourceSrc(fileDTOResponse.getResourceSrc());
            resourceTemp.setSize(fileDTOResponse.getSize());
            resourceTemp.setResourceType(fileDTOResponse.getResourceType());
            resourceTemp.setThumbnailSrc(fileDTOResponse.getThumbnailSrc());
            resourceTemp.setTabResourceType(TabResourceType.findByTabResourceType(fileDTOResponse.getResourceType()));
        }
        resourceTemp.setName(request.getName());
        resourceTemp.setDescription(request.getDescription());
        resourceTemp.setSubject(subjectRepository.findById(request.getSubjectId()).get());
        resourceTemp.setVisualType(request.getVisualType());
        resourceTemp.setCreatedAt(DateTimeHelper.getTimeNow());

        if (resourceTemp.getApproveType() == ApproveType.REJECT) {
            resourceTemp.setApproveType(ApproveType.UNACCEPTED);
        }

        resourceTemp = resourceRepository.save(resourceTemp);
        return ResourceMapper.toResourceDTOResponse(resourceTemp, s3Util);
    }

    @Override
    public Boolean deleteResource(Long id) {
        User user = userHelper.getUserLogin();
        var resource = resourceRepository.findByIdAndActiveTrue(id)
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));
        UserResourcePermission permission = userResourcePermissionRepository.findByUserAndResource(user, resource)
                .orElseThrow(() -> ApiException.notFoundException("permission is not found"));
        boolean isDelete = permission.getPermission().contains("D");
        if (user == resource.getAuthor() && isDelete) {
            resource.setActive(false);
            resourceRepository.save(resource);
            return true;
        }
        return false;
    }

    @Override
    public Boolean changeVisualType(Long id, VisualType type) {
        User user = userHelper.getUserLogin();
        var resource = resourceRepository.findByIdAndActiveTrue(id)
                .orElseThrow(() -> ApiException.notFoundException("Resource is not found"));
        if (user == resource.getAuthor()) {
            resource.setVisualType(type);
            resourceRepository.save(resource);
            return true;
        }
        return false;
    }

    public boolean isUserActionType(User user, Long resourceId, ActionType actionType) {
        return user != null && userResourceRepository.findUserResourceHasActionType(user.getId(), resourceId, actionType).isPresent();
    }

    public long countUserActionType(ActionType actionType, Long resourceId) {
        return userResourceRepository.countByActionTypeWithResource(actionType, resourceId);
    }
}
