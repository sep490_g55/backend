package com.capstone.backend.service.impl;

import com.capstone.backend.entity.SystemPermission;
import com.capstone.backend.entity.User;
import com.capstone.backend.exception.ApiException;
import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.systempermission.*;
import com.capstone.backend.model.mapper.SystemPermissionMapper;
import com.capstone.backend.repository.SystemPermissionRepository;
import com.capstone.backend.repository.UserRepository;
import com.capstone.backend.repository.criteria.SystemPermissionCriteria;
import com.capstone.backend.service.SystemPermissionService;
import com.capstone.backend.utils.MessageException;
import com.capstone.backend.utils.UserHelper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SystemPermissionServiceImpl implements SystemPermissionService {
    SystemPermissionRepository systemPermissionRepository;
    SystemPermissionCriteria systemPermissionCriteria;
    UserRepository userRepository;
    MessageException messageException;
    UserHelper userHelper;

    @Override
    public PagingDTOResponse viewSearchPermission(SystemPermissionDTOFilter request) {
        return systemPermissionCriteria.viewSearchPermission(request);
    }

    @Override
    public SystemPermissionDTOResponse getSystemPermissionById(Long id) {
        SystemPermission systemPermission = systemPermissionRepository.findById(id)
                .orElseThrow(() -> ApiException.notFoundException("System permission is not found")
                );
        return SystemPermissionMapper
                .toSystemPermissionDTOResponse(
                        systemPermission,
                        userRepository.findUsernameByUserId(systemPermission.getUserId())
                );
    }

    @Override
    public SystemPermissionDTOResponse createSystemPermission(SystemPermissionDTORequest request) {
        User userLoggedIn = userHelper.getUserLogin();
        Optional<SystemPermission> systemPermissionOptional = systemPermissionRepository
                .findByPathAndMethod(request.getPath(), request.getMethodType(), 0L);
        if (systemPermissionOptional.isPresent()) {
            throw ApiException.badRequestException("Duplicate system permission name or method");
        }

        SystemPermission systemPermission = SystemPermissionMapper
                .toSystemPermission(request);
        systemPermission.setUserId(userLoggedIn.getId());
        systemPermission = systemPermissionRepository.save(systemPermission);
        return SystemPermissionMapper
                .toSystemPermissionDTOResponse(
                        systemPermission,
                        userRepository.findUsernameByUserId(systemPermission.getUserId())
                );
    }

    @Override
    public SystemPermissionDTOResponse updateSystemPermission(SystemPermissionDTOUpdate request) {
        User userLoggedIn = userHelper.getUserLogin();

        Optional<SystemPermission> systemPermissionOptional = systemPermissionRepository
                .findByPathAndMethod(request.getPath(), request.getMethodType(), request.getPermissionId());
        if (systemPermissionOptional.isPresent()) {
            throw ApiException.badRequestException("Duplicate system permission name or method");
        }

        SystemPermission systemPermissionDb = systemPermissionRepository
                .findById(request.getPermissionId())
                .orElseThrow(() -> ApiException.notFoundException(messageException.MSG_SYSTEM_PERMISSION_NOT_FOUND));

        SystemPermission systemPermission = SystemPermissionMapper
                .toSystemPermission(request);
        systemPermission.setUserId(userLoggedIn.getId());
        systemPermission.setActive(systemPermissionDb.getActive());

        systemPermission = systemPermissionRepository.save(systemPermission);
        return SystemPermissionMapper
                .toSystemPermissionDTOResponse(
                        systemPermission,
                        userRepository.findUsernameByUserId(systemPermission.getUserId()
                        )
                );
    }

    @Override
    public Boolean changeStatus(Long id) {
        SystemPermission systemPermission = systemPermissionRepository
                .findById(id)
                .orElseThrow(() -> ApiException.notFoundException("System permission is not found"));
        systemPermission.setActive(!systemPermission.getActive());
        systemPermissionRepository.save(systemPermission);
        return true;
    }

    @Override
    public List<PermissionDTODisplay> getSystemPermissionsByRole(Long roleId) {
        List<SystemPermission> systemPermissions = systemPermissionRepository
                .findSystemPermissionByRoleId(roleId);
        return systemPermissions.stream()
                .map(SystemPermissionMapper::toPermissionDTODisplay)
                .toList();
    }

    @Override
    public List<PermissionDTODisplay> getListSystemPermissions() {
        List<SystemPermission> systemPermissions = systemPermissionRepository
                .findAll()
                .stream().filter(SystemPermission::getActive)
                .toList();
        return systemPermissions.stream()
                .map(SystemPermissionMapper::toPermissionDTODisplay)
                .toList();
    }


}
