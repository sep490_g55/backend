package com.capstone.backend.service;

import com.capstone.backend.model.dto.PagingDTOResponse;
import com.capstone.backend.model.dto.classes.ClassDTOFilter;
import com.capstone.backend.model.dto.classes.ClassDTORequest;
import com.capstone.backend.model.dto.classes.ClassDTOResponse;

import java.util.List;

public interface ClassService {
    ClassDTOResponse createClass(ClassDTORequest request);

    ClassDTOResponse updateClass(Long id, ClassDTORequest request);

    Boolean changeStatus(Long id);

    ClassDTOResponse viewClassById(Long id);

    PagingDTOResponse searchClass(ClassDTOFilter classDTOFilter);

    List<ClassDTOResponse> getListClasses();

    List<ClassDTOResponse> getListClassesBySubjectId(Long subjectId);
}
