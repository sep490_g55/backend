<h1 align="center">Welcome to Educational Material Sharing System 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
</p>

> Educational Material Sharing System

### ✨ [Educational Material Sharing System](http://localhost:8080)

## Author

👤 **Luong Trung Kien**

* Gitlab: [@com.capstone.backend](https://gitlab.com/sep490_g55/backend)

## Show your support

Give a ⭐️ if this project helped you!